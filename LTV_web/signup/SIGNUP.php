<?php
session_start();
include('../conn.php');
$first = "";

if(isset($_POST["newname"],$_POST["newemail"], $_POST["newpassword"], $_POST["newpassword2"], $_POST["platforms"])){

    $name  = $_POST["newname"];
    $email = $_POST["newemail"];
    $pass  = $_POST["newpassword"];
    $pass2 = $_POST["newpassword2"];
    $plat  = $_POST["platforms"];
    $date  = date("Y-m-d");
    $is_payer = 0;
    $sql="select * from login where emaillogin='$email'";
    $statement1 = $connect->prepare($sql);
    $statement1->execute();
    $result = $statement1->fetchAll();

    if(count($result)!= 0){
                    $first1 ='<br />
                    <div class="alert" style="text-align:center;">
                    <span class="closebtn" onclick="this.parentElement.style.display='."'none'".';">&times;</span> 
                    <strong>Fail!</strong> Email address already exists.
                     </div>';                    
                }

    elseif($pass!= $pass2){
               $first1 = '<br />
               <div class="alert warning">
               <span class="closebtn" onclick="this.parentElement.style.display='."'none'".';">&times;</span> 
               <strong>Warning!</strong> Password confirmation does not match.
               </div>';           
               }

    elseif($plat =="deltadna"){
              $hashpass = password_hash($pass, PASSWORD_DEFAULT);
              $query = "insert into login (emaillogin,password,name,platform,start_date,is_payer) values ('$email','$hashpass','$name','$plat','$date','$is_payer')";
              $statement = $connect->prepare($query);
              $statement->execute();
              
              $editemail = str_replace(".", "_" , str_replace("@","_", $email));
              $query1 = 'CREATE TABLE '.$editemail.'_ltvfilter (
                          EVENT_DATE date,
                          PLATFORM VARCHAR(255),
                          CAMPAIGN_NAME VARCHAR(255),
                          COUNTRY VARCHAR(255)
                          );';
              $statement1 = $connect->prepare($query1);
              $statement1->execute();


              $query2 = 'CREATE TABLE '.$editemail.'_ltvdata (
                DaySinceInstall bigint(20),
                LTV1 float,
                LowerLimit1 float,
                UpperLimit1 float,
                LTV float,
                UpperLimit float,
                LowerLimit float,
                ROAS float,
                revenue float,
                total_users bigint(20),
                total_payers bigint(20),
                ARPDAU float

                );';
              $statement2 = $connect->prepare($query2);
              $statement2->execute();

              $query3 = 'CREATE TABLE '.$editemail.'_roasfilter (
                EVENT_DATE date,
                PLATFORM VARCHAR(255),
                CAMPAIGN_NAME VARCHAR(255),
                COUNTRY VARCHAR(255)
                );';
              $statement3 = $connect->prepare($query3);
              $statement3->execute();

              $query33 = 'CREATE TABLE '.$editemail.'_roasdata (
                INSTALLDATE date,
                ACQUISITION_CHANNEL VARCHAR(255),
                USERS bigint(20),
                CAMPAIGN VARCHAR(255),
                PAYERS float,
                D0REVENUE float,
                D1REVENUE float,
                D2REVENUE float,
                D3REVENUE float,
                D4REVENUE float,
                D5REVENUE float,
                D6REVENUE float,
                D7REVENUE float,
                D14REVENUE float,
                D28REVENUE float,
                INSTALL_RANK bigint(20),
                WEEK_RANK bigint(20),
                CAMPAIGN_RANK bigint(20)
                );';
              $statement33 = $connect->prepare($query33);
              $statement33->execute();

              $query4 = 'CREATE TABLE '.$editemail.'_dailyroas (
                INSTALLDATE date,
                ACQUISITION_CHANNEL VARCHAR(255),
                USERS bigint(20),
                PAYERS bigint(20),
                D0REVENUE float,
                D1REVENUE float,
                D2REVENUE float,
                D3REVENUE float,
                D4REVENUE float,
                D5REVENUE float,
                D6REVENUE float,
                D7REVENUE float,
                D14REVENUE float,
                D28REVENUE float,
                COST float,
                rownumber bigint(20)
                );';
              $statement4 = $connect->prepare($query4);
              $statement4->execute();

              $query5 = 'CREATE TABLE '.$editemail.'_weeklyroas (
                INSTALLWEEK VARCHAR(255),
                ACQUISITION_CHANNEL VARCHAR(255),
                USERS bigint(20),
                PAYERS bigint(20),
                D0REVENUE float,
                D1REVENUE float,
                D2REVENUE float,
                D3REVENUE float,
                D4REVENUE float,
                D5REVENUE float,
                D6REVENUE float,
                D7REVENUE float,
                D14REVENUE float,
                D28REVENUE float,
                COST float,
                rownumber bigint(20)
                );';
              $statement5 = $connect->prepare($query5);
              $statement5->execute();
              

              $query6 = 'CREATE TABLE '.$editemail.'_campaignroas (
                CAMPAIGN VARCHAR(255),
                MAX_DATE DATE,
                USERS bigint(20),
                PAYERS bigint(20),
                D0REVENUE float,
                D1REVENUE float,
                D2REVENUE float,
                D3REVENUE float,
                D4REVENUE float,
                D5REVENUE float,
                D6REVENUE float,
                D7REVENUE float,
                D14REVENUE float,
                D28REVENUE float,
                rownumber bigint(20),
                COST float
                );';
              $statement6 = $connect->prepare($query6);
              $statement6->execute();


              mkdir('../users/'.$email);
              copy("../deltadna/first.py",'../users/'.$email.'/first.py');
              copy("../deltadna/ltv.py",'../users/'.$email.'/ltv.py');
              copy("../deltadna/second.py",'../users/'.$email.'/second.py');
              copy("../deltadna/roas.py",'../users/'.$email.'/roas.py');
              $_SESSION["id"]       = $email;  
              $_SESSION["platform"] = $plat;
              $_SESSION["name"]     = $name;
              $_SESSION["date"]     = $date;
              $_SESSION["is_payer"] = $is_payer;
              header("Location:../setting/CONFIGURATION.php");

          }

    elseif($plat =="bigquery"){
            $hashpass = password_hash($pass, PASSWORD_DEFAULT);
            $query = "insert into login (emaillogin,password,name,platform,start_date,is_payer) values ('$email','$hashpass','$name','$plat','$date','$is_payer')";
            $statement = $connect->prepare($query);
            $statement->execute();
            
            $editemail = str_replace(".", "_" , str_replace("@","_", $email));
            $query1 = 'CREATE TABLE '.$editemail.'_ltvfilter (
                        EVENT_DATE date,
                        PLATFORM VARCHAR(255),
                        CAMPAIGN_NAME VARCHAR(255),
                        COUNTRY VARCHAR(255)
                        );';
            $statement1 = $connect->prepare($query1);
            $statement1->execute();


            $query2 = 'CREATE TABLE '.$editemail.'_ltvdata (
              DaySinceInstall bigint(20),
              LTV1 float,
              LowerLimit1 float,
              UpperLimit1 float,
              LTV float,
              UpperLimit float,
              LowerLimit float,
              ROAS float,
              revenue float,
              total_users bigint(20),
              total_payers bigint(20),
              ARPDAU float

              );';
            $statement2 = $connect->prepare($query2);
            $statement2->execute();

            $query3 = 'CREATE TABLE '.$editemail.'_roasfilter (
              EVENT_DATE date,
              PLATFORM VARCHAR(255),
              CAMPAIGN_NAME VARCHAR(255),
              COUNTRY VARCHAR(255)
              );';
            $statement3 = $connect->prepare($query3);
            $statement3->execute();

            $query33 = 'CREATE TABLE '.$editemail.'_roasdata (
              INSTALLDATE date,
              ACQUISITION_CHANNEL VARCHAR(255),
              USERS bigint(20),
              CAMPAIGN VARCHAR(255),
              PAYERS float,
              D0REVENUE float,
              D1REVENUE float,
              D2REVENUE float,
              D3REVENUE float,
              D4REVENUE float,
              D5REVENUE float,
              D6REVENUE float,
              D7REVENUE float,
              D14REVENUE float,
              D28REVENUE float,
              INSTALL_RANK bigint(20),
              WEEK_RANK bigint(20),
              CAMPAIGN_RANK bigint(20)
              );';
            $statement33 = $connect->prepare($query33);
            $statement33->execute();

            $query4 = 'CREATE TABLE '.$editemail.'_dailyroas (
              INSTALLDATE date,
              ACQUISITION_CHANNEL VARCHAR(255),
              USERS bigint(20),
              PAYERS bigint(20),
              D0REVENUE float,
              D1REVENUE float,
              D2REVENUE float,
              D3REVENUE float,
              D4REVENUE float,
              D5REVENUE float,
              D6REVENUE float,
              D7REVENUE float,
              D14REVENUE float,
              D28REVENUE float,
              COST float,
              rownumber bigint(20)
              );';
            $statement4 = $connect->prepare($query4);
            $statement4->execute();

            $query5 = 'CREATE TABLE '.$editemail.'_weeklyroas (
              INSTALLWEEK VARCHAR(255),
              ACQUISITION_CHANNEL VARCHAR(255),
              USERS bigint(20),
              PAYERS bigint(20),
              D0REVENUE float,
              D1REVENUE float,
              D2REVENUE float,
              D3REVENUE float,
              D4REVENUE float,
              D5REVENUE float,
              D6REVENUE float,
              D7REVENUE float,
              D14REVENUE float,
              D28REVENUE float,
              COST float,
              rownumber bigint(20)
              );';
            $statement5 = $connect->prepare($query5);
            $statement5->execute();
            

            $query6 = 'CREATE TABLE '.$editemail.'_campaignroas (
              CAMPAIGN VARCHAR(255),
              MAX_DATE DATE,
              USERS bigint(20),
              PAYERS bigint(20),
              D0REVENUE float,
              D1REVENUE float,
              D2REVENUE float,
              D3REVENUE float,
              D4REVENUE float,
              D5REVENUE float,
              D6REVENUE float,
              D7REVENUE float,
              D14REVENUE float,
              D28REVENUE float,
              rownumber bigint(20),
              COST float
              );';
            $statement6 = $connect->prepare($query6);
            $statement6->execute();


              mkdir('../users/'.$email);
              copy("../bigquery/first.py",'../users/'.$email.'/first.py');
              copy("../bigquery/ltv.py",'../users/'.$email.'/ltv.py');
              copy("../bigquery/second.py",'../users/'.$email.'/second.py');
              copy("../bigquery/roas.py",'../users/'.$email.'/roas.py');
            $_SESSION["id"]       = $email;  
            $_SESSION["platform"] = $plat;
            $_SESSION["name"]     = $name;
            $_SESSION["date"]     = $date;
            $_SESSION["is_payer"] = $is_payer;
            header("Location:../setting/CONFIGURATION.php");

        }

    elseif($plat =="snowflake"){
          $hashpass = password_hash($pass, PASSWORD_DEFAULT);
          $query = "insert into login (emaillogin,password,name,platform,start_date,is_payer) values ('$email','$hashpass','$name','$plat','$date','$is_payer')";
          $statement = $connect->prepare($query);
          $statement->execute();
          
          $editemail = str_replace(".", "_" , str_replace("@","_", $email));
          $query1 = 'CREATE TABLE '.$editemail.'_ltvfilter (
                      EVENT_DATE date,
                      PLATFORM VARCHAR(255),
                      CAMPAIGN_NAME VARCHAR(255),
                      COUNTRY VARCHAR(255)
                      );';
          $statement1 = $connect->prepare($query1);
          $statement1->execute();


          $query2 = 'CREATE TABLE '.$editemail.'_ltvdata (
            DaySinceInstall bigint(20),
            LTV1 float,
            LowerLimit1 float,
            UpperLimit1 float,
            LTV float,
            UpperLimit float,
            LowerLimit float,
            ROAS float,
            revenue float,
            total_users bigint(20),
            total_payers bigint(20),
            ARPDAU float

            );';
          $statement2 = $connect->prepare($query2);
          $statement2->execute();

          $query3 = 'CREATE TABLE '.$editemail.'_roasfilter (
            EVENT_DATE date,
            PLATFORM VARCHAR(255),
            CAMPAIGN_NAME VARCHAR(255),
            COUNTRY VARCHAR(255)
            );';
          $statement3 = $connect->prepare($query3);
          $statement3->execute();

          $query33 = 'CREATE TABLE '.$editemail.'_roasdata (
            INSTALLDATE date,
            ACQUISITION_CHANNEL VARCHAR(255),
            USERS bigint(20),
            CAMPAIGN VARCHAR(255),
            PAYERS float,
            D0REVENUE float,
            D1REVENUE float,
            D2REVENUE float,
            D3REVENUE float,
            D4REVENUE float,
            D5REVENUE float,
            D6REVENUE float,
            D7REVENUE float,
            D14REVENUE float,
            D28REVENUE float,
            INSTALL_RANK bigint(20),
            WEEK_RANK bigint(20),
            CAMPAIGN_RANK bigint(20)
            );';
          $statement33 = $connect->prepare($query33);
          $statement33->execute();

          $query4 = 'CREATE TABLE '.$editemail.'_dailyroas (
            INSTALLDATE date,
            ACQUISITION_CHANNEL VARCHAR(255),
            USERS bigint(20),
            PAYERS bigint(20),
            D0REVENUE float,
            D1REVENUE float,
            D2REVENUE float,
            D3REVENUE float,
            D4REVENUE float,
            D5REVENUE float,
            D6REVENUE float,
            D7REVENUE float,
            D14REVENUE float,
            D28REVENUE float,
            COST float,
            rownumber bigint(20)
            );';
          $statement4 = $connect->prepare($query4);
          $statement4->execute();

          $query5 = 'CREATE TABLE '.$editemail.'_weeklyroas (
            INSTALLWEEK VARCHAR(255),
            ACQUISITION_CHANNEL VARCHAR(255),
            USERS bigint(20),
            PAYERS bigint(20),
            D0REVENUE float,
            D1REVENUE float,
            D2REVENUE float,
            D3REVENUE float,
            D4REVENUE float,
            D5REVENUE float,
            D6REVENUE float,
            D7REVENUE float,
            D14REVENUE float,
            D28REVENUE float,
            COST float,
            rownumber bigint(20)
            );';
          $statement5 = $connect->prepare($query5);
          $statement5->execute();
          

          $query6 = 'CREATE TABLE '.$editemail.'_campaignroas (
            CAMPAIGN VARCHAR(255),
            MAX_DATE DATE,
            USERS bigint(20),
            PAYERS bigint(20),
            D0REVENUE float,
            D1REVENUE float,
            D2REVENUE float,
            D3REVENUE float,
            D4REVENUE float,
            D5REVENUE float,
            D6REVENUE float,
            D7REVENUE float,
            D14REVENUE float,
            D28REVENUE float,
            rownumber bigint(20),
            COST float
            );';
          $statement6 = $connect->prepare($query6);
          $statement6->execute();

          
          mkdir('../users/'.$email);
              copy("../snowflake/first.py",'../users/'.$email.'/first.py');
              copy("../snowflake/ltv.py",'../users/'.$email.'/ltv.py');
              copy("../snowflake/second.py",'../users/'.$email.'/second.py');
              copy("../snowflake/roas.py",'../users/'.$email.'/roas.py');
              copy("../snowflake/ltv2.py",'../users/'.$email.'/ltv2.py');

          $_SESSION["id"]       = $email; 
          $_SESSION["platform"] = $plat; 
          $_SESSION["name"]     = $name;
          $_SESSION["date"]     = $date;
          $_SESSION["is_payer"] = $is_payer;
          header("Location:../setting/CONFIGURATION.php");

      }


    
$first = $first1;
  
}

?>





<?php
include("TOPSIGNUP.php");
?>



<br/>             
                    
                        <div class="button-box" >
                                        <div id="btn" style="left:110px"></div>
                                        <button  type = "button" class="toggle-btn" style="font-size:100%"><a href="../login/LOGIN.php">Login</a></button>
                                        <button  type = "button" class="toggle-btn" style="font-size:100%">SignUp</button>
                        
                        </div>
         
                           
        <div id="signup" class="tabcontent4">
                         <br />
                             <!-- <div style="color:black; text-align:center;font-size: 35px;line-height: 0.4em">
                               <p><b>Sign Up</b></p>                           
                            </div> 
                           <br/> -->
    
                <form class="tab3"  action="" method="post" id="newsignup">
                               
                                <div >
                                      <?php if($first) { echo $first; } ?>
                                 </div>

                               <div style="color:black; width:100%;">
                                <label for="psw"><b>Name</b></label>
                                <input type="text" placeholder="Enter Your Name" id="newname" name="newname" required>
                                <label for="psw"><b>Email</b></label>
                                <input type="text" placeholder="Enter Email" id ="newemail"  name= "newemail" pattern ="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" required>
                                 
                                
                                <label for="psw"><b>Password</b></label>
                                
                                
                                <input type="password" placeholder="Enter Password" name="newpassword" id = "newpassword" required>                           
    
                                <input type="password" placeholder="Confirm Password" name="newpassword2" id = "newpassword2" required> 
                                <div><span id='messagepassconfirm'></span></div>
                               
                                <p style="font-size:14px;margin:1% 0 0 0;"><input type="checkbox" onclick="showpassword()" >&nbsp Show password</p>
                                <br/>

                                <label for="psw"><b>Platform</b></label>
                                <select class="tab3" style="color:black; width:100%;" id="platforms" name="platforms" required>
                                                <option value="" disabled selected >Choose Your Platform </option>
                                                <option value="deltadna">DeltaDNA</option>
                                                <option value="bigquery">BigQuery</option>
                                                <option value="snowflake">Snowflake</option>
                                </select>
                                <br/>
                                <hr class="tab2" style="border-color:orange;color:orange;font-size:14px;margin:2% 0 0 0;">
                                <br/>
                                <p style="font-size:14px;"> <input type="checkbox" name="termsandcond" style="float:left;" required> &nbsp By creating an account you agree to our <a target = "_blank" href="../policy/terms-and-conditions.php" style="color:dodgerblue">Terms & Privacy</a>.</p>
                                <button type="submit" >Sign Up</button>
                                
                          </div>      
                </form>      
        </div>
        <div class="bottom-left" style="position: fixed; bottom: 0; left: 0;"> <p style="font-size:75%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> </div> 

</div>


<?php
include("../BOTTOM.php");
?>