<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInit7a93031aedce0fe3fa1cfa18bf1045f6
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require '../vendor/composer/ClassLoader.php';
        }
    }

    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInit7a93031aedce0fe3fa1cfa18bf1045f6', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader();
        spl_autoload_unregister(array('ComposerAutoloaderInit7a93031aedce0fe3fa1cfa18bf1045f6', 'loadClassLoader'));

        $useStaticLoader = PHP_VERSION_ID >= 50600 && !defined('HHVM_VERSION') && (!function_exists('zend_loader_file_encoded') || !zend_loader_file_encoded());
        if ($useStaticLoader) {
            require_once '../vendor/composer/autoload_static.php';

            call_user_func(\Composer\Autoload\ComposerStaticInit7a93031aedce0fe3fa1cfa18bf1045f6::getInitializer($loader));
        } else {
            $map = require '../vendor/composer/autoload_namespaces.php';
            foreach ($map as $namespace => $path) {
                $loader->set($namespace, $path);
            }

            $map = require '../vendor/composer/autoload_psr4.php';
            foreach ($map as $namespace => $path) {
                $loader->setPsr4($namespace, $path);
            }

            $classMap = require '../vendor/composer/autoload_classmap.php';
            if ($classMap) {
                $loader->addClassMap($classMap);
            }
        }

        $loader->register(true);

        return $loader;
    }
}
