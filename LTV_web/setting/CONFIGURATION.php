<?php
session_start();
?>



<?php
include_once "TOPCONFIG.php";
?>
<!-- <div style ="background-color:#F0F0F0;"> -->

<?php
include('../conn.php');
if(isset($_SESSION["id"])){
         
        $email = $_SESSION["id"];
        $sql="select name, delta_username, delta_password,platform,snowflake_server  from login where emaillogin='$email'";
        $statement1 = $connect->prepare($sql);
        $statement1->execute();
        $result = $statement1->fetchAll();
        foreach($result as $row){
            $deltaname = $row["name"];
            $deltaemail = $row["delta_username"];
            $deltapass = $row["delta_password"];
            $plat = $row["platform"];
            $server = $row["snowflake_server"];
        }}?>
      <br/>
<div style="border: 3px solid #3fa19c;
      margin: auto;
      width: 99%;
      border: 3px solid #3fa19c;
      padding: 10px;
      box-shadow: 0 0 20px 9px #f0f7f7;
      background-color:#f0f7f7;">

<?php if($plat == "deltadna"){?>

     <div id="savedeltalogin"></div>
    <label style="margin-left:12px; "><b>DeltaDNA Account Configuration:</b></label>
        <form class="tab3" name="editeconfig1" id="editeconfig1" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;display: flex;flex-flow: row wrap;align-items: center;">
            
            <div id="delta" style="margin-left:12px;width:75%;display:inline;float:left;">
                    <input id="deltaemail" type="text" placeholder="Enter Username" value="<?php echo $deltaemail; ?>" style = "margin:0 0 0 0;width:25%;float:left;" required>
                    <input id="deltapass" type="password" placeholder="Enter Password"  value="<?php echo $deltapass; ?>" style = "margin:0 0 0 2.5%;width:25%;float:left;" required>
                    <!-- <p><input type="checkbox" onclick="showpassword2()" style ="margin:2% 0 0 2.5%"> Show password</p> -->
            </div>
                    
                    <input type ="submit"  value="Save" style="background-color:orange;"/>
            
        </form>
           <br />                                  
           
<div>
        <label style="margin-left:12px;"><b>Add Game:</b></label>

<?php } else if($plat == "bigquery"){ ?>

  <div id="keyuploadmsg"> </div>
<label style="margin-left:12px; "><b>BigQuery Account Configuration:</b></label>
            
         <?php
           $email     = $_SESSION["id"];
           $dir       = '../users/'.$email.'/';
           $files     = glob($dir.'*.{json,Json,JSON}', GLOB_BRACE); //scandir($dir);  
           $output    = '';

           foreach ($files as $filename) {        
            $output .= '<tr>
                        <td style="text-align:left;padding: 0 0 1.5% 1.5%;"><h4>' .basename($filename) .'</h4></td>
                        <td style="text-align:center;"><ul class="list-inline m-0" >
                               
                               <li class="list-inline-item">
                                    <button class="btn btn-primary btn-sm rounded-0" data-toggle="tooltip" data-placement="top" type="button" onclick="deleteJSON()" name="deleteJSON" id="deleteJSON" style="height:35px;width:35px;background-color:#f50737; display:flex; justify-content:center; align-items: center; box-shadow:  0 0 15px 0 #888;border:none;">
                                    <i class="fa fa-trash" style="color:#ffffff;font-size:18px;"></i></button>
                                </li>
                            </ul>

                        </td>';
                             
           } 
           
           if(empty($files) ){?>

        <form class="tab3" id="keyupload" name="keyupload" method="post" enctype="multipart/form-data" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;display: flex;flex-flow: row wrap;align-items: center;">
              <input type="hidden" name="MAX_FILE_SIZE" value="100000" />    
              <input type="file" name="fileToUpload" id="fileToUpload" style="margin:0 0 0 5%">
              <input type="submit" value="Upload Key" style="margin:0 0 0 60%">
        </form>
           <br />  

           <?php } else { ?>

            <form class="tab3" id="keyupload" name="keyupload" method="post" enctype="multipart/form-data" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;display: flex;flex-flow: row wrap;align-items: center;">
              <input type="hidden" name="MAX_FILE_SIZE" value="100000" />    
              <input type="file" name="fileToUpload" id="fileToUpload" style="margin:0 0 0 5%" disabled>
              <input type="submit" value="Upload Key" style="margin:0 0 0 60%">
        </form>
           <br /> 
         
           <?php }
           echo '     <table>
                      <thead>
                      <tr> 
                      </tr>
                      <thead>
                      <tbody>'  
                         .$output
                    .'</tbody>
                      <table>';
         ?>                                  
           
<div>
        <label style="margin-left:12px;"><b>Add Game:</b></label>

<?php } else if($plat == "snowflake"){ ?>

    <div id="savedflakelogin"></div>
<label style="margin-left:12px; "><b>Snowflake Account Configuration:</b></label>
        <form class="tab3" name="editeconfig2" id="editeconfig2" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;display: flex;flex-flow: row wrap;align-items: center;">
            
            <div id="flake" style="margin-left:12px;width:75%;display:inline;float:left;">
                    <input id="flakeemail" type="text"  title="Three letter country code" placeholder="Enter Username" value="<?php echo $deltaemail; ?>" style = "margin:0 0 0 0;width:25%;float:left;" required>
                    <input id="flakepass" type="password" placeholder="Enter Password"  value="<?php echo $deltapass; ?>" style = "margin:0 0 0 2.5%;width:25%;float:left;" required>
                    <!-- <p><input type="checkbox" onclick="showpassword3()" style ="margin:2% 0 0 2.5%"> Show password</p> -->
                    
                    <input id="flakeserver" type="text"  title="Three letter country code" placeholder="Enter Account Name" value="<?php echo str_replace('_ZEN_EMJ_','.',$server); ?>" style = "margin:1% 0 0 0;width:52.5%;float:left;" required>

            </div>
                    <input type ="submit"  value="Save" style = "margin:0 0 0 0;"/>
            
        </form>
           <br />                                  
           
<div>
        <label style="margin-left:12px;"><b>Add Game:</b></label>

<?php }?>


<table id="user_games" class="table table-bordered table-striped; tab3" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;table-layout:fixed;">
        <thead >
            <tr style="background-color:#3fa19c;box-shadow: 0 0 15px 0 #888;">
                        <th >Database Name</th>
                        <th >Table Name</th>
                        <th >Event Date</th>
                        <th >User ID</th>
                        <th >User Start Date</th>
                        <th >Platform</th>
                        <th >Country</th>
                        <th >Campaign</th>
                        <th >Acquisition Channel</th>
                        <th >IAP Revenue</th>
                        <th >Ads Revenue</th>
                        <th > <a href="#note" style='color:white;float:right;'><i class="fa fa-info-circle" style="font-size:150%"></i></a> Revenue Rate </th>
                        <th style="width:150px;">Add Game</th>                    
                        					
            </tr>
        </thead>
        <tbody style="background-color:white;box-shadow: 0 0 15px 0 #888;">
        <tr style="height:2px">
        <td contenteditable='true' id="data1" onclick="empty_td('data1')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data2" onclick="empty_td('data2')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data3" onclick="empty_td('data3')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data4" onclick="empty_td('data4')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data5" onclick="empty_td('data5')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data6" onclick="empty_td('data6')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data7" onclick="empty_td('data7')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data8" onclick="empty_td('data8')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data9" onclick="empty_td('data9')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data10" onclick="empty_td('data10')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td contenteditable='true' id="data11" onclick="empty_td('data11')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Optional"></td>
        <td contenteditable='true' id="data12" onclick="empty_td('data12')"><input disabled style="color:#d1c9c9; border:none; background-color:transparent; width:60px;" value="Type Here"></td>
        <td ><button type="button" name="add" id="add" 
             style="background-color:green; display:flex; justify-content:center; align-items: center; margin-left:14px;
               color:white; border:none; width:100px;height:20px;text-align:center;border-radius: 3px;box-shadow: 0 0 15px 0 #888;">Add</button>
             </td>
            
        </tr>
        </tbody>
  </table>

   <br/>
    <label style="margin-left:12px;"><b>Games Table:</b></label>
    <br/>

        <div id ="newgameadd"> </div>
    <table id="user_games" class="table table-bordered table-striped; tab3" style="box-shadow: 0 0 15px 0 #888;background-color:#f0f7f7; margin-left:12px;width:97.5%;table-layout:fixed;">
        <thead >
            <tr style="background-color:#3fa19c;box-shadow: 0 0 15px 0 #888;">
                        <th >Game Name</th>
                        <th >Table Name</th>
                        <th >Event Date</th>
                        <th >User ID</th>
                        <th >User Start Date</th>
                        <th >Platform</th>
                        <th >Country</th>
                        <th >Campaign</th>
                        <th >Acquisition Channel</th> 
                        <th >IAP Revenue</th>
                        <th >Ads Revenue</th>
                        <th >Revenue Rate</th>
                        <th style="width:150px;">Edite</th>                    
                        					
                  
        <div>
        <?php
        include('../conn.php');
        if(isset($_SESSION["id"])){
            $email = $_SESSION["id"];
            $query = "select * from games where email ='$email' order by game_name_short";
            $statement = $connect->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
            $output = '';
            foreach($result as $row){
                    $output .=  '<tr style="height:20px;">
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'1">' . $row["game_name_short"]     .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'2">' . $row["table_name"]          .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'3">' . $row["event_date"]          .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'4">' . $row["user_id"]             .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'5">' . $row["user_start_date"]     .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'6">' . $row["platform"]            .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'7">' . $row["country"]             .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'8">' . $row["campaign"]            .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'9">' . $row["acquisition_channel"] .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'10">' . $row["revenue"]            .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'11">' . $row["ads_revenue"]        .'</td>
                        <td contenteditable='.'true'.' id="'. $row["table_name2"].'12">' . $row["revenue_rate"]       .'</td>
                        <td style="display:none;"      id="'. $row["table_name2"].'13">' . $row["game_name"]          .'</td>
                        <td style="display:none;"      id="'. $row["table_name2"].'14">' . $row["table_name2"]        .'</td>
                        <td style="text-align:center;"><ul class="list-inline m-0" >
                               <li class="list-inline-item" >
                                   <button class="btn btn-primary btn-sm rounded-0" data-toggle="tooltip" data-placement="top" type="button" onclick="editgame('."'".$row["table_name2"]."'".')" name="edit" id="edit" style="height:35px;width:35px;background-color:orange; display:flex; justify-content:center; align-items: center; box-shadow:  0 0 15px 0 #888;">
                                   <i class="fa fa-save" style="color:#ffffff;font-size:16px;"></i></button>
                               </li>
                               <li class="list-inline-item">
                                    <button class="btn btn-primary btn-sm rounded-0" data-toggle="tooltip" data-placement="top" type="button" onclick="deletegame('."'".$row["table_name2"]."'".')" name="delete" id="delete" style="height:35px;width:35px;background-color:#f50737; display:flex; justify-content:center; align-items: center; box-shadow:  0 0 15px 0 #888;">
                                    <i class="fa fa-trash" style="color:#ffffff;font-size:18px;"></i></button>
                                </li>
                            </ul>

                        </td>
                        </tr> ';
                }

          echo $output;
            
        }        
        ?>
        
        </div>

        </tbody>
	</table>
</div>
</div> 

<div style="width: 100%; height: 212%;background-color:#f0f7f7;">
  <div style="font-size: 115%;margin: 0 10% 0 8%;">
    <br><br>

    <label style="margin-left:5%;"><h2>Account Configuration:</h2></label> 
    <div style = "margin-left:5.5%;">In your account configuration part, you can click inside any squire to add your account configuration details or edit them. Then press the save button.
       <ul style = "margin: 0.5% 0 0 3%;">
              <li style = "margin: 0.3% 0">If you have a <b>DeltaDNA</b> account then you need to fill in your DeltaDNA account <b>Email</b> and <b>Password</b>.</li>
              <li style = "margin: 0.3% 0">If you have a <b>BigQuery</b> account then you need to upload a <b>JSON Key</b> file that has all the games you want to add.</li>
              <li style = "margin: 0.3% 0">If you have a <b>Snowflake</b> account then you need to fill in your Snowflake account <b>Email</b> and <b>Password</b>, plus, your <b>Account Name</b> as the example below.
                  <br><br> <img src="SFE.png" alt="Account Example" style = "margin: 1% 0 0 15%; box-shadow: 0 0 15px 0 #888;"> <br><br>  </li>
       </ul>
    </div>
    <hr>
    <label style="margin-left:5%;"><h2>Add Game:</h2></label> 
    <div style = "margin-left:5.5%;">In your add game part, you need to fill in your game database name, table name, and columns names. Try to copy and paste all the names from the original source to not lose any name's structure. 
       <ul style = "margin: 0.5% 0 0 3%;">
              <li style = "margin: 0.3% 0">If you have a <b>DeltaDNA</b> account then you can find your database name in <b>setting > Direct SQL Access > Database</b>. Or you can find the database name directly as the example below.
              <br><br> <img src="DDNADE.png" alt="DDNADE Example" style = "margin: 1% 0 0 15%; box-shadow: 0 0 15px 0 #888;"> <br><br>  </li>
              <li style = "margin: 0.3% 0">If you have a <b>BigQuery</b> account then you can find your database name in <b>Select a project > ID</b>. Your project ID is your game database name.</li>
              <li style = "margin: 0.3% 0">If you have a <b>Snowflake</b> account then you can find your database name in <b>Databases > Database</b> followe by <b>dot</b> then the <b>Schema</b> name. Or you can find the database name directly as the example below.
              <br><br><img src="SFDE.png" alt="SFDE Example" style = "margin: 1% 0 0 15%; box-shadow: 0 0 15px 0 #888;"> <br><br> </li>
       </ul>
       
    </div>

    <hr>

    <label style="margin-left:5%;"><h2>Game Table:</h2></label> 
    <div style = "margin-left:5.5%;"> 
    In this part, you can see all your added games. You can modify the names by clicking inside any squire, then press the save button <img src="edit.png" alt="edit Example" style = "display: inline;">. 
    If you want to delete the game press the delete button <img src="delete.png" alt="delete Example" style = "display: inline;">.
    </div>

    <hr>
    <div style = "margin-left:5.5%;" id="note"> 
       <label><b><h3>Note:</h3></b></label> 
       <ul style = "margin: 0.5% 0 0 3%; list-style-type:decimal;">
          <li >Remove all the quotation marks ( single (‘ ’) or double(“ ”)) from the database name where exist.</li>
          <li>You can <b>NOT</b> modify the database name after adding the game details. If you add a wrong database name then delete the game from the <b>Games Table</b> and re-add the game.</li>
          <li>The <b>Revenue Rate</b> Field is the rate number to convert the revenue to the desired output. For example, if your revenue column is in cents then you have to fill the revenue rate by <b>0.01</b> to convert the revenue to the dollar. to keep the revenue column rate as it is, fill the revenue rate by <b>1</b>.</li>
      </ul>
      </div>

</div>
<div class="bottom-left" style="margin:10% 0 3% 0; bottom: 0; left: 0;"> <p style="font-size:75%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> </div>

</div>


<script>
   function deleteJSON(){     
     if (confirm("Are you sure you want to delete this KEY?") == true) {
     $.ajax({
      url:"deleteJSON.php",
      // method:"POST",
      // data:{game:game, game_name:game_name},
      success:function(data)
      {
        $("#keyuploadmsg").html(data);
        $(function() { $("#newgameadd").hide().fadeIn().delay(2000).fadeOut('slow');});
        setTimeout(function() {location.reload();}, 3000);         
      }
     

     });
     }  }

</script>



<script>
function empty_td(e){ 
    $('#'.concat(e)).empty();
} 
</script>


<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
</script>


<script>
 function deletegame(event){
     var game       = $('#'.concat(event).concat('1')).text();
     var game_name  = $('#'.concat(event).concat('13')).text();
     var table_name = $('#'.concat(event).concat('2')).text();
     
     if (confirm("Are you sure you want to delete this Game?") == true) {
     $.ajax({
      url:"deletegame.php",
      method:"POST",
      data:{game:game, game_name:game_name, table_name:table_name},
      success:function(data)
      {
        $("#newgameadd").html(data);
        $(function() { $("#newgameadd").hide().fadeIn().delay(2000).fadeOut('slow');});
        setTimeout(function() {location.reload();}, 3000);         
      }
     

     });
     }  }
</script>

<script>
function editgame(event){
     var game_name_short     = $('#'.concat(event).concat('1')).text();
     var table_name          = $('#'.concat(event).concat('2')).text();
     var event_date          = $('#'.concat(event).concat('3')).text();
     var user_id             = $('#'.concat(event).concat('4')).text();
     var user_start_date     = $('#'.concat(event).concat('5')).text();
     var platform            = $('#'.concat(event).concat('6')).text();
     var country             = $('#'.concat(event).concat('7')).text();
     var campaign            = $('#'.concat(event).concat('8')).text();
     var acquisition_channel = $('#'.concat(event).concat('9')).text();
     var revenue             = $('#'.concat(event).concat('10')).text();
     var adsrevenue          = $('#'.concat(event).concat('11')).text();
     var revenue_rate        = $('#'.concat(event).concat('12')).text();
     var game_name           = $('#'.concat(event).concat('13')).text();
     var table_name2         = $('#'.concat(event).concat('14')).text();
     var pattern             = new RegExp('[^.a-zA-Z0-9_-]'); //[.\'"~`!@#$%\^&*()-+={}[ ]|/\\:;<>,?]
     if (confirm("Are you sure you want to edit this Game?") == true) {
       if(pattern.test(game_name_short) ||
        pattern.test(table_name) ||
        pattern.test(event_date) ||
        pattern.test(user_id) ||
        pattern.test(platform) ||
        pattern.test(country) ||
        pattern.test(campaign) ||
        pattern.test(acquisition_channel) ||
        pattern.test(revenue) ||
        pattern.test(revenue_rate) ){
         alert("espicial character is not allow!!!!!!");
        }
      else if (game_name_short != '' && game_name != '' && table_name != '' && event_date != '' && user_id != '' && country != '' && campaign != '' && acquisition_channel != ''
      && user_start_date != '' && platform != '' && revenue != '' && revenue_rate != '' && !isNaN(revenue_rate))
      {
      $.ajax({
      url:"editgame.php",
      method:"POST",
      data:{table_name2:table_name2, game_name_short:game_name_short, game_name:game_name, table_name:table_name,event_date:event_date,user_id:user_id,country:country,campaign:campaign,
        acquisition_channel:acquisition_channel,user_start_date:user_start_date,platform:platform,revenue:revenue,revenue_rate:revenue_rate,adsrevenue:adsrevenue},
      success:function(data)
      {
        $("#newgameadd").html(data);
        $(function() { $("#newgameadd").hide().fadeIn().delay(2000).fadeOut('slow');});
        setTimeout(function() {location.reload();}, 3000); 
        
      }
    });
  }
  else if(isNaN(revenue_rate)){ 
    alert("Please provide the Revenue Rate as a number or put 1 as default.");
    location.reload();
      }
  else{
    alert("Please fill in all the fields before editing the game details.");
    location.reload();
  }
  
 }
};
</script>


</div>
<?php
include_once "../BOTTOM.php";
?>


	
			