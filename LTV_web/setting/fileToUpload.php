<?php
session_start();
include('../conn.php');

if(isset($_FILES['file']['name'],$_SESSION["id"])){

$filename     = $_FILES['file']['name'];
$email        = $_SESSION["id"];
$location    = '../users/'.$email.'/'.$filename;
$FileType = pathinfo($location,PATHINFO_EXTENSION);
$FileType = strtolower($FileType);
$valid_extensions = array("json");
$response = '
                <div class="alert" style="text-align:center;">
                <span class="closebtn" onclick="this.parentElement.style.display='."'none'".';">&times;</span> 
                <strong>Fail!</strong> The KEY was failed to upload.
                </div>';

if(in_array(strtolower($FileType), $valid_extensions)) {
    
    if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
       $response = '
                    <div class="alert success" style="text-align:center;">
                    <span class="closebtn" onclick="this.parentElement.style.display='."'none'".';">&times;</span> 
                    <strong>Success!</strong> The KEY was successfully uploaded.
                    </div>';
    }
 }

 echo $response;
 exit;
}


?>