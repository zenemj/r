<?php


//1. Import the PayPal SDK client
namespace Sample;

require '../vendor/autoload.php';

use Sample\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;

require '../payment/paypal-client.php';

$orderID = $_GET['orderID'];

class GetOrder {

  // 2. Set up your server to receive a call from the client
  public static function getOrder($orderId) {

    // 3. Call PayPal to get the transaction details
    $client = PayPalClient::client();
    $response = $client->execute(new OrdersGetRequest($orderId));
	
	// 4. Specify which information you want from the transaction details. For example:
  session_start();
  include('../conn.php');
  $date                  = date("Y-m-d");
  $email                 = $_SESSION["id"];
  $order_email           = $response->result->payer->email_address;
  $order_payer_id        = $response->result->payer->payer_id;
  $order_payer_name      = $response->result->payer->name->given_name;
  $order_payer_surname   = $response->result->payer->name->surname;
  $order_payer_country   = $response->result->payer->address->country_code;
  $order_create_time     = $response->result->create_time;
  $order_update_time     = $response->result->update_time;
  $orderID               = $response->result->id;
  $order_status          = $response->result->status;
  $order_item            = $response->result->purchase_units[0]->items[0]->name;
  $order_currency_code   = $response->result->purchase_units[0]->items[0]->unit_amount->currency_code;
  $order_value           = $response->result->purchase_units[0]->items[0]->unit_amount->value;
  $payment_id            = $response->result->purchase_units[0]->payments->captures[0]->id;
  $payment_status        = $response->result->purchase_units[0]->payments->captures[0]->status;
  $payment_currency_code = $response->result->purchase_units[0]->payments->captures[0]->amount->currency_code;
  $payment_value         = $response->result->purchase_units[0]->payments->captures[0]->amount->value;
  
       if($order_item === '12 months'){$is_payer = 12;}
  else if($order_item === '6 months' ){$is_payer = 6;}
  else if($order_item === '3 months' ){$is_payer = 3;}
  else if($order_item === '1 month'  ){$is_payer = 1;}
	$sql = 'insert into paypal_trans VALUES 
     ("'.$email.'","'.$order_email.'","'.$order_payer_id.'","'.$order_payer_name.'","'.$order_payer_surname
     .'","'.$order_payer_country.'","'.$order_create_time.'","'.$order_update_time.'","'.$orderID.'","'.$order_status
     .'","'.$order_item.'","'.$order_currency_code.'","'.$order_value.'","'.$payment_id.'","'.$payment_status
     .'","'.$payment_currency_code.'","'.$payment_value.'")';
  $statement = $connect->prepare($sql);
  $statement->execute();

  $sql2 = "update login set
           start_date = '$date',
           is_payer   =  '$is_payer' 
           where emaillogin = '$email' ";
  $statement2 = $connect->prepare($sql2);
  $statement2->execute();
	header("Location:../payment/success.php");
  }
}

if (!count(debug_backtrace())) {
  GetOrder::getOrder($orderID, true);
}
?>