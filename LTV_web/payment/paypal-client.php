<?php
namespace Sample;

require '../vendor/autoload.php';

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

class PayPalClient {
    public static function client() {
        return new PayPalHttpClient(self::environment());
    }

    public static function environment() {
        $clientId = "AWAyGdfMlMykWjo5n1kqiT5H6py7w9NNPBVtOyO1JXkc7dQ3vTEU8tNS_6HQGqaTNGg8mTOkVxz_5DZH";
        $clientSecret = "EA_6tuXgwioUz8dvHshf2RochHWnjSBJvg2dl2qus3hPFSF8dOYh3rMnf3GzzBJU534Bv8TK1DsglC-Q";
        return new ProductionEnvironment($clientId, $clientSecret);
    }
}

?>