<?php
session_start();
if(isset($_SESSION["id"])== FALSE){ header("Location:../login/LOGIN.php");}
include("TOPpay.php");
include('../conn.php');

$email= $_SESSION["id"];
$is_payer= $_SESSION["is_payer"];
$today = new DateTime("now");

$query = "select case when is_payer=1 then  DATE_ADD(start_date, INTERVAL 31 DAY)
                      when is_payer=3 then  DATE_ADD(start_date, INTERVAL 92 DAY) 
                      when is_payer=6 then  DATE_ADD(start_date, INTERVAL 183 DAY) 
                      when is_payer=12 then  DATE_ADD(start_date, INTERVAL 366 DAY) end as expier
                      from login where emaillogin='$email'";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row)
            {
              $date = new DateTime($row["expier"]);
            }
if($date > $today && $is_payer != 0) { echo "<div style='margin:6% 0 0 0;'>
                                                              <center>
                                                                <b style='font-size: 150%; font-family: Arial; color:#424242;'>
                                                                  Your account has already a subscription. Click <a target='_blank' href='../userSetting/userSetting.php'>here </a>
                                                                  to know more about your subscription
                                                                
                                                                </b>
                                                              </center>
                                                            </div>";
                                        }
 else {                                    
 ?>
<br><br>

<div style="align-items: center; display: flex; justify-content: center;">
  
<div id="smart-button-container" style="width:35%;">
      <div style="text-align: center;">
        <div class="form-group"  style="margin-bottom: 1.25rem;">
          <label style="float: left;font-size: 115%;" >Subscription Plans:</label><br/>
          <select class="form-control" id="item-options">
            <option value="" disabled selected style="text-align:center;">Select your option</option>
            <option value="12 months" price="999">12 months - 999 EUR</option>
            <option value="6 months" price="599">6 months - 599 EUR</option>
            <option value="3 months" price="350">3 months - 350 EUR</option>
            <option value="1 month" price="150">1 month - 150 EUR</option>
          </select>
          <select style="visibility: hidden" id="quantitySelect"></select>
        </div>
      <div id="paypal-button-container"></div>
      </div>
    </div>
</div>

<br><br><br>
<div style="font-size: 115%; align-items: center; display: flex; justify-content: center;">
<div style="width:55%;">
<center><b>Any subscription plan you choose and pay for it will be a one-time subscription. 
    Once your subscription plan is expired You need to re-select the subscription plan again and re-pay for it to re-subscribe 
 </b>
    <br><br> 
  
  </center>
  </div>
  <div class="bottom-left" style="position: fixed; bottom: 0; left: 0;"> <p style="font-size:65%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> </div> 
</div>

<?php } ?>


<script src="https://www.paypal.com/sdk/js?client-id=AWAyGdfMlMykWjo5n1kqiT5H6py7w9NNPBVtOyO1JXkc7dQ3vTEU8tNS_6HQGqaTNGg8mTOkVxz_5DZH&enable-funding=venmo&currency=EUR" data-sdk-integration-source="button-factory"></script>
    <script>
      function initPayPalButton() {
        var shipping = 0;
        var itemOptions = document.querySelector("#smart-button-container #item-options");
    var quantity = parseInt();
    var quantitySelect = document.querySelector("#smart-button-container #quantitySelect");
    if (!isNaN(quantity)) {
      quantitySelect.style.visibility = "visible";
    }
    var orderDescription = 'Subscription Plans:';
    if(orderDescription === '') {
      orderDescription = 'Item';
    }
    paypal.Buttons({
      style: {
        shape: 'rect',
        color: 'gold',
        layout: 'vertical',
        label: 'pay',
        
      },
      createOrder: function(data, actions) {
        var selectedItemDescription = itemOptions.options[itemOptions.selectedIndex].value;
        var selectedItemPrice = parseFloat(itemOptions.options[itemOptions.selectedIndex].getAttribute("price"));
        var tax = (0 === 0 || false) ? 0 : (selectedItemPrice * (parseFloat(0)/100));
        if(quantitySelect.options.length > 0) {
          quantity = parseInt(quantitySelect.options[quantitySelect.selectedIndex].value);
        } else {
          quantity = 1;
        }

        tax *= quantity;
        tax = Math.round(tax * 100) / 100;
        var priceTotal = quantity * selectedItemPrice + parseFloat(shipping) + tax;
        priceTotal = Math.round(priceTotal * 100) / 100;
        var itemTotalValue = Math.round((selectedItemPrice * quantity) * 100) / 100;

        return actions.order.create({
          purchase_units: [{
            description: orderDescription,
            amount: {
              currency_code: 'EUR',
              value: priceTotal,
              breakdown: {
                item_total: {
                  currency_code: 'EUR',
                  value: itemTotalValue,
                },
                shipping: {
                  currency_code: 'EUR',
                  value: shipping,
                },
                tax_total: {
                  currency_code: 'EUR',
                  value: tax,
                }
              }
            },
            items: [{
              name: selectedItemDescription,
              unit_amount: {
                currency_code: 'EUR',
                value: selectedItemPrice,
              },
              quantity: quantity
            }]
          }]
          ,
              "application_context" :  { 
              "shipping_preference":"NO_SHIPPING"
            }
        });
      },
      onApprove: function(data, actions) {
        return actions.order.capture().then(function(orderData) {
          
          // Full available details
          console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
          window.location = "../payment/paypal-transaction-complete.php?&orderID="+data.orderID;

          // Show a success message within this page, e.g.
          const element = document.getElementById('paypal-button-container');
          element.innerHTML = '';
          element.innerHTML = '<h3>Thank you for your payment!</h3>';

          // Or go to another URL:  actions.redirect('thank_you.html');

        });
      },
      onError: function(err) {
       return console.log(err).then(function(){
        var selectedItemPrice = 1;
        $.ajax({
          url: "../payment/updatePayment.php",
          method: "POST",
          data:{selectedItemPrice:selectedItemPrice},
          success: function (data) {
            $('#test').html(data);         
          }
        });
       });
      },
    }).render('#paypal-button-container');
  }
  initPayPalButton();
    </script>

<?php
include("../BOTTOM.php");
?>