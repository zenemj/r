<?php
session_start();
include('../conn.php');
if(isset($_POST["platform"],$_POST["country"],$_POST["campaign"],$_POST["revenue"],$_POST["adsrevenue"]
,$_POST["from_date"],$_POST["to_date"],$_POST["games"],$_SESSION["id"]))
{   
    $end       = $_POST["to_date"];
    $start     = $_POST["from_date"];
    $campaign  = join("','", $_POST["campaign"]);
    $platform  = join("','", $_POST["platform"]);
    $country   = join("','", $_POST["country"]);
    $email     = $_SESSION["id"];
    $editemail = str_replace(".", "_" , str_replace("@","_", $email));
    $games     = $_POST["games"];
    $iaprevenue= $_POST["revenue"];
    $adsrevenue= $_POST["adsrevenue"];

    
    $query = "SELECT * FROM games where game_name_short='$games' and email='$email'";
    $statement = $connect->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    foreach($result as $row)
    { $game_name = $row['game_name']; 
      $table_name = $row['table_name'];
      $event_date = $row['event_date'];
      $user_id = $row['user_id'];
      $user_start_date = $row['user_start_date'];
      $platforms = $row['platform'];
      $countrys = $row['country'];
      $campaigns = $row['campaign'];
      $acquisition_channel = $row['acquisition_channel'];      
      $revenue = $row['revenue'];
      $ads_revenue = $row['ads_revenue'];
      $revenue_rate = $row['revenue_rate'];
    }

    $payerrevenue = $revenue;

    if($iaprevenue == "checked" && $adsrevenue != "checked"){ $ads_revenue = '';} elseif($iaprevenue != "checked" && $adsrevenue == "checked"){ $revenue = '';}
 
    $query1 = "SELECT delta_username,delta_password, snowflake_server FROM login where emaillogin='$email'";
    $statement1 = $connect->prepare($query1);
    $statement1->execute();
    $result1 = $statement1->fetchAll();
    foreach($result1 as $row)
    { $delta_username = $row['delta_username']; 
      $delta_password = $row['delta_password']; 
      $snowflake_server = $row['snowflake_server'];     
    }
    
    $dir       = '../users/'.$email.'/';
    $files     = glob($dir.'*.{json,Json,JSON}', GLOB_BRACE); //scandir($dir);  
    $key    = '../users/'.$email.'/';
    foreach ($files as $filename) {        
            $key .= basename($filename);
    }
    
    $query = 'delete FROM '.$editemail.'_roasdata where INSTALLDATE is not null';
    $statement = $connect->prepare($query);
    $statement->execute();

    // echo $start.'" "'.$end.'" "'.$game_name.'" "'.$table_name.'" "'.$event_date.'" "'.$user_start_date.'" "'.$platform.'" "'.$platforms.'" "'.$country.'" "'.$countrys.'" "'.$campaign.'" "'.$campaigns.'" "'.$acquisition_channel.'" "'.$revenue.'" "'.$revenue_rate.'" "'.$delta_username.'" "'.$delta_password.'" "'.$email.'" "'.$user_id.'" "'.$key.'" "'.$snowflake_server.'" "'.$ads_revenue.'" "'.$payerrevenue.'"';
    
    if($snowflake_server ==""){
    $roas = shell_exec('python3 ../users/'.$email.'/roas.py "'.$start.'" "'.$end.'" "'.$game_name.'" "'.$table_name.'" "'.$event_date.'" "'.$user_start_date.'" "'.$platform.'" "'.$platforms.'" "'.$country.'" "'.$countrys.'" "'.$campaign.'" "'.$campaigns.'" "'.$acquisition_channel.'" "'.$revenue.'" "'.$revenue_rate.'" "'.$delta_username.'" "'.$delta_password.'" "'.$email.'" "'.$user_id.'" "'.$key.'" "'.$snowflake_server.'" "'.$ads_revenue.'" "'.$payerrevenue.'"');
    }
    else{
    $roas = shell_exec('python ../users/'.$email.'/roas.py "'.$start.'" "'.$end.'" "'.$game_name.'" "'.$table_name.'" "'.$event_date.'" "'.$user_start_date.'" "'.$platform.'" "'.$platforms.'" "'.$country.'" "'.$countrys.'" "'.$campaign.'" "'.$campaigns.'" "'.$acquisition_channel.'" "'.$revenue.'" "'.$revenue_rate.'" "'.$delta_username.'" "'.$delta_password.'" "'.$email.'" "'.$user_id.'" "'.$key.'" "'.$snowflake_server.'" "'.$ads_revenue.'" "'.$payerrevenue.'"');
    }
    
    $roasdata = 'INSERT INTO '.$editemail.'_roasdata VALUES '.$roas;
    $statement2 = $connect->prepare($roasdata);
    $statement2->execute();

    $sql = 'SELECT INSTALLDATE, ACQUISITION_CHANNEL as ACQUISITIONCHANNEL, 
                   sum(USERS) as TOTALUSERS, 
                   sum(PAYERS) as TOTALPAYERS, 
                   round(sum(D0REVENUE),2) as D0REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 1 then 0 else round(sum(D1REVENUE),2) end as D1REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 2 then 0 else round(sum(D2REVENUE),2) end as D2REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 3 then 0 else round(sum(D3REVENUE),2) end as D3REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 4 then 0 else round(sum(D4REVENUE),2) end as D4REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 5 then 0 else round(sum(D5REVENUE),2) end as D5REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 6 then 0 else round(sum(D6REVENUE),2) end as D6REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 7 then 0 else round(sum(D7REVENUE),2) end as D7REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 14 then 0 else round(sum(D14REVENUE),2) end as D14REVENUE, 
                   case when DATEDIFF(CURRENT_DATE,INSTALLDATE) < 30 then 0 else round(sum(D28REVENUE),2) end as D28REVENUE 
                   FROM '.$editemail.'_roasdata group by 1,2 order by 1,2';
    $statement = $connect->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll();
    $output = '';
    foreach($result as $row){
                                $output .=  '{<tr style="height:20px;font-weight: 700;">
                                    <td style="text-align:left;padding-left: 6px;" >' . $row["INSTALLDATE"]. '</td>
                                    <td style="text-align:left;padding-left: 6px;">' . $row["ACQUISITIONCHANNEL"] . '</td>
                                    <td style="text-align:right;padding-right: 6px;">'. $row["TOTALUSERS"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["TOTALPAYERS"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D0REVENUE"] . '</td>
                                    <td style="text-align:right;padding-right: 6px;">'. $row["D1REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D2REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D3REVENUE"] . '</td>
                                    <td style="text-align:right;padding-right: 6px;">'. $row["D4REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D5REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D6REVENUE"] . '</td>
                                    <td style="text-align:right;padding-right: 6px;">'. $row["D7REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D14REVENUE"]. '</td>
                                    <td style="text-align:right;padding-right: 6px;">' . $row["D28REVENUE"] . '</td>
                                    </tr> }';
                            }
        
    echo   '            <thead>
                            <tr style="height:26px;">
                                <th style="width:76px;">Install Date</th>
                                <th>Acquisition Channel</th>
                                <th>New Users</th>
                                <th>Total Payers</th>
                                <th>D0 Revenue</th>
                                <th>D1 Revenue</th>
                                <th>D2 Revenue</th>
                                <th>D3 Revenue</th>
                                <th>D4 Revenue</th>
                                <th>D5 Revenue</th>
                                <th>D6 Revenue</th>
                                <th>D7 Revenue</th>
                                <th>D14 Revenue</th>
                                <th>D30 Revenue</th>
                            </tr>
                    
                        </thead>

                        <tbody>'        
                         .$output
                        .'<tbody>';
        
        // echo $roas;
}

?> 