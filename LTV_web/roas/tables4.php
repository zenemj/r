<?php
session_start();
include('../conn.php');

if(isset($_POST["country"],$_POST["selected"],$_SESSION["id"]))
{

    $AC        = join('","', $_POST["selected"]); 
    $email     = $_SESSION["id"];
    $editemail = str_replace(".", "_" , str_replace("@","_", $email));

if (strpos($AC, 'None') == TRUE){
    $sql1 = ' DELETE FROM '.$editemail.'_campaignroas WHERE USERS IS NOT NULL;
    INSERT INTO '.$editemail.'_campaignroas'
        .' SELECT CASE WHEN CAMPAIGN IS NULL OR CAMPAIGN ="" or lower(CAMPAIGN) like "%none%" THEN " None" ELSE CAMPAIGN END AS CAMPAIGN,
                        max(INSTALLDATE) as MAX_DATE,   
                        sum(USERS) as USERS, 
                        sum(PAYERS) as PAYERS, 
                        sum(D0REVENUE) as D0REVENUE, 
                        sum(D1REVENUE) as D1REVENUE, 
                        sum(D2REVENUE) as D2REVENUE, 
                        sum(D3REVENUE) as D3REVENUE, 
                        sum(D4REVENUE) as D4REVENUE, 
                        sum(D5REVENUE) as D5REVENUE, 
                        sum(D6REVENUE) as D6REVENUE, 
                        sum(D7REVENUE) as D7REVENUE, 
                        sum(D14REVENUE) as D14REVENUE, 
                        sum(D28REVENUE) as D28REVENUE,
                        (ROW_NUMBER() OVER()) as rownumber,
                        0 AS COST
                        FROM '.$editemail.'_roasdata 
                        where CAMPAIGN in ("'.$AC.'") or CAMPAIGN IS NULL OR CAMPAIGN = "" or lower(CAMPAIGN) like "%none%" group by 1 order by 1;';
    $statement1 = $connect->prepare($sql1);
    $statement1->execute();
    $statement1->closeCursor();
    
}else{
    $sql1 = ' DELETE FROM '.$editemail.'_campaignroas WHERE USERS IS NOT NULL;
        INSERT INTO '.$editemail.'_campaignroas'
            .' SELECT CASE WHEN CAMPAIGN IS NULL OR CAMPAIGN ="" THEN " None" ELSE CAMPAIGN END AS CAMPAIGN,  
                            max(INSTALLDATE) as MAX_DATE, 
                            sum(USERS) as USERS, 
                            sum(PAYERS) as PAYERS, 
                            sum(D0REVENUE) as D0REVENUE, 
                            sum(D1REVENUE) as D1REVENUE, 
                            sum(D2REVENUE) as D2REVENUE, 
                            sum(D3REVENUE) as D3REVENUE, 
                            sum(D4REVENUE) as D4REVENUE, 
                            sum(D5REVENUE) as D5REVENUE, 
                            sum(D6REVENUE) as D6REVENUE, 
                            sum(D7REVENUE) as D7REVENUE, 
                            sum(D14REVENUE) as D14REVENUE, 
                            sum(D28REVENUE) as D28REVENUE,
                            (ROW_NUMBER() OVER()) as rownumber,
                            0 AS COST                            
                            FROM '.$editemail.'_roasdata 
                            where CAMPAIGN in ("'.$AC.'") group by 1 order by 1;';
        $statement1 = $connect->prepare($sql1);
        $statement1->execute();
        $statement1->closeCursor();
              
    }  
    
$sql = 'SELECT CAMPAIGN, 
            USERS as TOTALUSERS, 
            PAYERS as TOTALPAYERS, 
            CONCAT(ROUND(D0REVENUE*COST,2),"%") as D0REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 1 then "Na" else CONCAT(ROUND(D1REVENUE*COST,2),"%") end as D1REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 2 then "Na" else CONCAT(ROUND(D2REVENUE*COST,2),"%") end as D2REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 3 then "Na" else CONCAT(ROUND(D3REVENUE*COST,2),"%") end as D3REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 4 then "Na" else CONCAT(ROUND(D4REVENUE*COST,2),"%") end as D4REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 5 then "Na" else CONCAT(ROUND(D5REVENUE*COST,2),"%") end as D5REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 6 then "Na" else CONCAT(ROUND(D6REVENUE*COST,2),"%") end as D6REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 7 then "Na" else CONCAT(ROUND(D7REVENUE*COST,2),"%") end as D7REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 14 then "Na" else CONCAT(ROUND(D14REVENUE*COST,2),"%") end as D14REVENUE, 
            case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 30 then "Na" else CONCAT(ROUND(D28REVENUE*COST,2),"%") end as D28REVENUE,
            COST 
            FROM '.$editemail.'_campaignroas ORDER BY 1';

$statement = $connect->prepare($sql);
$statement->execute();
$result = $statement->fetchAll();
$output = '';

foreach($result as $row){

                        $output .=  '{<tr style="height:20px;font-weight: 700;">
                            <td> <input type="number" id="CostValue4" name="CostValue4" min="0" step="0.01" style="width: 5em" autocomplete="off" value='.$row["COST"].'> </td>
                            <td style="text-align:left;padding-left: 6px;" >'. $row["CAMPAIGN"]. '</td>
                            
                            <td style="text-align:right;padding-right: 6px;">'. $row["TOTALUSERS"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["TOTALPAYERS"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D0REVENUE"] . '</td>
                            <td style="text-align:right;padding-right: 6px;">'. $row["D1REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D2REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D3REVENUE"] . '</td>
                            <td style="text-align:right;padding-right: 6px;">'. $row["D4REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D5REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D6REVENUE"] . '</td>
                            <td style="text-align:right;padding-right: 6px;">'. $row["D7REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D14REVENUE"]. '</td>
                            <td style="text-align:right;padding-right: 6px;">' . $row["D28REVENUE"] . '</td>
                            </tr> }';

                           
                    }



echo   '            <thead>
                    <tr style="height:26px;">
                        <th style="text-align:left;padding-left: 4px;">Campaign Cost</th>
                        <th style="text-align:left;padding-left: 4px;">Campaign</th>
                        
                        <th>Total Users</th>
                        <th>Total Payers</th>
                        <th>D0 ROAS</th>
                        <th>D1 ROAS</th>
                        <th>D2 ROAS</th>
                        <th>D3 ROAS</th>
                        <th>D4 ROAS</th>
                        <th>D5 ROAS</th>
                        <th>D6 ROAS</th>
                        <th>D7 ROAS</th>
                        <th>D14 ROAS</th>
                        <th>D30 ROAS</th>
                    </tr>
            
                </thead>

                <tbody>'        
                    .$output
                .'</tbody>';


}

?> 