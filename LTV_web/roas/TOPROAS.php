<?php
session_start();
if(isset($_SESSION["id"])== FALSE){ header("Location:../login/LOGIN.php");}
?>

<!DOCTYPE html>
<html lang="en">
  <head>

  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <!-- <meta charset="utf-8"> -->
  <!-- <link rel="stylesheet" href="bootstrap.min.css"> -->
  <!-- <script src="jquery.min.js"></script> -->
  <!-- <script src="bootstrap.min.js"></script> -->
  <script src="bootstrap3-typeahead.min.js"></script> 
  <!-- <script src="bootstrap-multiselect.js"></script> -->
  <!-- <link rel="stylesheet" href="bootstrap-multiselect.css" /> -->
  <link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
  <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
  <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
  <!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>
  


  <link type="text/css" rel="stylesheet" href="http://fakedomain.com/smilemachine/html.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="styles.css?v=<?php echo time();?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


 
 <style>

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

div { width: auto; height: auto; min-width: auto; min-height: auto; }

.topnav {
  overflow: hidden;
  background-color: #4790ba;
}

.topnav a {
  float: left;
  color: #383838;
  text-align: center;
  padding: 0.75% 3% 0% 5%;
  text-decoration: none;
  font-size: 125%;
}

.topnav a:hover {
  /* background-color: #ddd; */
  color: #dadee3;
}

.topnav a.active {
  /* background-color: #04AA6D; */
  color: #f2f2f2;
}
</style>
 

</style>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>



<style>
.overlay{
    display: none;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 999;
    background: rgba(255,255,255,0.8) url("loader.gif") center no-repeat;
}
/* Turn off scrollbar when body element has the loading class */
body.loading{
    overflow: hidden;   
}
/* Make spinner image visible when body element has the loading class */
body.loading .overlay{
    display: block;
}
</style>

<script>
// Initiate an Ajax request on button click
$(document).on( "button", function(){
    $.get(function(data){
        $("body").html(data);
    });       
});
 
// Add remove loading class on body element based on Ajax request status
$(document).on({
    ajaxStart: function(){
        $("body").addClass("loading"); 
    },
    ajaxStop: function(){ 
        $("body").removeClass("loading"); 
    }    
});
</script>

<style>
.center {
  margin: auto;
  width: 60%;
  border: 3px solid gray;
  padding: 10px;
}
</style>



<style>
input[name='weeks']:after {
        width: 12.7px;
        height: 12.7px;
        border-radius: 12.7px;
        top: 0px;
        left: 0.5px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[name='weeks']:checked:after {
        width: 12.7px;
        height: 12.7px;
        border-radius: 12.7px;
        top: 0px;
        left: 0.5px;
        position: relative;
        background-color: green;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
</style>

<style>
.table {
border-collapse: separate;
width: 100%;
color: #2d2d2e;
font-size: 12px;
/*font-family: monospace;
/*text-align: right;
border-spacing:10px 10px;*/
}
th {
background-color: #4790ba;
color: white;
text-align: center;
font-size:10.5px;
}
tr:nth-child(even) {background-color: #cedeed}
</style>

<style>
body {font-family: Arial;}

/* Style the tab */
.tab2 {
  overflow: hidden;
  
  
  /* border: 1px solid #ccc; */
  background-color: #F0F0F0; /*#f1f1f1;*/
  
}

/* Style the buttons inside the tab */
.tab2 button {
  background-color: #4790ba;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 14px;
  border-radius: 5.5% 5.5% 0 0;
  height:15px;
  width:24.78% ;
  display: table-cell;
  vertical-align: middle;
  padding-left:8%;
  text-align: center;
  display:flex; 
  align-items:center;
   /* vertical-align:middle;   */
  margin: 1% 0 0 0 ;
  color:white;
  border-top: 1px solid #ccc;
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  font-weight:500; 
 
  
  
}


/* Change background color of buttons on hover */
.tab2 button:hover {
  background-color: #4790ba;
  
}

/* Create an active/current tablink class */
.tab2 button.active {
  background-color: white;
  color:#4790ba;
  box-shadow: 0 0 15px 0 #888;
  
}

/* .tab2 button a{
    color:#fff!important; 
    font-weight:normal!important; 
    text-decoration:none;
    padding-left:27px;
    text-shadow:none!important;
} */

/* Style the tab content */
.tabcontent2 {
  display: none;
  padding: 6px 12px;
  border-bottom: 1px solid #ccc;
  border-left: 1px solid #ccc;
  border-right: 1px solid #ccc;
  border-top: none;
  color:black;
  background-color: white;
  width:82%;
  margin-left:17.3%;
}

</style>



<style>
body {font-family: Arial, Helvetica, sans-serif;}
.tab3 {border: 3px solid orange;
      margin: auto;
      width: 30%;
      border: 3px solid orange;
      padding: 10px;
}

.tab3 input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid orange;
  box-sizing: border-box;
}

.tab3 select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid orange;
  box-sizing: border-box;
}

.tab3 button {
  background-color: orange;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

.tab3 button:hover {
  opacity: 0.8;
}

.tab3.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* .tab3.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

.tab3 img.avatar {
  width: 40%;
  border-radius: 50%;
} */

.tab3.container {
  padding: 16px;
}

.tab3 span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}

</style>

<style>
.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #04AA6D;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>

  
</head>

<body>



<div class="topnav">
  <?php 
 if(isset($_SESSION["id"])){
   echo '
   <a href="../index.php" style="padding: 0% 0% 0% 0%; width:13%;"> <img src="6-01.png" alt="logo" width="80%" height="80%" style="margin-left:2%; float:left;"></a>
   <a href="../ltv/LTV.php" style="padding: 0.75% 5% 0% 5%; "> Estimate LTV & ROAS</a>
   <a href="../roas/ROAS.php" style="padding: 0.75% 5% 0% 0%; color:#f2f2f2;"> ROAS</a>
   <a href="../setting/CONFIGURATION.php" style="padding: 0.75% 5% 0% 0%; "> Add Games</a>
   <a href="../sampleSize/sample.php" style="padding: 0.75% 5% 0% 0%; "> Sample Size</a>
   <a target = "_blank" href="../game/snake/index.html" style="padding: 0.75% 5% 0% 0%;"> Snake</a>
   <a href="../logout/LOGOUT.php" style="float:right; padding: 0.75% 2% 0% 0%;"> Log out</a>
   <a href="../payment/payment.php" style="float:right; padding: 0.75% 2.5% 0% 0%;"> Subscribe</a>';
 }
 else{
   header("Location:../login/LOGIN.php");
 }
?>
</div>

