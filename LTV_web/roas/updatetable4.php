<?php
session_start();
include('../conn.php');

if(isset($_POST["variable"],$_SESSION["id"]))
{

    $data = $_POST["variable"];
    $email     = $_SESSION["id"];
    $editemail = str_replace(".", "_" , str_replace("@","_", $email));

    foreach ($data as $key => $value) {
    $query = 'UPDATE '.$editemail.'_campaignroas set COST = '.$value.' where rownumber = '.$key.'+1';
    $statement = $connect->prepare($query);
    $statement->execute();
    }


    $sql = 'SELECT CAMPAIGN, 
                USERS as TOTALUSERS, 
                PAYERS as TOTALPAYERS, 
                CONCAT(ROUND(D0REVENUE*100/COST,2),"%") as D0REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 1 then "Na" else CONCAT(ROUND(D1REVENUE*100/COST,2),"%") end as D1REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 2 then "Na" else CONCAT(ROUND(D2REVENUE*100/COST,2),"%") end as D2REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 3 then "Na" else CONCAT(ROUND(D3REVENUE*100/COST,2),"%") end as D3REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 4 then "Na" else CONCAT(ROUND(D4REVENUE*100/COST,2),"%") end as D4REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 5 then "Na" else CONCAT(ROUND(D5REVENUE*100/COST,2),"%") end as D5REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 6 then "Na" else CONCAT(ROUND(D6REVENUE*100/COST,2),"%") end as D6REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 7 then "Na" else CONCAT(ROUND(D7REVENUE*100/COST,2),"%") end as D7REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 14 then "Na" else CONCAT(ROUND(D14REVENUE*100/COST,2),"%") end as D14REVENUE, 
                case when DATEDIFF(CURRENT_DATE,MAX_DATE) < 30 then "Na" else CONCAT(ROUND(D28REVENUE*100/COST,2),"%") end as D28REVENUE,
                COST 
                FROM '.$editemail.'_campaignroas ORDER BY 1';

            $statement = $connect->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();
            $output = '';
            
    foreach($result as $row){

            $output .=  '{<tr style="height:20px;font-weight: 700;">
                <td> <input type="number" id="CostValue4" name="CostValue4" step="0.01" style="width: 5em" autocomplete="off" value='.$row["COST"].'> </td>
                
                <td style="text-align:left;padding-left: 6px;">' . $row["CAMPAIGN"] . '</td>
                <td style="text-align:right;padding-right: 6px;">'. $row["TOTALUSERS"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["TOTALPAYERS"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D0REVENUE"] . '</td>
                <td style="text-align:right;padding-right: 6px;">'. $row["D1REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D2REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D3REVENUE"] . '</td>
                <td style="text-align:right;padding-right: 6px;">'. $row["D4REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D5REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D6REVENUE"] . '</td>
                <td style="text-align:right;padding-right: 6px;">'. $row["D7REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D14REVENUE"]. '</td>
                <td style="text-align:right;padding-right: 6px;">' . $row["D28REVENUE"] . '</td>
                </tr> }';

              
            }



    echo   '            <thead>
            <tr style="height:26px;">
            <th style="text-align:left;padding-left: 4px;">Campaign Cost</th>
            <th style="text-align:left;padding-left: 4px;">Campaign</th>
            
            <th>Total Users</th>
            <th>Total Payers</th>
            <th>D0 ROAS</th>
            <th>D1 ROAS</th>
            <th>D2 ROAS</th>
            <th>D3 ROAS</th>
            <th>D4 ROAS</th>
            <th>D5 ROAS</th>
            <th>D6 ROAS</th>
            <th>D7 ROAS</th>
            <th>D14 ROAS</th>
            <th>D30 ROAS</th>
            </tr>

            </thead>

            <tbody>'        
            .$output
            .'</tbody>';

}


?>