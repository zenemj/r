<?php
session_start();
include("TOPROAS.php");
?>

<div  style ="width: 100%; height: 100%; background-color:#F0F0F0; color:white;">
  <div class="container" style="float:left;width:17%; height:100%; margin-left:-1px; margin-right:5px; color:white;background-color:#383838;" >
                     <br />     
                    <div  id="daterang2">
        
                            <div class="form-group">
                            <label >Players Start Dates:</label><br />
                            <input  onfocus="(this.type='date')" onblur="(this.type='text')" type="text" name="from_date2" id="from_date2" class="form-control dateFilter2" placeholder="From Date" autocomplete="off" max="<?=date('Y-m-d',strtotime('now'));?>"/>
                            </div>  

                            <div class="form-group">
                                <input  onfocus="(this.type='date')" onblur="(this.type='text')" type="text" name="to_date2" id="to_date2"     class="form-control dateFilter2" placeholder="To Date"   autocomplete="off" max="<?=date('Y-m-d',strtotime('now'));?>"/>
                            </div>

                     </div>     
    
                    <div class="form-group" >
                        
                        <label >Game:</label><br />
                        <select class="form-control" name="games2" id="games2" style="text-align:center;">
                            <?php
                                session_start();
                                include('../conn.php');
                                if(isset($_SESSION["id"]))
                                {    
                                    $email = $_SESSION["id"];
                                    $query = "SELECT game_name_short from games where email = '$email' group by game_name_short order by game_name_short";
                                    $statement = $connect->prepare($query);
                                    $statement->execute();
                                    $result = $statement->fetchAll();
                                    $output = '';
                                    foreach($result as $row)
                                    {
                                        $output .='<option value="'.$row['game_name_short'].'">'.$row['game_name_short'].'</option>';
                                    }
                                    echo $output;
                                }
                            ?>
                        
                        </select>
                    
                    </div>    
    
                    <div class="form-group" >

                        <input type="submit" name="btn_search2" id="btn_search2" value="Collect Dates Data" class="btn btn-primary"/>
                    </div> 

                    <div class="form-group" >
                        
                        <label >Platform:</label><br />
                        <select name="platform2[]" id="platform2" multiple>
                        
                        </select>
                    
                    </div>

                    <div class="form-group" >
                        <label >Campaign:</label><br />
                        <select name="campaign2[]" id="campaign2" multiple class="form-control" >
                        
                        </select>
                    </div>

                    <div class="form-group">
                        <label >Country:</label><br />
                        <select name="country2[]" id="country2" multiple class="form-control">
                        
                        </select>
                    </div>

                    <div class="form-group">
                         <input type="checkbox" name = "revenueselectiap2" id="iap_revenue_roas" value ="checked" checked /> &nbsp IAP Revenue<br />
                         <input type="checkbox" name = "revenueselectads2" id="ads_revenue_roas" value ="checked" /> &nbsp ADS Revenue<br />
                    </div>

                    <div class="form-group">        
                        <input type ="submit" name="btn_LTV2" id="btn_LTV2" value="Calculate The ROAS" class="btn btn-primary" />
                    </div>   

                    <div style="position: relative; bottom: 0; left:-1%; color:white; margin-top:260%; width:100%"> <p style="font-size:70%;white-space: nowrap;">Copyright © 2022 MANDSLTV | All Rights Reserved</p> </div> 

      
    </div >
             
    <div style ="background-color: #F0F0F0;">
                            <div style="float:right;margin-left:25px;color:red;"> <p id ="expire" > </p> </div> 
                            <div style="display:inline-block;vertical-align:top; position: relative; height: 30px;"></div>
                            <div class="tab2">
                                    <button class="tablinks" onclick="roastables(event, 'Revenue')"  id="defaultOpen2" name="defaultOpen2" > Revenue </button>
                                    <button class="tablinks" onclick="roastables(event, 'DailyROAS')"  id="table2" name = "table2">Daily ROAS</button>
                                    <button class="tablinks" onclick="roastables(event, 'WeeklyROAS')"  id="table3" name = "table3">Weekly ROAS</button>
                                    <button class="tablinks" onclick="roastables(event, 'CampaignROAS')"  id="table4" name = "table4">Campaigns ROAS</button>
                            </div>   
                            <div id="clear2"> <input type="hidden" id="clear3" name="clear3" value= "0"> </div>
                            <div id="Revenue" class="tabcontent2" style="height:100%;box-shadow:  0 0 15px 0 #888;">

                                <h4 id="please" style="float:left;color:#4790ba;">Plese select your filter.</h4>
                                <input type ="submit" name="downloadRevenue" id="downloadRevenue" value="Download Data" class="btn btn-primary" style="float:right; background-color:#4790ba;border-color:#4790ba;box-shadow: 0 0 10px 0 #888;"/>
                                <br/><br/>
                                <table id="tables" name = "tables" class="table" >
                                     
                                </table>
                                <br/>
                                <!-- <div id = "test"> </div> -->
                                <!-- <div id = "test">hello world</div> -->
                            </div>

                            <div id="DailyROAS" class="tabcontent2" style="height:100%;box-shadow: 0 0 15px 0 #888;">
                            <input type ="submit" name="downloadDailyROAS" id="downloadDailyROAS" value="Download Data" class="btn btn-primary" style="float:right; background-color:#4790ba;border-color:#4790ba;box-shadow: 0 0 10px 0 #888;"/>

                                <label >Acquisition Channel:</label><br />
                                <select name="AcquisitionChannel[]" id="AcquisitionChannel"  multiple class="form-control">
                                
                                </select>
                                <br/>
                                <input type ="submit" name="Update" id="Update" value="Update Table" style="margin: 0.75% 0 0 0;"/>
                                <h6 style="display: inline-block;">Press Update Table Button after filling in the cost values inside the table.</h6>
                                
                                <br /><br />

                                <table id="tables2" name = "tables2" class="table" >
                                
                                </table>

                                
                            </div>


                            <div id="WeeklyROAS" class="tabcontent2" style="height:100%;box-shadow:  0 0 15px 0 #888;">
                            <input type ="submit" name="downloadWeeklyROAS" id="downloadWeeklyROAS" value="Download Data" class="btn btn-primary" style="float:right; background-color:#4790ba;border-color:#4790ba;box-shadow: 0 0 10px 0 #888;"/>

                                <label >Acquisition Channel:</label><br />
                                <select name="AcquisitionChannel3[]" id="AcquisitionChannel3" multiple class="form-control" >
                                
                                </select>
                                <br/>
                                <input type ="submit" name="Update3" id="Update3" value="Update Table" style="margin: 0.75% 0 0 0;"/>
                                <h6 style="display: inline-block;">Press Update Table Button after filling in the cost values inside the table.</h6>
                                
                                <br /><br />

                                <table id="tables3" name = "tables3" class="table" >
                                
                                </table>
                            </div>

                            <div id="CampaignROAS" class="tabcontent2" style="height:100%;box-shadow: 0 0 15px 0 #888;">
                            <input type ="submit" name="downloadCampaignROAS" id="downloadCampaignROAS" value="Download Data" class="btn btn-primary" style="float:right; background-color:#4790ba;border-color:#4790ba;box-shadow: 0 0 10px 0 #888;"/>

                                <label >Campaigns:</label><br />
                                <select name="AcquisitionChannel4[]" id="AcquisitionChannel4" multiple class="form-control" >
                                
                                </select>
                                <br/>
                                <input type ="submit" name="Update4" id="Update4" value="Update Table" style="margin: 0.75% 0 0 0;"/>
                                <h6 style="display: inline-block;">Press Update Table Button after filling in the cost values inside the table.</h6>

                                <br /><br />
                                <table id="tables4" name = "tables4" class="table" >
                                
                                </table>
                            </div>


    </div>
       
</div>

<div class="overlay center"  style="float:center; width:17%; height:100%; margin-top:0px; margin-right:5px; color:black; text-align:center;">
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <h2>    Please wait...</h2>
                
<?php
include("../BOTTOM.php");
?>