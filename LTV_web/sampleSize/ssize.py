import sys
from statistics import NormalDist


confidenceIntv      = float(sys.argv[1])
marginErr           = float(sys.argv[2])
estimatedProportion = float(sys.argv[3])
KPI                 = sys.argv[5]
if KPI == "Retention":
    rate = int(sys.argv[4])
elif KPI == "Conversion":
    rate = float(sys.argv[4])
elif KPI == "LTV":
    rate = float(sys.argv[4])
elif KPI == "General":
    rate = 1

if KPI == "Retention":
    payers = 1
elif KPI == "Conversion" and int(sys.argv[6]) != 0:
    payers = int(sys.argv[6])
elif KPI == "LTV" and int(sys.argv[6]) != 0:
    payers = int(sys.argv[6])
elif KPI == "General":
    payers = 1
else:
    payers = 16.005

z                   = NormalDist().inv_cdf((1 + confidenceIntv/100) / 2.)
sample              = round( (z**2)*(estimatedProportion/100)*(1-estimatedProportion/100)/((marginErr/100)**2) )

if KPI == "Retention":
    payers = 1
elif KPI == "Conversion" and int(sys.argv[6]) != 0:
    payers = int(sys.argv[6])
elif KPI == "LTV" and int(sys.argv[6]) != 0:
    payers = int(sys.argv[6])
elif KPI == "General":
    payers = 1
else:
    payers = sample*0.015

retention           = [100*0.4114918603, 31.08204401, 26.37733462, 23.47782673, 21.45015401,
                                    19.92412377, 18.71890074, 17.73398004, 16.90832429, 16.20237714,
                                    15.58919544, 15.04968996, 14.56989629, 14.13932456, 13.74991694,
                                    13.39536456, 13.07064573, 12.77170536, 12.49522711, 12.23846807,
                                    11.99913671, 11.77530118, 11.56531949, 11.36778563, 11.18148745,
                                    11.00537341, 10.83852613, 10.68014098, 10.52950884, 10.38600188,
                                    10.2490619 ]
if KPI == "Retention":
    print(int((sample*0.4114918603)/(retention[rate-1]/100)))
elif KPI == "Conversion":
    print(int((sample*0.015*payers/16.005/(rate/100))))
elif KPI == "LTV":
    print(int(sample*payers/16.005))
else:
    print(int(sample))

