<?php
session_start();
if(isset($_SESSION["id"])== FALSE){ header("Location:../login/LOGIN.php");}
include("TOPsample.php");
?>
 <div style="float:right;margin-left:25px;color:red;"> <p id ="expire" > </p> </div> 
 <div style="display:inline-block;vertical-align:top; position: relative; height: 30px;"></div>
<div style="text-align:center; margin: 1% 6% 3% 0;"><h3 style="font-size: 400%;" id="sampleSize" > 1067 </h3> </div>

<br>

     <label style="margin-left: 30%;">Select KPI:</label> &nbsp
      <select name="KPI" id="KPI">
        <option value="Retention" >Retention</option>
        <option value="Conversion" >Conversion Rate</option>
        <option value="LTV">LTV</option>
        <option value="General">General</option>
    </select>
    <div style="float:right; margin-right: 35%;" id = "Payers">
    <label style="float:left; ">Min. Payers:</label>
    <input id = "PayersN" step="1" value="0" min="1" style="margin-right: 30%; float:right; border: true; width:30%;" type="number" disabled/>
    </div>
<br>
<br>

<div style="margin-left: 30%;" id = "selectedKPI">
        <input style="float:left; width: 40%;" id="rangeInput3" type="range" oninput="amount3.value=rangeInput3.value" step="1" 
        value="1" min="1" max="30" />
        &nbsp <div style="float:right;margin-right: 56.5%;" id="sign">Day</div>
        <input  step="1"  value="1" min="1" max="30" 
        style="margin-right: -8%;float:right; border: none; width:5%;" id="amount3" type="number" oninput="rangeInput3.value=amount3.value" disabled/>
</div>


<br>

<div style="margin-left: 30%;">
    <label>Confidence level:</label><br>
    <input style="float:left; width: 40%;" id="rangeInput" type="range" step="0.01" value="95" min="0" max="99.99" oninput="amount.value=rangeInput.value" />
    &nbsp &nbsp &nbsp %
    <input style="margin-right: 51%;float:right; border: none; width:5%;" id="amount" type="number" step="0.01" value="95" min="0" max="99.99" oninput="rangeInput.value=amount.value" disabled />
  </div>


<br>


<div style="margin-left: 30%;">
     <label>Margin of error:</label><br>
    <input style="float:left; width: 40%;" id="rangeInput2" type="range" step="0.01" value="3" min="0.01" max="100"  oninput="amount2.value=rangeInput2.value" />
    &nbsp &nbsp &nbsp %
    <input style="margin-right: 51%;float:right; border: none; width:5%;" id="amount2"  type="number" step="0.01"  value="3" min="0.01" max="100" oninput="rangeInput2.value=amount2.value" disabled/>
  </div>


<br>


<div style="margin-left: 30%;">
    <label>Estimated Proportion:</label><br>
    <input style="float:left; width: 40%;" id="rangeInput4" type="range" step="0.01" value="50" min="0" max="100" oninput="amount4.value=rangeInput4.value" />
    &nbsp &nbsp &nbsp %
    <input style="margin-right: 51%;float:right; border: none; width:5%;" id="amount4" type="number" step="0.01" value="50" min="0" max="100" oninput="rangeInput4.value=amount4.value" disabled/>
  </div>

  <br>
  <br>
  <br>
  <br>

  <div style="font-size: 115%;margin: 0 10% 0 26%;">
   <label style="margin-left:-3%;"><h2> Note:</h2></label> 
      <ul>
        <li>The calculation uses the KPIs' standard benchmarks.</li>
        <li>The sample size number is per variant. </li>
        <li>If the minimum payers number is not defined please keep the <b>Min. Payers box equal zero</b>.</li>
        <li>If the <b>Estimated Proportion</b> is unknown please keep the proportion equal 50%.</li>
    </ul>
  </div>
  <div class="bottom-left" style="position: fixed; bottom: 0; left: 0;"> <p style="font-size:75%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> </div> 


<?php
include("../BOTTOM.php");
?>
