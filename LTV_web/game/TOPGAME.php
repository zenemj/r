<?php
session_start();
if(isset($_SESSION["id"])== FALSE){ header("Location:../login/LOGIN.php");}
?>

<!DOCTYPE html>
<html lang="en">
  <head>


  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <script src="bootstrap3-typeahead.min.js"></script> 
  
  <link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
  <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>
  


  <link type="text/css" rel="stylesheet" href="http://fakedomain.com/smilemachine/html.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="styles.css?v=<?php echo time();?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

 <style>
html,body {
  height: 100%;
  width: 100%
}
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

div { width: auto; height: auto; min-width: auto; min-height: auto; } 

.topnav {
  overflow: hidden;
  background-color: #3fa19c;
}

.topnav a {
  float: left;
  color: #383838;
  text-align: center;
  padding: 0.75% 3% 0% 5%;
  text-decoration: none;
  font-size: 125%;
}

/* .topnav a:hover {
  background-color: #ddd;
  color: black;
} */

/* .topnav a.active {
  background-color: #04AA6D;
  color: white;
} */
</style>


<style>
*{
  margin:0;
  padding:0;
}

/* .form-box{
   width: 380px;
   height: 80px;
   position: relative;
   margin: 6% auto;
   background: #fff;
   padding: 5px;
} */

.button-box{
  width:220px;
  margin: 10px auto;
  position: relative;
  box-shadow: 0 0 20px 9px #ff61241f;
  border-radius: 30px;
}

.toggle-btn{
  padding: 10px 30px;
  cursor: pointer;
  background: transparent;
  border: 0;
  outline: none;
  position: relative; 
}

.toggle-btn2{
  padding: 10px 30px;
  cursor: pointer;
  background: transparent;
  border: 0;
  outline: none;
  position: relative; 
}

#btn{
  top: 0;
  left: 0;
  position: absolute;
  width: 110px;
  height: 100%;
  background:linear-gradient(to right,orange,#fae1b9);
  border-radius: 30px;
  transition:.5s;
}

</style>

<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>



<!-- <style>
.overlay{
    display: none;
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 999;
    background: rgba(255,255,255,0.8) url("loader.gif") center no-repeat;
}
/* Turn off scrollbar when body element has the loading class */
body.loading{
    overflow: hidden;   
}
/* Make spinner image visible when body element has the loading class */
body.loading .overlay{
    display: block;
}
</style> -->

<script>
// Initiate an Ajax request on button click
$(document).on( "button", function(){
    $.get(function(data){
        $("body").html(data);
    });       
});
 
// Add remove loading class on body element based on Ajax request status
$(document).on({
    ajaxStart: function(){
        $("body").addClass("loading"); 
    },
    ajaxStop: function(){ 
        $("body").removeClass("loading"); 
    }    
});
</script>

<style>
.center {
  margin: auto;
  width: 60%;
  border: 3px solid gray;
  padding: 10px;
}
</style>



<style>
input[name='weeks']:after {
        width: 12.7px;
        height: 12.7px;
        border-radius: 12.7px;
        top: 0px;
        left: 0.5px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[name='weeks']:checked:after {
        width: 12.7px;
        height: 12.7px;
        border-radius: 12.7px;
        top: 0px;
        left: 0.5px;
        position: relative;
        background-color: green;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }
</style>

<style>
select.form-control 
{
    background-color: green;
}
</style>

<style>
table {
border-collapse: separate;
width: 100%;
color: #2d2d2e;
font-size: 11px;
/*font-family: monospace;
/*text-align: right;
border-spacing:10px 10px;*/
}
th {
background-color: #2c7cc7;
color: white;
text-align: center;
}
tr:nth-child(even) {background-color: #ffffff}
</style>

<style>
body {font-family: Arial;}

/* Style the tab */
.tab2 {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab2 button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;

}

/* Change background color of buttons on hover */
.tab2 button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab2 button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent2 {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
  color:black;
}

</style>



<style>
body {font-family: Arial, Helvetica, sans-serif;}
.tab3 {border: 3px solid orange;
      margin: auto;
      width: 30%;
      border: 3px solid orange;
      padding: 10px;
      box-shadow: 0 0 20px 9px #ff61241f;
}

.tab3 input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid orange;
  box-sizing: border-box;
}

.tab3 select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid orange;
  box-sizing: border-box;
}

.tab3 button {
  background-color: orange;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

.tab3 button:hover {
  opacity: 0.8;
}

.tab3.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* .tab3.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

.tab3 img.avatar {
  width: 40%;
  border-radius: 50%;
} */

.tab3.container {
  padding: 16px;
}

.tab3 span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}

</style>

<style>
.alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #04AA6D;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>

<style>
.sets {border: 3px solid #b4d1d1;
      margin: auto;
      width: 40%;
      border: 3px solid #b4d1d1;
      padding: 10px;
      /* background-color:#b4d1d1; */
      box-shadow: 0 0 10px 3px #8bb5b5;
}

.sets input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #b4d1d1;
  box-sizing: border-box;
}

.sets select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #b4d1d1;
  box-sizing: border-box;
}

.sets.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* .sets.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

.sets img.avatar {
  width: 40%;
  border-radius: 50%;
} */

.sets.container {
  padding: 16px;
}

.sets span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style> 

<style>
  .sets2 {
  background-color: #3fa19c;
  color: white;
  padding: 1.3% 5%;
  margin: 1% 35%;
  border: none;
  cursor: pointer;
  /* width: 33%; */
  font-weight: bold;
}

.sets2:hover {
  opacity: 0.8;
}
</style>

</head>

<body>


<div class="topnav">
       <a href="../index.php" style="width:17%;"> <img src="4-01.png" alt="logo" width="120%" height="120%" style="margin-left:-35%; float:left;"></a>
       <a href="../ltv/LTV.php" style="padding: 1.25% 5% 0% 2%; color:#f2f2f2;"> Dashboards </a> 
       <a href="../logout/LOGOUT.php" style="float:right; padding: 1.25% 2% 0% 0%;"> Log out</a>
</div>
