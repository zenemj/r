<?php
session_start();
include("TOPINDEX.php");
?>

<br>
<div style="width: 100%; height: 100%; background-color:white;color:black;">
<!-- <div>
    <center>
        <h1 style="color:red;margin-top:5%;">Coming Soon ....</h1>
    </center>
</div> -->

<div class="container" style="background-color:white;">
    <center>
        <img src="4-01.png" alt="title" style="width:40%; margin-top:5%;">
    </center>
</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 3% 12% 0 12%;">

        <div style="margin:0 1% 0 2%; font-family: Arial;color:#424242;float:left;"> 
            <b>
                <h1 style="font-size: 300%;">Estimate Your Games LTV and ROAS</h1>
                <p>We provide a solution to estimate the LTV (Life-Time Value) and ROAS (Return On Advertising Spend), and
                calculate the real ROAS for your games.</p>
              </b>
        </div>
        <br>
        <div style="margin: 1% 0 0 0;">
            <img src="LTV1.png" alt="LTV" style="float: right; width:100%;box-shadow: 0 0 15px 0 #888;">
        </div>
</div>
<div class="container" style="background-color:white;font-size: 115%;margin: 3% 12% 0 12%;">
        <div style="background-color:white; ">
            <img src="7-01.png" alt="logo2"
                style="float:left; width:99%;opacity: 0.1;z-index: 1;margin:0 3% 0 2%;">
        </div>
</div>
<div class="container" style="background-color:white;font-size: 115%;margin: 0 12% 1% 12%;">
        <div style="float:left;margin: 0 0 0 4%;">
            <b style=" font-family: Arial;color:#424242;width:100%;float:right;margin:0 2% 0 2%;">
                <h1 style="font-size: 200%;">No data is saved<br>
                No result is saved <br>
                We just provide you with excellent predictive results and give you the possibility to download them
                </h1>
            </b>
        </div>
</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 0 12% 0 12%;">

        <div >
            <img src="ROAS1.png" alt="ROAS"
                style="float:left; width:100%;box-shadow: 0 0 15px 0 #888;margin: 0 2% 1% 6%;">
        </div>
        <div  style="float:right; margin:2% 0% 2% 4%; font-family: Arial;color:#424242;">
            <b>
                MANDS uses machine-learning algorithms and complex math formulas to give the most accurate LTV and ROAS
                estimation, and real ROAS calculation based on provided game data.
            </b>
            <center>
                <a href="../signup/SIGNUP.php"><button class="loginbutton" type="button" style="margin: 8% 0 0 0;"><b>Join for Free</b></button></a>
            </center>
        </div>

</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 3% 12% 0 12%;">

        <div style="margin:2% 2% 2% 2%; font-family: Arial;color:#424242;float:left; width:100%;"> 
                <center><b style="font-family: Arial; color:#424242;"><h1 style="font-size: 300%;">All Your Games in One Place</h1></b></center>
                
         </div>
</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 0 12% 0 12%;">

        <div style="margin:2% 2% 2% 2%; font-family: Arial;color:#424242;float:left;"> 
                <b style="color:#424242; width:100%; float:left; margin: 2% 0 0 4%;">
                    At MANDS, we enable the user to use multiple filters to get the desired cohort results. Further more,
                    you can estimate and calculate the LTV and ROAS for all of your games in one place.
                    
                    <center>
                        <a href="../contactus/contact.php"><button class="loginbutton" type="button" style="margin: 15% 0 0 0;"><b>Contact Us</b></button></a>
                    </center>
                </b>
        </div>
        <br>
        <div>
            <img src="ROAS2.png" alt="FILTER" style="float:right; width:100%;box-shadow: 0 0 15px 0 #888; ">
        </div>
</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 3% 12% 0 12%;">
        <div style="background-color:white; ">
            <img src="7-01.png" alt="logo2"
                style="float:left; width:99%;opacity: 0.1;z-index: 1;margin:0 3% 0 2%;">
        </div>
</div>

<div class="container" style="background-color:white;font-size: 115%;margin: 0 12% 0 12%;">

        <div style="margin:0 2% 2% 2%; font-family: Arial;color:#424242;float:left;"> 
          <b style="font-family: Arial; color:#424242;"><h1 style="font-size: 300%;">Find how many users do you need in your AB tests</h1></b>
        </div>
        <div style="margin:4.5% 2% 2% 2%; font-family: Arial;color:#424242;float:left;">
          <b >
          Our sample size calculator lets you find the minimum number of users you need in your game’s AB tests for many KPIs, 
          and calculates the sample size for the general tests.
          </b>
          <center>
                <a href="../signup/SIGNUP.php"><button class="loginbutton" type="button" style="margin: 5% 0 0 0;"><b>Try It Now</b></button></a>
            </center>
                
        </div>
</div>

<hr class="container" style="color:#424242; font-size: 115%; margin: 3% 5% -1% 5%;">

<div class="container" style="margin: 0 5% 0 5%;">
    
        <div style="width:100%;">
            <img src="1-01.png" alt="logo" width="130px" style="margin-left:1%; margin-right:2%; margin-top:1%; float:left;"> 
      
            <a target="_blank" href="../policy/privacy-policy.php" style="font-size: 70%; float:left; margin:2.5% 4% 0 0;">Privacy Policy</a>
            <a target="_blank" href="../policy/privacy-policy.php" style="font-size: 70%; float:left; margin:2.5% 4% 0 0;">Cookie  Policy</a>
            <a target="_blank" href="../contactus/contact.php" style="font-size: 70%; float:left; margin:2.5% 0 0 0;">Contact us </a>
            <a target="_blank" href="../contactus/contact.php" style="font-size: 70%; float:left; margin:2.5% 0 0 0;">&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp </a>
            
            <a href="#" class="fa fa-facebook"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class="fa fa-instagram"></a>
        </div>

</div>


<div class="container" class="bottom-left" style=" bottom: 0; left: 0;  margin: -2% 0 0 0;"> 
 
<p style="font-size:75%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> 

</div> 

<div class="privacy-banner" style="border: 2px solid #ff910e; display: none;">
  <div class="banner-wrapper">
    <p>We use cookies to provide and improve our services. By using our site, you consent to cookies. <a target="_blank" href="../policy/privacy-policy.php" style="color:white;">Learn more</a></p>
    <button aria-label="Close" style="background: #ff910e;" type="button"><span aria-hidden="true">x</span></button>
  </div>
</div>

</div>

<script>
    // Banner Trigger if Not Closed
if (!localStorage.bannerClosed) {
  $('.privacy-banner').css('display', 'inherit');
} else {
  $('.privacy-banner').css('display', 'none');
}
$('.privacy-banner button').click(function() {
  $('.privacy-banner').css('display', 'none');
  localStorage.bannerClosed = 'true';
});
$('.banner-accept').click(function() {
  $('.privacy-banner').css('display', 'none');
  localStorage.bannerClosed = 'true';
});
if (navigator.userAgent.match(/Opera|OPR\//)) {
  $('.privacy-banner').css('display', 'inherit');
}
</script>
<?php
include("BOTTOM.php");
?>