<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
  <head>

  <title>Estimate LTV and ROAS for games and mobile games, and a real calculation for the ROAS</title>
  <link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
  <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
  <meta name="description" content="Estimate LTV and ROAS for games and mobile games and f2p games, and a real calculation for the ROAS. In addition, you can calculate your sample size for your AB tests by using the sample size calculator for many KPIs">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" charset="utf-8" >
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <script src="bootstrap3-typeahead.min.js"></script> 
  <script src="bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="bootstrap-multiselect.css" />
  <link type="text/css" rel="stylesheet" href="http://fakedomain.com/smilemachine/html.css" />

  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="styles.css?v=<?php echo time();?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

 <script>
// Initiate an Ajax request on button click
$(document).on( "button", function(){
    $.get(function(data){
        $("body").html(data);
    });       
});
 
// Add remove loading class on body element based on Ajax request status
$(document).on({
    ajaxStart: function(){
        $("body").addClass("loading"); 
    },
    ajaxStop: function(){ 
        $("body").removeClass("loading"); 
    }    
});
</script>

<style>

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.container {
     display: flex;   
}

@media screen and (max-width: 480px) {
    .full_half {  width: 100%; }
    .text      {  padding: 0 0 !important; }
    .container { flex-direction: column; }        
    .container:nth-child(2) > .pic { order: -1; }  
}

/* div { width: auto; height: auto; min-width: auto; min-height: auto; }
form { width: auto; height: auto; min-width: auto; min-height: auto; } 
b { width:auto; height: auto; min-width: 375px; min-height: auto; } */

.topnav {
  position:fixed;
  overflow: hidden;
  background-color: white;
  box-shadow: 0 0 15px 0 #888;
  width:100%;
}

.topnav a {
  float: left;
  color: #383838;
  text-align: center;
  padding: 0.75% 3% 0% 5%;
  text-decoration: none;
  font-size: 125%;
}

.topnav a:hover {
  /* background-color: #ddd; */
  color: #dadee3;
}

.topnav a.active {
  /* background-color: #04AA6D; */
  color: blue; /*#f2f2f2;*/
}


.button5 {
  color: white;
  background-color: #555555;
  border-color: white;
  /* position:fixed; */
  margin:20% 0 0 0 ;
  font-size: 100%;
  font-weight: bold;
  border-radius: 10px 0px 10px 0px;
  box-shadow:  0 0 15px 0 #888; /*0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);*/
  width: 120%;
  /* color: black; */
  
} 

.button5:hover {
  background-color: #ddd;
  color: #4790ba;
}


.cookie-banner {
  position: fixed;
  bottom: 40px;
  left: 10%;
  right: 10%;
  width: 80%;
  padding: 5px 14px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #eee;
  border-radius: 5px;
  box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
}
.close {
  height: 20px;
  background-color: #777;
  border: none;
  color: white;
  border-radius: 2px;
  cursor: pointer;
}


.loginbutton {
  background-color: #4790ba; 
  border: none;
  color: white;
  padding: 3% 6%;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 100%;
  /* margin: 4px 2px; */
  cursor: pointer;
  box-shadow: 0 0 15px 0 #888;
  border-radius: 22px;
  width:50%;
}

.loginbutton:hover {
/* color: blue; */
background-color: #75a3bd;

}

.loginbutton a {
color: white;
text-decoration: none;

}

.loginbutton a:hover {
/* color: blue; */
text-decoration: none;
}

.fa {
  padding: 0.3%;
  font-size: 125%;
  width: 20px;
  text-align: center;
  text-decoration: none;
  margin: 2.2% 0.2%;
  float:right;
  border-radius: 50%;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}


.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

a {
  color: #424242;
  text-decoration: none;
  font-size: 100%;
  font-weight: bold;
 
}

a:hover {
  color: blue;
  text-decoration: none;
}

.privacy-banner {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    max-width: 100%;
    padding: 1rem .5rem;
    background: #424242;
    z-index: 1030;
    color: white;
    font-size: 18px;
    margin: 0;
    display: none;
  }
  .banner-wrapper {
    max-width: 1200px;
    width: 100%;
    margin: 0 auto;
    display: flex;
    position: relative;
    align-items: center;
  }
  .privacy-banner p {
    margin: 0;
    color: white;
    text-align: center;
  }
  .privacy-banner .banner-wrapper p {
      padding-right: 3rem;
  }
  .privacy-banner a {
    text-decoration: none;
    margin: 20px auto 0 auto;
    display: block;
    max-width: 150px;
  }
  .privacy-banner a:hover {
    text-decoration: underline;
  }
  .privacy-banner button {
    position: absolute;
    right: 5px;
    top: calc(50% - 12.5px);
    color: #fff;
    outline: 0;
    height: 25px;
    width: 25px;
    border: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.35rem;
    font-weight: 700;
    border-radius: 50%;
    text-align: center;
    padding: 0;
    line-height: 1;
    background: blue;
    cursor: pointer;
  }
  .banner-learn {
    color: blue;
  }
  .banner-accept {
    padding: 7px 15px;
    color: blue;
    border-radius: 5px;
    background: white;
  }
  @media (min-width: 768px) {
    .privacy-banner {
      padding: 1.5rem .5rem;
    }
    .privacy-banner a {
      display: inline-block;
      margin: 0 10px;
    }
  }

</style>

</head>

<body>


<div class="topnav">
            <!-- <img src="gray.jpg" alt="gray" width="100%" style="box-shadow: 0 0 15px 0 #888;opacity: 1;"> -->
            <div >
                   <img src="1-01.png" alt="logo" style="margin-left:1%; float:left;
                            max-width: 160px;
                            max-height: auto;"> 
            </div>
            <?php    
                  session_start();
                  if(isset($_SESSION["id"])){
                    echo '<div  style="margin:0.9% -2% 0 0; float:right; width:10%;"> <a href="../logout/LOGOUT.php" > Log out</a></div>';
                    echo '<div  style="margin:0.9% 2% 0 0; float:right; width:10%;"> <a href="../ltv/LTV.php" style="float:right;"> Dashboards </a></div>';
                    echo '<div  style="margin:0.7% 0 0 0;float:right; width:50%"> <a href="../userSetting/userSetting.php" style="float:right;"> <img src="Setting-icon.png" alt="Setting" style = "display: inline;" width="18" height="18" > ' .$_SESSION["name"] .':</a></div>';
                  }
                  else{
                    // echo '<div class="top-right" style="margin:1% 5% 0 0;"> <a href="login/LOGIN.php" class="button" > Log in </a> </div>';
                    echo '<div style="margin-right:1.5%; float:right;max-width: 100%;"> 
                            <form action="signup/SIGNUP.php" >
                                <input type="submit" value="&nbsp Join &nbsp" class="button5"/>
                            </form>
                          </div>';
                    echo '<div  style="margin-right:1%; float:right;"> 
                            <form action="login/LOGIN.php" >
                              <input type="submit" value="Log in" class="button5"/>
                            </form>
                          </div>';
                    
                  }
            ?> 
</div>
</div>

