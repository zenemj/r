import sys
from google.cloud import bigquery
from google.oauth2 import service_account
from datetime import datetime, timedelta

startdate           = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
enddate             = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7]
country             = sys.argv[8]
campaign            = sys.argv[9]
# delta_username      = sys.argv[10]
# delta_password      = sys.argv[11]
# emaillogin          = sys.argv[12]
key                 = sys.argv[13]
# flake_server        = args[14]
user_id             = sys.argv[15]

credentials = service_account.Credentials.from_service_account_file(key)
project_id = game_name.split('.')[0]
client = bigquery.Client(credentials= credentials,project=project_id)

QUERY = ("select * from (SELECT "+event_date+" as EVENT_DATE, FIRST_VALUE("+platform+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS PLATFORM, FIRST_VALUE("+campaign+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS CAMPAIGN_NAME, FIRST_VALUE("+country+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS COUNTRY FROM "+game_name+"."+table_name+" WHERE "+user_start_date+" between '"+startdate+"' and '"+enddate+"' and "+event_date+" between '"+startdate+"' and '"+enddate+"') A GROUP BY 1,2,3,4  order by 1")
query_job = client.query(QUERY) 
rows = query_job.result()  

for i,j in zip(query_job, range(0,rows.total_rows)):
    print('("'+str(i['EVENT_DATE'])+'","'+str(i['PLATFORM'])+'","'+str(i['CAMPAIGN_NAME'])+'","'+str(i['COUNTRY'])+'")')
    if j < rows.total_rows-1:
        print(',')