import sys
import snowflake.connector
from datetime import datetime, timedelta
import pandas as pd 
import numpy as np

start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
weeks               = int(sys.argv[3][0:2])+1
game_name           = sys.argv[4].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[5]
event_date          = sys.argv[6]
user_start_date     = sys.argv[7]
platform            = sys.argv[8].strip()
platforms           = sys.argv[9]
country             = sys.argv[10].strip()
countrys            = sys.argv[11]
campaign            = sys.argv[12].strip()
campaigns           = sys.argv[13]
acquisition_channel = sys.argv[14]
revenue             = sys.argv[15]
revenue_rate        = sys.argv[16]
delta_username      = sys.argv[17]
delta_password      = sys.argv[18]
#emaillogin           = args[19]
user_id             = sys.argv[20]
# key                 = args[21]
flake_server        = sys.argv[22].replace("_ZEN_EMJ_", ".")
ads_revenue         = sys.argv[23]
enddateweek         = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=weeks-1), "%Y-%m-%d")

if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","").strip()
    else:
        campaign1 = campaign.replace("None','","").strip()


if "None" not in campaign:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY "+user_start_date+",USER_ID ,DAY ,Days_Since_Today),                                                                                     data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY ) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"
elif "None" not in campaign1:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"')) GROUP BY "+user_start_date+" ,user_id, DAY, Days_Since_Today), data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY ) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY" 
else:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none') GROUP BY "+user_start_date+", user_id, DAY, Days_Since_Today),                                       data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY ) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"

if revenue != '' and ads_revenue =='':
    revenue1= "sum("+revenue
    total_revenue = revenue
elif revenue == '' and ads_revenue !='':
    revenue1= "sum("+ads_revenue
    total_revenue = ads_revenue
else:
    revenue1= "(sum("+revenue+")+sum("+ads_revenue+")"
    total_revenue = ads_revenue+" > 0 or "+revenue
    
    
if "None" not in campaign:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY 1 order by 1) A,                                                                                       (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') ) B"
elif "None" not in campaign1:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ( "+campaigns+" is null or  "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"') ) GROUP BY 1 order by 1) A, (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ( "+campaigns+" is null or  "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"') ) ) B"
else:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' ) GROUP BY 1 order by 1) A,                                         (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' ) ) B"
    
con = snowflake.connector.connect(
    user=delta_username,
    password=delta_password,
    account=flake_server
    )

cur = con.cursor()
cur.execute(q1)
result = cur.fetchall()

cur2 = con.cursor()
cur2.execute(q2)
ARPDAU = cur2.fetchall()


data=[ARPDAU[0][0],float(ARPDAU[0][1]),int(ARPDAU[0][2]),int(ARPDAU[0][3])]
for i in range(0,len(result)):
    data.append(float(result[i][1]))

print(data)