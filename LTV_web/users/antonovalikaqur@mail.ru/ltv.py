import sys
from datetime import datetime, timedelta
import pandas as pd 
import math
import numpy as np

from google.cloud import bigquery
from google.oauth2 import service_account

start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
weeks               = int(sys.argv[3][0:2])+1
game_name           = sys.argv[4].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[5]
event_date          = sys.argv[6]
user_start_date     = sys.argv[7]
platform            = sys.argv[8].strip()
platforms           = sys.argv[9]
country             = sys.argv[10].strip()
countrys            = sys.argv[11]
campaign            = sys.argv[12].strip()
campaigns           = sys.argv[13]
acquisition_channel = sys.argv[14]
revenue             = sys.argv[15]
revenue_rate        = sys.argv[16]
# delta_username      = sys.argv[17]
# delta_password      = sys.argv[18]
#emaillogin           = args[19]
user_id             = sys.argv[20]
key                 = sys.argv[21]
# flake_server        = args[22]
ads_revenue         = sys.argv[23]
enddateweek         = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=weeks-1), "%Y-%m-%d")

if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","").strip()
    else:
        campaign1 = campaign.replace("None','","").strip()


if "None" not in campaign:
    q1 = "with data as (select "+user_id+" AS user_id, DATE_DIFF("+event_date+","+user_start_date+",DAY ) AS DAY, DATE_DIFF(CURRENT_DATE(),"+user_start_date+",DAY) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATE_DIFF("+event_date+","+user_start_date+",DAY) >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY "+user_start_date+",USER_ID ,DAY ,Days_Since_Today),                                                    data1 as ( select count(distinct case when DAY = 0 then user_id end) AS count_installs  FROM data), data2 as (select DAY, count(distinct user_id) as users, count_installs as total_installs from data, data1 group by DAY,count_installs) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM(users)/SUM(total_installs))* 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"
elif "None" not in campaign1:
    q1 = "with data as (select "+user_id+" AS user_id, DATE_DIFF("+event_date+","+user_start_date+",DAY ) AS DAY, DATE_DIFF(CURRENT_DATE(),"+user_start_date+",DAY) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATE_DIFF("+event_date+","+user_start_date+",DAY) >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"')) GROUP BY "+user_start_date+" ,user_id, DAY, Days_Since_Today), data1 as ( select count(distinct case when DAY = 0 then user_id end) AS count_installs  FROM data), data2 as (select DAY, count(distinct user_id) as users, count_installs as total_installs from data, data1 group by DAY,count_installs) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY" 
else:
    q1 = "with data as (select "+user_id+" AS user_id, DATE_DIFF("+event_date+","+user_start_date+",DAY ) AS DAY, DATE_DIFF(CURRENT_DATE(),"+user_start_date+",DAY) AS Days_Since_Today FROM "+game_name+"."+table_name+" where DATE_DIFF("+event_date+","+user_start_date+",DAY) >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none') GROUP BY "+user_start_date+", user_id, DAY, Days_Since_Today),                                       data1 as ( select count(distinct case when DAY = 0 then user_id end) AS count_installs  FROM data), data2 as (select DAY, count(distinct user_id) as users, count_installs as total_installs from data, data1 group by DAY,count_installs) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"

if revenue != '' and ads_revenue =='':
    revenue1= "sum("+revenue
    total_revenue = revenue
elif revenue == '' and ads_revenue !='':
    revenue1= "sum("+ads_revenue
    total_revenue = ads_revenue
else:
    revenue1= "(sum("+revenue+")+sum("+ads_revenue+")"
    total_revenue = ads_revenue+" > 0 or "+revenue 
    
if "None" not in campaign:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY 1 order by 1) A,                                                      (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+"  where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') ) B"
elif "None" not in campaign1:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ( "+campaigns+" is null or  "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"') ) GROUP BY 1 order by 1) A, (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+"  where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ( "+campaigns+" is null or  "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ('"+campaign1+"') ) ) B"
else:
    q2 = "select avg(ARPDAU) as ARPDAU, avg(REVENUE) as REVENUE, avg(TOTAL_USERS) as TOTAL_USERS, avg(PAYERS) as PAYERS from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+game_name+"."+table_name+" where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none') GROUP BY 1 order by 1) A,                                         (select ("+revenue1+")*"+revenue_rate+") as REVENUE, count(distinct "+user_id+") as TOTAL_USERS, count(distinct case when "+total_revenue+" > 0 then "+user_id+" end) as PAYERS FROM "+game_name+"."+table_name+"  where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none') ) B"
    
credentials = service_account.Credentials.from_service_account_file(key)
project_id = game_name.split('.')[0]
client = bigquery.Client(credentials= credentials,project=project_id)

retention = (q1)
retention2 = client.query(retention) 
# rows = retention2.result() 

ARPDAU = (q2)
ARPDAU2 = client.query(ARPDAU) 
# rows2 = ARPDAU2.result() 


day = []
retention = []
one = []
log_DSI = []
log_R = []
for i in retention2: #range(0,rows.total_rows):
    day.append(i["DAY"])
    retention.append(i["retention"])
    one.append(1)
    log_DSI.append(math.log10(i["DAY"]))
    log_R.append(math.log10(i["retention"]))

for i in ARPDAU2:
    ARPDAU = i["ARPDAU"]
    REVENUE = i["REVENUE"]
    TOTAL_USERS = i["TOTAL_USERS"]
    PAYERS = i["PAYERS"]
    
x  = np.array(list(range(1,366)))
d1 = {'ones':one, 'log_DSI':log_DSI}
a  = pd.DataFrame(d1).to_numpy()
ta = a.transpose()
ly = pd.DataFrame(log_R).to_numpy()
q  = np.dot(np.linalg.inv(np.dot(ta,a)),np.dot(ta,ly))
b  = q[1][0]
e  = 10**q[0][0]
f  = (e*x**b)*ARPDAU/100
E  = 1.96*np.std(f)/np.sqrt(365)

LowerLimit1 = []

for i in range(0,365):
    if f[i] < E:
        c= 0
    else:
        c=f[i]-E
    LowerLimit1.append(c)

UpperLimit1 = (f-LowerLimit1)+f
LTV = np.cumsum(f)
UpperLimit = np.cumsum(UpperLimit1)
LowerLimit = np.cumsum(LowerLimit1)
ROAS = np.round(LTV*100,2)

for i in range(0,len(x)):
    print('(')
    print(list(x)[i])
    print(',')
    print(format(list(f)[i],'f'))
    print(',')
    print(format(list(LowerLimit1)[i],'f'))
    print(',')
    print(format(list(UpperLimit1)[i],'f'))
    print(',')
    print(format(list(LTV)[i],'f'))
    print(',')
    print( format(list(UpperLimit)[i],'f'))
    print(',')
    print(format(list(LowerLimit)[i],'f'))
    print(',')
    print(format(list(ROAS)[i],'f'))
    print(',')
    print(format(REVENUE,'f'))
    print(',')
    print(format(TOTAL_USERS,'f'))
    print(',')
    print(format(PAYERS,'f'))
    print(',')
    print(format(ARPDAU,'f'))
    print(')')
    if i < len(x)-1:
        print(',')