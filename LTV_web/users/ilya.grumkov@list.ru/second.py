import sys
from datetime import datetime
import snowflake.connector

startdate           = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
enddate             = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7]
country             = sys.argv[8]
campaign            = sys.argv[9]
delta_username      = sys.argv[10]
delta_password      = sys.argv[11]
emaillogin          = sys.argv[12]
# key                 = sys.argv[13]
flake_server        = sys.argv[14].replace("_ZEN_EMJ_", ".")
user_id             = sys.argv[15]


con = snowflake.connector.connect(
    user=delta_username,
    password=delta_password,
    account=flake_server
    )

cur = con.cursor()
cur.execute("select * from (SELECT "+event_date+" as EVENT_DATE, FIRST_VALUE("+platform+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS PLATFORM, FIRST_VALUE("+campaign+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS CAMPAIGN_NAME, FIRST_VALUE("+country+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS COUNTRY FROM "+game_name+"."+table_name+" WHERE "+user_start_date+" between '"+startdate+"' and '"+enddate+"' and "+event_date+" between '"+startdate+"' and '"+enddate+"') A GROUP BY 1,2,3,4  order by 1")
result = cur.fetchall() 

for i in range(0,len(result)):
    print('("'+str(result[i][0])+'","'+str(result[i][1])+'","'+str(result[i][2])+'","'+str(result[i][3])+'")')
    if i < len(result)-1:
        print(',')

