import sys
import snowflake.connector
from datetime import datetime, timedelta
import pandas as pd 
import numpy as np

start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7].strip()
platforms           = sys.argv[8]
country             = sys.argv[9].strip()
countrys            = sys.argv[10]
campaign            = sys.argv[11].strip()
campaigns           = sys.argv[12]
acquisition_channel = sys.argv[13]
revenue             = sys.argv[14]
revenue_rate        = sys.argv[15]
delta_username      = sys.argv[16]
delta_password      = sys.argv[17]
#emaillogin           = args[18]
user_id             = sys.argv[19]
# key                 = args[20]
flake_server        = sys.argv[21].replace("_ZEN_EMJ_", ".")
ads_revenue         = sys.argv[22]
endplus30           = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=30), "%Y-%m-%d")


if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","").strip()
    else:
        campaign1 = campaign.replace("None','","").strip()

if revenue != '' and ads_revenue =='':
    revenue1= revenue
elif revenue == '' and ads_revenue !='':
    revenue1= ads_revenue
else:
    revenue1= "("+revenue+"+"+ads_revenue+")"

if revenue != '' and ads_revenue =='':
    revenue2 = revenue
elif revenue == '' and  ads_revenue !='':
    revenue2 = ads_revenue
else:
    revenue2 = revenue+" > 0 or "+ads_revenue

if "None" not in campaign:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND "+campaigns+" in ( '"+campaign+"' ) ) A group by 1,2,4 having PAYERS > 0"
elif "None" not in campaign1:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ( '"+campaign1+"' ) ) ) A group by 1,2,4 having PAYERS > 0"
else:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATEDIFF(DAY, A.INSTALLDATE,current_date()) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none') ) A group by 1,2,4 having PAYERS > 0"

   
conn = snowflake.connector.connect(
    user=delta_username,
    password=delta_password,
    account=flake_server
    )

cur = conn.cursor()
cur.execute(q1)
result = cur.fetchall()

for i in range(0,len(result)):
    print('("'+str(result[i][0])+'","'+str(result[i][1])+'"')
    print(',')
    print(result[i][2])
    print(',"'+str(result[i][3])+'"')
    print(',')
    print(result[i][4])
    print(',')
    print(result[i][5])
    print(',')
    print(result[i][6])
    print(',')
    print(result[i][7])
    print(',')
    print(result[i][8])
    print(',')
    print(result[i][9])
    print(',')
    print(result[i][10])
    print(',')
    print(result[i][11])
    print(',')
    print(result[i][12])
    print(',')
    print(result[i][13])
    print(',')
    print(result[i][14])
    print(',')
    print(result[i][15])
    print(',')
    print(result[i][16])
    print(',')
    print(result[i][17])
    print(')')
    if i < len(result)-1:
        print(',')

