import sys
from datetime import datetime, timedelta
import pandas as pd 
import math
import numpy as np

data0   = sys.argv[1]
data    = [float(x) for x in data0[1:-2].split(",")]
ARPDAU  = data[0]
revenue = data[1]
total_users = data[2]
total_payers  = data[3]
result  = list(data[4:])

day = []
retention = []
one = []
log_DSI = []
log_R = []
for i in range(0,len(result)):
    day.append(i+1)
    retention.append(result[i])
    one.append(1)
    log_DSI.append(math.log10(i+1))
    log_R.append(math.log10(result[i]))

x  = np.array(list(range(1,366)))
d1 = {'ones':one, 'log_DSI':log_DSI}
a  = pd.DataFrame(d1).to_numpy()
ta = a.transpose()
ly = pd.DataFrame(log_R).to_numpy()
q  = np.dot(np.linalg.inv(np.dot(ta,a)),np.dot(ta,ly))
b  = q[1][0]
e  = 10**q[0][0]
f  = (e*x**b)*ARPDAU/100
E  = 1.96*np.std(f)/np.sqrt(365)

LowerLimit1 = []

for i in range(0,365):
    if f[i] < E:
        c= 0
    else:
        c=f[i]-E
    LowerLimit1.append(c)

UpperLimit1 = (f-LowerLimit1)+f
LTV = np.cumsum(f)
UpperLimit = np.cumsum(UpperLimit1)
LowerLimit = np.cumsum(LowerLimit1)
ROAS = np.round(LTV*100,2)

for i in range(0,len(x)):
    print('(')
    print(list(x)[i])
    print(',')
    print(format(list(f)[i],'f'))
    print(',')
    print(format(list(LowerLimit1)[i],'f'))
    print(',')
    print(format(list(UpperLimit1)[i],'f'))
    print(',')
    print(format(list(LTV)[i],'f'))
    print(',')
    print( format(list(UpperLimit)[i],'f'))
    print(',')
    print(format(list(LowerLimit)[i],'f'))
    print(',')
    print(format(list(ROAS)[i],'f'))
    print(',')
    print(format(revenue,'f'))
    print(',')
    print(format(total_users,'f'))
    print(',')
    print(format(total_payers,'f'))
    print(',')
    print(format(ARPDAU,'f'))
    print(')')
    if i < len(x)-1:
        print(',')