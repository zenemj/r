import sys
from datetime import datetime, timedelta
from google.cloud import bigquery
from google.oauth2 import service_account


start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7].strip()
platforms           = sys.argv[8]
country             = sys.argv[9].strip()
countrys            = sys.argv[10]
campaign            = sys.argv[11].strip()
campaigns           = sys.argv[12]
acquisition_channel = sys.argv[13]
revenue             = sys.argv[14]
revenue_rate        = sys.argv[15]
# delta_username      = sys.argv[16]
# delta_password      = sys.argv[17]
#emaillogin           = args[18]
user_id             = sys.argv[19]
key                 = sys.argv[20]
# flake_server        = args[21]
ads_revenue         = sys.argv[22]
endplus30           = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=30), "%Y-%m-%d")


if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","").strip()
    else:
        campaign1 = campaign.replace("None','","").strip()

if revenue != '' and ads_revenue =='':
    revenue1= revenue
elif revenue == '' and ads_revenue !='':
    revenue1= ads_revenue
else:
    revenue1= "("+revenue+"+"+ads_revenue+")"

if revenue != '' and ads_revenue =='':
    revenue2 = revenue
elif revenue == '' and  ads_revenue !='':
    revenue2 = ads_revenue
else:
    revenue2 = revenue+" > 0 or "+ads_revenue


if "None" not in campaign:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE)) as INSTALL_RANK, (dense_RANK() over (order by EXTRACT(ISOWEEK FROM A.INSTALLDATE))) as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN)) as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATE_DIFF("+event_date+","+user_start_date+",DAY ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND "+campaigns+" in ( '"+campaign+"' ) ) A group by 1,2,4 having PAYERS > 0"
elif "None" not in campaign1:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE)) as INSTALL_RANK, (dense_RANK() over (order by EXTRACT(ISOWEEK FROM A.INSTALLDATE))) as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN)) as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATE_DIFF("+event_date+","+user_start_date+",DAY ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' or "+campaigns+" in ( '"+campaign1+"' ) ) ) A group by 1,2,4 having PAYERS > 0"
else:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, count(distinct USERID) as USERS, CAMPAIGN, count(distinct (case when REVENUE  > 0  then USERID else null end) ) as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 1 then sum(0) else sum (case when USERAGE <= 1 then REVENUE else 0 end) end as D1REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 2 then sum(0) else sum (case when USERAGE <= 2 then REVENUE else 0 end) end as D2REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 3 then sum(0) else sum (case when USERAGE <= 3 then REVENUE else 0 end) end as D3REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 4 then sum(0) else sum (case when USERAGE <= 4 then REVENUE else 0 end) end as D4REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 5 then sum(0) else sum (case when USERAGE <= 5 then REVENUE else 0 end) end as D5REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 6 then sum(0) else sum (case when USERAGE <= 6 then REVENUE else 0 end) end as D6REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 7 then sum(0) else sum (case when USERAGE <= 7 then REVENUE else 0 end) end as D7REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 14 then sum(0) else sum (case when USERAGE <= 14 then REVENUE else 0 end) end as D14REVENUE, case when DATE_DIFF(current_date(),A.INSTALLDATE,DAY) < 30 then sum(0) else sum (case when USERAGE <= 30 then REVENUE else 0 end) end as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE)) as INSTALL_RANK, (dense_RANK() over (order by EXTRACT(ISOWEEK FROM A.INSTALLDATE))) as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN)) as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, first_value("+acquisition_channel+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as GAUSERACQUISITIONCHANNEL, first_value("+campaigns+" ignore nulls) over (partition by "+user_id+" order by "+event_date+") as CAMPAIGN, DATE_DIFF("+event_date+","+user_start_date+",DAY ) as USERAGE,"+revenue1+"*"+revenue_rate+" as REVENUE, "+user_id+" as USERID from "+game_name+"."+table_name+" where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or lower("+campaigns+") = 'none' ) ) A group by 1,2,4 having PAYERS > 0"

   
credentials = service_account.Credentials.from_service_account_file(key)
project_id = game_name.split('.')[0]
client = bigquery.Client(credentials= credentials,project=project_id)

roas = (q1)
roas2 = client.query(roas) 
rows = roas2.result() 

for result,i in zip(roas2,range(0,rows.total_rows)):
    print('("'+str(result["INSTALLDATE"])+'","'+str(result["ACQUISITION_CHANNEL"])+'"')
    print(',')
    print(result["USERS"])
    print(',"'+str(result["CAMPAIGN"])+'"')
    print(',')
    print(result["PAYERS"])
    print(',')
    print(result["D0REVENUE"])
    print(',')
    print(result["D1REVENUE"])
    print(',')
    print(result["D2REVENUE"])
    print(',')
    print(result["D3REVENUE"])
    print(',')
    print(result["D4REVENUE"])
    print(',')
    print(result["D5REVENUE"])
    print(',')
    print(result["D6REVENUE"])
    print(',')
    print(result["D7REVENUE"])
    print(',')
    print(result["D14REVENUE"])
    print(',')
    print(result["D28REVENUE"])
    print(',')
    print(result["INSTALL_RANK"])
    print(',')
    print(result["WEEK_RANK"])
    print(',')
    print(result["CAMPAIGN_RANK"])
    print(')')
    if i < rows.total_rows-1:
        print(',')

