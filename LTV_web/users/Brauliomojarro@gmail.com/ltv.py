# from __future__ import division
# from __future__ import print_function
import sys
import psycopg2
from datetime import datetime, timedelta
import pandas as pd 
import math
import numpy as np

start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
weeks               = int(sys.argv[3][0:2])
game_name           = sys.argv[4].replace("/", ".")
table_name          = sys.argv[5]
event_date          = sys.argv[6]
user_start_date     = sys.argv[7]
platform            = sys.argv[8]
platforms           = sys.argv[9]
country             = sys.argv[10]
countrys            = sys.argv[11]
campaign            = sys.argv[12]
campaigns           = sys.argv[13]
acquisition_channel = sys.argv[14]
revenue             = sys.argv[15]
revenue_rate        = sys.argv[16]
delta_username      = sys.argv[17]
delta_password      = sys.argv[18]
#emaillogin           = args[19]
user_id             = sys.argv[20]
# key                 = args[21]
# flake_server        = args[22]
ads_revenue         = sys.argv[23]
enddateweek         = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=weeks-1), "%Y-%m-%d")

if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","")
    else:
        campaign1 = campaign.replace("None','","")


if "None" not in campaign:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+table_name+"_live where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY "+user_start_date+",USER_ID ,DAY ,Days_Since_Today), data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM(users)/SUM(total_installs))* 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"
elif "None" not in campaign1:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+table_name+"_live where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' or "+campaigns+" in ('"+campaign1+"')) GROUP BY "+user_start_date+" ,user_id, DAY, Days_Since_Today), data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY ) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY" 
else:
    q1 = "with data as (select "+user_id+" AS user_id, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) AS DAY, DATEDIFF(DAY,"+user_start_date+",CURRENT_DATE()) AS Days_Since_Today FROM "+table_name+"_live where DATEDIFF (DAY,"+user_start_date+","+event_date+") >= 0 and "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '') GROUP BY "+user_start_date+", user_id, DAY, Days_Since_Today), data1 as (select * , CONDITIONAL_TRUE_EVENT(DAY = 0) OVER (ORDER BY Days_Since_Today DESC, DAY ASC) AS count_installs FROM data), data2 as (select DAY, count(distinct user_id) as users, max(count_installs) as total_installs from data1 group by DAY ) select DAY+1 as DAY, CASE WHEN SUM(total_installs) = 0 THEN 0 ELSE (SUM (users) / SUM(total_installs)) * 100 END AS retention from data2 where DAY between 0 and "+str(weeks-1)+" group by DAY order by DAY"

if revenue != '' and ads_revenue =='':
    revenue1= "sum("+revenue
elif revenue == '' and ads_revenue !='':
    revenue1= "sum("+ads_revenue
else:
    revenue1= "(sum("+revenue+")+sum("+ads_revenue+")"
    
if "None" not in campaign:
    q2 = "select avg(ARPDAU) as ARPDAU from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+table_name+"_live where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and "+campaigns+" in ('"+campaign+"') GROUP BY 1 order by 1) A"
elif "None" not in campaign1:
    q2 = "select avg(ARPDAU) as ARPDAU from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+table_name+"_live where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ( "+campaigns+" is null or  "+campaigns+" = '' or "+campaigns+" in ('"+campaign1+"') ) GROUP BY 1 order by 1) A"
else:
    q2 = "select avg(ARPDAU) as ARPDAU from (select "+event_date+", ("+revenue1+")*"+revenue_rate+")/count(distinct "+user_id+") as ARPDAU FROM "+table_name+"_live where "+event_date+" between '"+start+"' and '"+enddateweek+"' and "+user_start_date+" between '"+start+"' and '"+end+"' and "+platforms+" in ('"+platform+"') and "+countrys+"  in ('"+country+"') and ("+campaigns+" is null or "+campaigns+" = '' ) GROUP BY 1 order by 1) A"
    
conn = psycopg2.connect(dbname=game_name, user=delta_username, password=delta_password, host='data.deltadna.net', port='5432', sslmode='require')

cur = conn.cursor()
cur.execute(q1)
result = cur.fetchall()

cur2 = conn.cursor()
cur2.execute(q2)
ARPDAU = cur2.fetchall()


day = []
retention = []
one = []
log_DSI = []
log_R = []
for i in range(0,len(result)):
    day.append(result[i][0])
    retention.append(result[i][1])
    one.append(1)
    log_DSI.append(math.log10(result[i][0]))
    log_R.append(math.log10(result[i][1]))

x  = np.array(list(range(1,366)))
d1 = {'ones':one, 'log_DSI':log_DSI}
a  = pd.DataFrame(d1).to_numpy()
ta = a.transpose()
ly = pd.DataFrame(log_R).to_numpy()
q  = np.dot(np.linalg.inv(np.dot(ta,a)),np.dot(ta,ly))
b  = q[1][0]
e  = 10**q[0][0]
f  = (e*x**b)*ARPDAU/100
E  = 1.96*np.std(f)/np.sqrt(365)

LowerLimit1 = []

for i in range(0,365):
    if f[0][i] < E:
        c= 0
    else:
        c=f[0][i]-E
    LowerLimit1.append(c)

UpperLimit1 = (f-LowerLimit1)+f
LTV = np.cumsum(f)
UpperLimit = np.cumsum(UpperLimit1)
LowerLimit = np.cumsum(LowerLimit1)
ROAS = np.round(LTV*100,2)

for i in range(0,len(x)):
    print('(')
    print(list(x)[i])
    print(',')
    print(format(list(f)[0][i],'f'))
    print(',')
    print(format(list(LowerLimit1)[i],'f'))
    print(',')
    print(format(list(UpperLimit1)[0][i],'f'))
    print(',')
    print(format(list(LTV)[i],'f'))
    print(',')
    print( format(list(UpperLimit)[i],'f'))
    print(',')
    print(format(list(LowerLimit)[i],'f'))
    print(',')
    print(format(list(ROAS)[i],'f'))
    print(')')
    if i < len(x)-1:
        print(',')