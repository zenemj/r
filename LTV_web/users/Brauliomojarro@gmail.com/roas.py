# from __future__ import division
# from __future__ import print_function
import sys
import psycopg2
from datetime import datetime, timedelta
import pandas as pd 
import math
import numpy as np

start               = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
end                 = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("/", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7]
platforms           = sys.argv[8]
country             = sys.argv[9]
countrys            = sys.argv[10]
campaign            = sys.argv[11]
campaigns           = sys.argv[12]
acquisition_channel = sys.argv[13]
revenue             = sys.argv[14]
revenue_rate        = sys.argv[15]
delta_username      = sys.argv[16]
delta_password      = sys.argv[17]
#emaillogin           = args[18]
user_id             = sys.argv[19]
# key                 = args[20]
# flake_server        = args[21]
ads_revenue         = sys.argv[22]
endplus30           = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=30), "%Y-%m-%d")


if "None" in campaign:
    if ",'None'" in campaign:
        campaign1 = campaign.replace(",'None'","")
    else:
        campaign1 = campaign.replace("None','","")

if revenue != '' and ads_revenue =='':
    revenue1= "sum("+revenue
elif revenue == '' and ads_revenue !='':
    revenue1= "sum("+ads_revenue
else:
    revenue1= "(sum("+revenue+")+sum("+ads_revenue+")"

if revenue != '' and ads_revenue =='':
    revenue2 = revenue
elif revenue == '' and  ads_revenue !='':
    revenue2 = ads_revenue
else:
    revenue2 = revenue+" > 0 or "+ads_revenue

if "None" not in campaign:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, USERS, CAMPAIGN, PAYERS as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, sum (case when USERAGE <= 1 then REVENUE else 0 end) as D1REVENUE, sum (case when USERAGE <= 2 then REVENUE else 0 end) as D2REVENUE, sum (case when USERAGE <= 3 then REVENUE else 0 end) as D3REVENUE, sum (case when USERAGE <= 4 then REVENUE else 0 end) as D4REVENUE, sum (case when USERAGE <= 5 then REVENUE else 0 end) as D5REVENUE, sum (case when USERAGE <= 6 then REVENUE else 0 end) as D6REVENUE, sum (case when USERAGE <= 7 then REVENUE else 0 end) as D7REVENUE, sum (case when USERAGE <= 14 then REVENUE else 0 end) as D14REVENUE, sum (case when USERAGE <= 28 then REVENUE else 0 end) as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, "+campaigns+" as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+")*"+revenue_rate+" as REVENUE from "+table_name+"_live where ("+revenue2+" > 0) AND "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND "+campaigns+" in ( '"+campaign+"' ) group by 1,2,3,4) A , (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, count(distinct "+user_id+") as USERS, count(distinct (case when "+revenue2+"  > 0  then "+user_id+" else null end) ) as PAYERS from "+table_name+"_live where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' AND "+platforms+" in  ( '"+platform+"' ) AND "+countrys+" in ( '"+country+"' ) AND "+campaigns+" in ( '"+campaign+"' ) group by 1,2) B WHERE A.INSTALLDATE = B.INSTALLDATE and A.GAUSERACQUISITIONCHANNEL=B.GAUSERACQUISITIONCHANNEL group by 1,2,3,4,5 order by 1,2"
elif "None" not in campaign1:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, USERS, CAMPAIGN, PAYERS as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, sum (case when USERAGE <= 1 then REVENUE else 0 end) as D1REVENUE, sum (case when USERAGE <= 2 then REVENUE else 0 end) as D2REVENUE, sum (case when USERAGE <= 3 then REVENUE else 0 end) as D3REVENUE, sum (case when USERAGE <= 4 then REVENUE else 0 end) as D4REVENUE, sum (case when USERAGE <= 5 then REVENUE else 0 end) as D5REVENUE, sum (case when USERAGE <= 6 then REVENUE else 0 end) as D6REVENUE, sum (case when USERAGE <= 7 then REVENUE else 0 end) as D7REVENUE, sum (case when USERAGE <= 14 then REVENUE else 0 end) as D14REVENUE, sum (case when USERAGE <= 28 then REVENUE else 0 end) as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, "+campaigns+" as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+")*"+revenue_rate+" as REVENUE from "+table_name+"_live where ("+revenue2+" > 0) AND "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or "+campaigns+" in ( '"+campaign1+"' ) ) group by 1,2,3,4) A , (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, count(distinct "+user_id+") as USERS, count(distinct (case when "+revenue2+"  > 0  then "+user_id+" else null end) ) as PAYERS from "+table_name+"_live where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' AND "+platforms+" in  ( '"+platform+"' ) AND "+countrys+" in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' or "+campaigns+" in ( '"+campaign1+"' ) ) group by 1,2) B WHERE A.INSTALLDATE = B.INSTALLDATE and A.GAUSERACQUISITIONCHANNEL=B.GAUSERACQUISITIONCHANNEL group by 1,2,3,4,5 order by 1,2"
else:
    q1 = "select A.INSTALLDATE, A.GAUSERACQUISITIONCHANNEL as ACQUISITION_CHANNEL, USERS, CAMPAIGN, PAYERS as PAYERS, sum (case when USERAGE <= 0 then REVENUE else 0 end) as D0REVENUE, sum (case when USERAGE <= 1 then REVENUE else 0 end) as D1REVENUE, sum (case when USERAGE <= 2 then REVENUE else 0 end) as D2REVENUE, sum (case when USERAGE <= 3 then REVENUE else 0 end) as D3REVENUE, sum (case when USERAGE <= 4 then REVENUE else 0 end) as D4REVENUE, sum (case when USERAGE <= 5 then REVENUE else 0 end) as D5REVENUE, sum (case when USERAGE <= 6 then REVENUE else 0 end) as D6REVENUE, sum (case when USERAGE <= 7 then REVENUE else 0 end) as D7REVENUE, sum (case when USERAGE <= 14 then REVENUE else 0 end) as D14REVENUE, sum (case when USERAGE <= 28 then REVENUE else 0 end) as D28REVENUE, (dense_RANK() over (order by A.INSTALLDATE))::numeric as INSTALL_RANK, (dense_RANK() over (order by weekiso(A.INSTALLDATE)))::numeric as WEEK_RANK, (dense_RANK() over (order by CAMPAIGN))::numeric as CAMPAIGN_RANK from (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, "+campaigns+" as CAMPAIGN, DATEDIFF(DAY,"+user_start_date+","+event_date+" ) as USERAGE,"+revenue1+")*"+revenue_rate+" as REVENUE from "+table_name+"_live where ("+revenue2+" > 0) AND "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' and "+platforms+" in ( '"+platform+"' ) and "+countrys+"  in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '' ) group by 1,2,3,4) A , (select "+user_start_date+" as INSTALLDATE, "+acquisition_channel+" as GAUSERACQUISITIONCHANNEL, count(distinct "+user_id+") as USERS, count(distinct (case when "+revenue2+"  > 0  then "+user_id+" else null end) ) as PAYERS from "+table_name+"_live where "+user_start_date+" BETWEEN '"+start+"' and '"+end+"' AND "+event_date+" BETWEEN '"+start+"' and '"+endplus30+"' AND "+platforms+" in  ( '"+platform+"' ) AND "+countrys+" in ( '"+country+"' ) AND ("+campaigns+" is null or "+campaigns+" = '') group by 1,2) B WHERE A.INSTALLDATE = B.INSTALLDATE and A.GAUSERACQUISITIONCHANNEL=B.GAUSERACQUISITIONCHANNEL group by 1,2,3,4,5 order by 1,2"

   
conn = psycopg2.connect(dbname=game_name, user=delta_username, password=delta_password, host='data.deltadna.net', port='5432', sslmode='require')

cur = conn.cursor()
cur.execute(q1)
result = cur.fetchall()

for i in range(0,len(result)):
    print('("'+str(result[i][0])+'","'+str(result[i][1])+'"')
    print(',')
    print(result[i][2])
    print(',"'+str(result[i][3])+'"')
    print(',')
    print(result[i][4])
    print(',')
    print(result[i][5])
    print(',')
    print(result[i][6])
    print(',')
    print(result[i][7])
    print(',')
    print(result[i][8])
    print(',')
    print(result[i][9])
    print(',')
    print(result[i][10])
    print(',')
    print(result[i][11])
    print(',')
    print(result[i][12])
    print(',')
    print(result[i][13])
    print(',')
    print(result[i][14])
    print(',')
    print(result[i][15])
    print(',')
    print(result[i][16])
    print(',')
    print(result[i][17])
    print(')')
    if i < len(result)-1:
        print(',')

