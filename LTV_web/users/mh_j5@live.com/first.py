import sys
from datetime import datetime, timedelta
import snowflake.connector

startdate           = datetime.strftime(datetime.strptime(sys.argv[1], "%Y-%m-%d"), "%Y-%m-%d")
enddate             = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d"), "%Y-%m-%d")
game_name           = sys.argv[3].replace("_ZEN_EMJ_", ".")
table_name          = sys.argv[4]
event_date          = sys.argv[5]
user_start_date     = sys.argv[6]
platform            = sys.argv[7]
country             = sys.argv[8]
campaign            = sys.argv[9]
revenue             = sys.argv[10]
delta_username      = sys.argv[11]
delta_password      = sys.argv[12]
weeks               = int(sys.argv[13][0:2])+1
enddateweek         = datetime.strftime(datetime.strptime(sys.argv[2], "%Y-%m-%d") + timedelta(days=weeks-1), "%Y-%m-%d")
emaillogin          = sys.argv[14]
# key                 = args[15]
flake_server        = sys.argv[16].replace("_ZEN_EMJ_", ".")
user_id             = sys.argv[17]

con = snowflake.connector.connect(
    user=delta_username,
    password=delta_password,
    account=flake_server
    )

cur = con.cursor()
cur.execute("SELECT * FROM (SELECT "+event_date+" as EVENT_DATE, FIRST_VALUE("+platform+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS PLATFORM, FIRST_VALUE("+campaign+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS CAMPAIGN_NAME, FIRST_VALUE("+country+" IGNORE NULLS) OVER (PARTITION BY "+user_id+" ORDER BY "+event_date+") AS COUNTRY FROM "+game_name+"."+table_name+" WHERE "+user_start_date+" between '"+startdate+"' and '"+enddate+"' and "+event_date+" between '"+startdate+"' and '"+enddateweek+"' and "+revenue+" > 0) A GROUP BY 1,2,3,4 order by 1")
result = cur.fetchall()

for i in range(0,len(result)):
    print('("'+str(result[i][0])+'","'+str(result[i][1])+'","'+str(result[i][2])+'","'+str(result[i][3])+'")')
    if i < len(result)-1:
        print(',')