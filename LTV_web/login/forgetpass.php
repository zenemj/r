<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
      <link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
      <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
      <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />

      <script src="bootstrap3-typeahead.min.js"></script> 
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>
  <link type="text/css" rel="stylesheet" href="http://fakedomain.com/smilemachine/html.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="styles.css?v=<?php echo time();?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
      <title>Send Reset Password Link</title>
       <!-- CSS -->
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
   </head>
   <body>
     <div>
     <a href="../index.php" style="padding: 0% 0% 0% 0%; width:13%;"> <img src="1-01.png" alt="logo" width="10%" height="10%" style="margin-left:2%; float:left;"></a>
     </div>
      <div class="container">
      <br/>
          <div class="card" style="width:40%;margin-left:30%;">
            <div class="card-header text-center">
              Send Reset Password Link
            </div>
            <div class="card-body">
              <!-- <form action="../login/password-reset-token.php" method="post"> -->
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" id="email" class="form-control" id="email" aria-describedby="emailHelp">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <input type="button" name="password-reset-token" id="password-reset-token" class="btn btn-primary" value="Submit">
              <!-- </form> -->
              <br>
              <br>
              <div id ="message"></div>
            </div>
          </div>
      </div>
 
   </body>
</html>

<script>
$(document).ready(function(){
    $('#password-reset-token').click(function(){
        var email            = $("#email").val();
        $.ajax({
          url:"../login/password-reset-token.php",
          method:"POST",
          action:"",
          data:{email:email},
          success: function (data) {
            $("#message").html(data);
            // $(function() { $("#message").hide().fadeIn().delay(3000).fadeOut('slow');});
            // setTimeout(function() {location.reload();}, 4000); 
               }            
             });    
    });
});
</script>