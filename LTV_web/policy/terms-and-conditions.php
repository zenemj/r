<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



</head>
<link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<body>
<div>
     <a href="../index.php" > <img src="1-01.png" alt="logo" width="11%" height="11%"></a>
</div>
<br><br>
<div style="font-size: 115%;margin: 0 10% 0 10%">
<!-- <label style="margin-left: 13%; font-size:200%">Terms And Conditions</label> -->
<br>
<br>
<p>
<b>TERMS AND CONDITIONS</b> <br>
Last updated March 4, 2022 <br>
________________________________________

<h3>AGREEMENT TO TERMS</h3>

These Terms of Use constitute a legally binding agreement made between you, whether personally or on behalf of an entity (“you”, “user”, “customer”) and MandsLTV (“we”, “us” or “our”), concerning your access to and use of the www.mandsltv.com website as well as any other media form, media channel, mobile website or mobile application related, linked, or otherwise connected thereto (collectively, the “Site”, “Website”). <br> <br>

You agree that by accessing the Site, you have read, understood, and agree to be bound by all of these Terms of Use, which were created using MandsLTV’s terms and conditions generator. If you do not agree with all of these Terms of Use, then you are expressly prohibited from using the Site and you must discontinue use immediately.  <br> <br>

Supplemental terms and conditions or documents that may be posted on the Site from time to time are hereby expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or modifications to these Terms of Use at any time and for any reason.  <br> <br>

We will alert you about any changes by updating the “Last updated” date of these Terms of Use and we may update the Service with or without notifying you, and you waive any right to receive specific notice of each such change. 

It is your responsibility to periodically review these Terms of Use to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms of Use by your continued use of the Site after the date such revised Terms of Use are posted.  <br> <br>

The information provided on the Site is not intended for distribution to or use by any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation or which would subject us to any registration requirement within such jurisdiction or country.
Accordingly, those persons who choose to access the Site from other locations do so on their own initiative and are solely responsible for compliance with local laws, if and to the extent local laws are applicable.  <br> <br>

The Site is intended for users who are at least 18 years old. Persons under the age of 18 are not permitted to use or register for the Site. 

<h3>INTELLECTUAL PROPERTY RIGHTS</h3>

Unless otherwise indicated, the Site is our proprietary property and all source code, databases, functionality, software, website designs, audio, video, text, photographs, and graphics on the Site (collectively, the “Content”) and the trademarks, service marks, and logos contained therein (the “Marks”) are owned or controlled by us or licensed to us, and are protected by copyright and trademark laws and various other intellectual property rights and unfair competition laws of the Spain, foreign jurisdictions, and international conventions. <br> <br>
The Content and the Marks are provided on the Site “AS IS” for your information and personal use only. Except as expressly provided in these Terms of Use, no part of the Site and no Content or Marks may be copied, reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted, distributed, sold, licensed, or otherwise exploited for any commercial purpose whatsoever, without our express prior written permission.<br> <br>

Provided that you are eligible to use the Site, you are granted a limited license to access and use the Site and to download or print a copy of any portion of the Content to which you have properly gained access solely for your personal, non-commercial use. We reserve all rights not expressly granted to you in and to the Site, the Content and the Marks.

<h3>USER REPRESENTATIONS</h3>

By using the Site , you represent and warrant that: 
<div>
<ul>
<li> all registration information you submit will be true, accurate, current, and complete;</li>
<li> you will maintain the accuracy of such information and promptly update such registration information as necessary;</li>
<li> you have the legal capacity and you agree to comply with these Terms of Use; </li>
<li> you are not under the age of 18; </li>
<li> you are not a minor in the jurisdiction in which you reside, or if a minor, you have received parental permission to use the Site; </li>
<li> you will not access the Site through automated or non-human means, whether through a bot, script or otherwise; </li>
<li> you will not use the Site for any illegal or unauthorized purpose; </li>
<li> your use of the Site will not violate any applicable law or regulation.</li>
</ul>
</div>
If you provide any information that is untrue, inaccurate, not current, or incomplete, we have the right to suspend or terminate your account and refuse any and all current or future use of the Site (or any portion thereof). 

<h3>USER REGISTRATION</h3>

You may be required to register with the Site. You agree to keep your password confidential and will be responsible for all use of your account and password. We reserve the right to remove, reclaim, or change a username you select if we determine, in our sole discretion, that such username is inappropriate, obscene, or otherwise objectionable.
 
<h3>PRODUCTS</h3>

You may use all the data analysis services in MandsLTV's website in your Account.

<h3>PAYMENT</h3>

You agree to provide current, complete, and accurate payment and account information for your subscription made via the Site. You further agree to promptly use account and payment information, including email address, payment method, and payment card expiration date, so that we can complete your transactions and contact you as needed.<br><br>

You agree to pay all charges at the prices then in effect for your subscription and any applicable fees, and you authorize us to charge your chosen payment provider for any such amounts upon placing your order.<br><br>

We reserve the right to refuse any subscription order placed through the Site. We may, in our sole discretion, limit or cancel your subscription.<br><br>

<h3>REFUNDS POLICY</h3>

All subscriptions are final and no refund will be issued.

<h3>PROHIBITED ACTIVITIES</h3>

You may not access or use the Site for any purpose other than that for which we make the Site available. The Site may not be used in connection with any commercial endeavors except those that are specifically endorsed or approved by us. 

As a user of the Site, you agree not to:
<div>
<ul>
<li>systematically retrieve data or other content from the Site to create or compile, directly or indirectly, a collection, compilation, database, or directory without written permission from us.</li>
<li>make any unauthorized use of the Site, including collecting usernames and/or email addresses of users by electronic or other means for the purpose of sending unsolicited email, or creating user accounts by automated means or under false pretenses.</li>
<li>use the Site to advertise or offer to sell goods and services.</li>
<li>circumvent, disable, or otherwise interfere with security-related features of the Site, including features that prevent or restrict the use or copying of any Content or enforce limitations on the use of the Site and/or the Content contained therein.</li>
<li>engage in unauthorized framing of or linking to the Site.</li>
<li>trick, defraud, or mislead us and other users, especially in any attempt to learn sensitive account information such as user passwords.</li>
<li>make improper use of our support services or submit false reports of abuse or misconduct.</li>
<li>engage in any automated use of the system, such as using scripts to send comments or messages, or using any data mining, robots, or similar data gathering and extraction tools.</li>
<li>interfere with, disrupt, or create an undue burden on the Site or the networks or services connected to the Site.</li>
<li>attempt to impersonate another user or person or use the username of another user.</li>
<li>sell or otherwise transfer your profile.</li>
<li>use any information obtained from the Site in order to harass, abuse, or harm another person.</li>
<li>use the Site as part of any effort to compete with us or otherwise use the Site and/or the Content for any revenue-generating endeavor or commercial enterprise.</li>
<li>decipher, decompile, disassemble, or reverse engineer any of the software comprising or in any way making up a part of the Site.</li>
<li>attempt to bypass any measures of the Site designed to prevent or restrict access to the Site, or any portion of the Site.</li>
<li>harass, annoy, intimidate, or threaten any of our employees or agents engaged in providing any portion of the Site to you.</li>
<li>delete the copyright or other proprietary rights notice from any Content.</li>
<li>copy or adapt the Site’s software, including but not limited to Flash, PHP, HTML, JavaScript, or other code.</li>
<li>upload or transmit (or attempt to upload or to transmit) viruses, Trojan horses, or other material, including excessive use of capital letters and spamming (continuous posting of repetitive text), that interferes with any party’s uninterrupted use and enjoyment of the Site or modifies, impairs, disrupts, alters, or interferes with the use, features, functions, operation, or maintenance of the Site.</li>
<li>upload or transmit (or attempt to upload or to transmit) any material that acts as a passive or active information collection or transmission mechanism, including without limitation, clear graphics interchange formats (“gifs”), 1×1 pixels, web bugs, cookies, or other similar devices (sometimes referred to as “spyware” or “passive collection mechanisms” or “pcms”).</li>
<li>except as may be the result of standard search engine or Internet browser usage, use, launch, develop, or distribute any automated system, including without limitation, any spider, robot, cheat utility, scraper, or offline reader that accesses the Site, or using or launching any unauthorized script or other software.</li>
<li>disparage, tarnish, or otherwise harm, in our opinion, us and/or the Site.</li>
<li>use the Site in a manner inconsistent with any applicable laws or regulations.</li>
<li>other.</li>
</ul>
</div>
 
<h3>MOBILE APPLICATION LICENSE</h3>

Use License
If you access the Site via a mobile application, then we grant you a revocable, non-exclusive, non-transferable, limited right to install and use the mobile application on wireless electronic devices owned or controlled by you, and to access and use the mobile application on such devices strictly in accordance with the terms and conditions of this mobile application license contained in these Terms of Use. 

You shall not: 
<div>
<ul>
<li>decompile, reverse engineer, disassemble, attempt to derive the source code of, or decrypt the application; </li>
<li>make any modification, adaptation, improvement, enhancement, translation, or derivative work from the application; </li>
<li>violate any applicable laws, rules, or regulations in connection with your access or use of the application; </li>
<li>remove, alter, or obscure any proprietary notice (including any notice of copyright or trademark) posted by us or the licensors of the application; </li>
<li>use the application for any revenue generating endeavor, commercial enterprise, or other purpose for which it is not designed or intended; </li>
<li>make the application available over a network or other environment permitting access or use by multiple devices or users at the same time; </li>
<li>use the application for creating a product, service, or software that is, directly or indirectly, competitive with or in any way a substitute for the application; </li>
<li>use the application to send automated queries to any website or to send any unsolicited commercial e-mail;</li>
<li>use any proprietary information or any of our interfaces or our other intellectual property in the design, development, manufacture, licensing, or distribution of any applications, accessories, or devices for use with the application.</li>
</ul>
</div>
Apple and Android Devices
The following terms apply when you use a mobile application obtained from either the Apple Store or Google Play (each an “App Distributor”) to access the Site: 
<div>
<ul>
<li>the license granted to you for our mobile application is limited to a non-transferable license to use the application on a device that utilizes the Apple iOS or Android operating systems, as applicable, and in accordance with the usage rules set forth in the applicable App Distributor’s terms of service; </li>
<li>we are responsible for providing any maintenance and support services with respect to the mobile application as specified in the terms and conditions of this mobile application license contained in these Terms of Use or as otherwise required under applicable law, and you acknowledge that each App Distributor has no obligation whatsoever to furnish any maintenance and support services with respect to the mobile application; </li>
<li>in the event of any failure of the mobile application to conform to any applicable warranty, you may notify the applicable App Distributor, and the App Distributor, in accordance with its terms and policies, may refund the purchase price, if any, paid for the mobile application, and to the maximum extent permitted by applicable law, the App Distributor will have no other warranty obligation whatsoever with respect to the mobile application; </li>
<li>you represent and warrant that (i) you are not located in a country that is subject to a Spain government embargo, or that has been designated by the Spain government as a “terrorist supporting” country and (ii) you are not listed on any Spain government list of prohibited or restricted parties; </li>
<li>you must comply with applicable third-party terms of agreement when using the mobile application, e.g., if you have a VoIP application, then you must not be in violation of their wireless data service agreement when using the mobile application; </li>
<li>you acknowledge and agree that the App Distributors are third-party beneficiaries of the terms and conditions in this mobile application license contained in these Terms of Use, and that each App Distributor will have the right (and will be deemed to have accepted the right) to enforce the terms and conditions in this mobile application license contained in these Terms of Use against you as a third-party beneficiary thereof.  </li>
</ul>
</div>
<h3>SUBMISSIONS</h3>

You acknowledge and agree that any questions, comments, suggestions, ideas, feedback, or other information regarding the Site ("Submissions") provided by you to us are non-confidential and shall become our sole property. We shall own exclusive rights, including all intellectual property rights, and shall be entitled to the unrestricted use and dissemination of these Submissions for any lawful purpose, commercial or otherwise, without acknowledgment or compensation to you. <br><br>
You hereby waive all moral rights to any such Submissions, and you hereby warrant that any such Submissions are original with you or that you have the right to submit such Submissions. You agree there shall be no recourse against us for any alleged or actual infringement or misappropriation of any proprietary right in your Submissions. 

<h3>THIRD-PARTY WEBSITES AND CONTENT</h3>

The Site may contain (or you may be sent via the Site) links to other websites ("Third-Party Websites") as well as articles, photographs, text, graphics, pictures, designs, music, sound, video, information, applications, software, and other content or items belonging to or originating from third parties ("Third-Party Content"). <br><br>

Such Third-Party Websites and Third-Party Content are not investigated, monitored, or checked for accuracy, appropriateness, or completeness by us, and we are not responsible for any Third Party Websites accessed through the Site or any Third-Party Content posted on, available through, or installed from the Site, including the content, accuracy, offensiveness, opinions, reliability, privacy practices, or other policies of or contained in the Third-Party Websites or the Third-Party Content. <br><br>

Inclusion of, linking to, or permitting the use or installation of any Third-Party Websites or any Third-Party Content does not imply approval or endorsement thereof by us. If you decide to leave the Site and access the Third-Party Websites or to use or install any Third-Party Content, you do so at your own risk, and you should be aware these Terms of Use no longer govern. <br><br>

You should review the applicable terms and policies, including privacy and data gathering practices, of any website to which you navigate from the Site or relating to any applications you use or install from the Site. Any purchases you make through Third-Party Websites will be through other websites and from other companies, and we take no responsibility whatsoever in relation to such purchases which are exclusively between you and the applicable third party. <br><br>

You agree and acknowledge that we do not endorse the products or services offered on Third-Party Websites and you shall hold us harmless from any harm caused by your purchase of such products or services. Additionally, you shall hold us harmless from any losses sustained by you or harm caused to you relating to or resulting in any way from any Third-Party Content or any contact with Third-Party Websites. <br><br>

<h3>ADVERTISERS</h3>

We may allow advertisers to display their advertisements and other information in certain areas of the Site, such as sidebar advertisements or banner advertisements. If you are an advertiser, you shall take full responsibility for any advertisements you place on the Site and any services provided on the Site or products sold through those advertisements. <br><br>

Further, as an advertiser, you warrant and represent that you possess all rights and authority to place advertisements on the Site, including, but not limited to, intellectual property rights, publicity rights, and contractual rights.  <br><br>

As an advertiser, you agree that such advertisements are subject to our Policy, and you understand and agree there will be no refund or other compensation for any issues. We simply provide the space to place such advertisements, and we have no other relationship with advertisers.   <br><br>
 
<h3>SITE MANAGEMENT</h3>

We reserve the right, but not the obligation, to: 
<div>
<ul>
<li> monitor the Site for violations of these Terms of Use; </li>
<li> take appropriate legal action against anyone who, in our sole discretion, violates the law or these Terms of Use, including without limitation, reporting such user to law enforcement authorities; </li>
<li> in our sole discretion and without limitation, refuse, restrict access to, limit the availability of, or disable (to the extent technologically feasible) any of your Contributions or any portion thereof; </li>
<li> in our sole discretion and without limitation, notice, or liability, to remove from the Site or otherwise disable all files and content that are excessive in size or are in any way burdensome to our systems; </li>
<li> otherwise manage the Site in a manner designed to protect our rights and property and to facilitate the proper functioning of the Site.</li>
</ul>
</div>

<h3>PRIVACY POLICY</h3>

We care about data privacy and security. Please review our <a href="https://www.mandsltv.com/policy/privacy-policy.php">Privacy Policy</a> posted on the Site. By using the Site, you agree to be bound by our Privacy Policy, which is incorporated into these Terms of Use. Please be advised the Site is hosted in Spain. <br><br>

If you access the Site from the European Union, Asia, or any other region of the world with laws or other requirements governing personal data collection, use, or disclosure that differ from applicable laws in Spain, then through your continued use of the Site, you are transferring your data to Spain, and you expressly consent to have your data transferred to and processed in Spain.<br><br>


<h3>TERM AND TERMINATION</h3>

These Terms of Use shall remain in full force and effect while you use the Site. WITHOUT LIMITING ANY OTHER PROVISION OF THESE TERMS OF USE, WE RESERVE THE RIGHT TO, IN OUR SOLE DISCRETION AND WITHOUT NOTICE OR LIABILITY, DENY ACCESS TO AND USE OF THE SITE (INCLUDING BLOCKING CERTAIN IP ADDRESSES), TO ANY PERSON FOR ANY REASON OR FOR NO REASON, INCLUDING WITHOUT LIMITATION FOR BREACH OF ANY REPRESENTATION, WARRANTY, OR COVENANT CONTAINED IN THESE TERMS OF USE OR OF ANY APPLICABLE LAW OR REGULATION. WE MAY TERMINATE YOUR USE OR PARTICIPATION IN THE SITE OR DELETE YOUR ACCOUNT AND ANY CONTENT OR INFORMATION THAT YOU POSTED AT ANY TIME, WITHOUT WARNING, IN OUR SOLE DISCRETION. <br><br>

If we terminate or suspend your account for any reason, you are prohibited from registering and creating a new account under your name, a fake or borrowed name, or the name of any third party, even if you may be acting on behalf of the third party. <br><br>

In addition to terminating or suspending your account, we reserve the right to take appropriate legal action, including without limitation pursuing civil, criminal, and injunctive redress.<br><br>

<h3>MODIFICATIONS AND INTERRUPTIONS</h3>

We reserve the right to change, modify, or remove the contents of the Site at any time or for any reason at our sole discretion without notice. However, we have no obligation to update any information on our Site. We also reserve the right to modify or discontinue all or part of the Site without notice at any time.<br><br>

We will not be liable to you or any third party for any modification, price change, suspension, or discontinuance of the Site. <br><br>

We cannot guarantee the Site will be available at all times. We may experience hardware, software, or other problems or need to perform maintenance related to the Site, resulting in interruptions, delays, or errors. We reserve the right to change, revise, update, suspend, discontinue, or otherwise modify the Site at any time or for any reason without notice to you. <br><br>

You agree that we have no liability whatsoever for any loss, damage, or inconvenience caused by your inability to access or use the Site during any downtime or discontinuance of the Site. Nothing in these Terms of Use will be construed to obligate us to maintain and support the Site or to supply any corrections, updates, or releases in connection therewith.

<h3>GOVERNING LAW</h3>

These Terms of Use and your use of the Site are governed by and construed in accordance with the laws of Spain applicable to agreements made and to be entirely performed within Spain, without regard to its conflict of law principles.

<h3>DISPUTE RESOLUTION AND LAW</h3>
If a dispute arises between you and MandsLTV, we strongly encourage you to first contact us directly. These Terms of Service and any dispute arising out of or related to it or Privacy Policy or the Service shall be governed in all respects by the laws of Spain, without regard to conflict of law decisions.

<h3>CORRECTIONS</h3>

There may be information on the Site that contains typographical errors, inaccuracies, or omissions, including descriptions, pricing, availability, and various other information. We reserve the right to correct any errors, inaccuracies, or omissions and to change or update the information on the Site at any time, without prior notice.

<h3>DISCLAIMER</h3>

THE SITE IS PROVIDED ON AN AS-IS AND AS-AVAILABLE BASIS. YOU AGREE THAT YOUR USE OF THE SITE SERVICES WILL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SITE AND YOUR USE THEREOF, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE MAKE NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THE SITE’S CONTENT OR THE CONTENT OF ANY WEBSITES LINKED TO THIS SITE AND WE WILL ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY: 
<div>
<ul>
<li> ANY DECISIONS BASED ON THE RESULTS OF THE ANALYSIS ON THE WEBSITE, AS THE SITE PROVIDES PREDICTIVE AND NON-PREDICTIVE ANALYZES BASED ON THE DATA PROVIDED BY THE USER AND WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SITE, SITE'S RESULTS, AND YOUR USE THEREOF, </li>
<li> ERRORS, MISTAKES, OR INACCURACIES OF CONTENT AND MATERIALS, </li>
<li> PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE SITE, </li>
<li> ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, </li>
<li> ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE SITE, </li>
<li> ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH THE SITE BY ANY THIRD PARTY, AND/OR </li>
<li> ANY ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SITE. WE DO NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SITE, ANY HYPERLINKED WEBSITE, OR ANY WEBSITE OR MOBILE APPLICATION FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND WE WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND ANY THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</li>
</ul>
</div>
<h3>LIMITATIONS OF LIABILITY</h3>

IN NO EVENT WILL WE OR OUR DIRECTORS, EMPLOYEES, OR AGENTS BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL, OR PUNITIVE DAMAGES, INCLUDING LOST PROFIT, LOST REVENUE, LOSS OF DATA, OR OTHER DAMAGES ARISING FROM YOUR USE OF THE SITE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, OUR LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO YOUR SUBSCRIPTION AMOUNT PAID , IF ANY, BY YOU TO US DURING THE 30 DAYS PERIOD PRIOR TO ANY CAUSE OF ACTION ARISING.

<h3>INDEMNIFICATION</h3>

You agree to defend, indemnify, and hold us harmless, including our subsidiaries, affiliates, and all of our respective officers, agents, partners, and employees, from and against any loss, damage, liability, claim, or demand, including reasonable attorneys’ fees and expenses, made by any third party due to or arising out of: 
<div>
<ul>
<li> your Contributions; </li>
<li> use of the Site; </li>
<li> breach of these Terms of Use; </li>
<li> any breach of your representations and warranties set forth in these Terms of Use; </li>
<li> your violation of the rights of a third party, including but not limited to intellectual property rights; or </li>
<li> any overt harmful act toward any other user of the Site with whom you connected via the Site. </li>
</ul>
</div>
Notwithstanding the foregoing, we reserve the right, at your expense, to assume the exclusive defense and control of any matter for which you are required to indemnify us, and you agree to cooperate, at your expense, with our defense of such claims. We will use reasonable efforts to notify you of any such claim, action, or proceeding which is subject to this indemnification upon becoming aware of it. 

<h3>USER DATA</h3>

We will maintain certain data that you transmit to the Site for the purpose of managing the performance of the Site, as well as data relating to your use of the Site. Although we perform regular routine backups of data, you are solely responsible for all data that you transmit or that relates to any activity you have undertaken using the Site. <br><br>

You agree that we shall have no liability to you for any loss or corruption of any such data, and you hereby waive any right of action against us arising from any such loss or corruption of such data.<br><br>

ELECTRONIC COMMUNICATIONS, TRANSACTIONS, AND SIGNATURES VISITING THE SITE, SENDING US EMAILS, AND COMPLETING ONLINE FORMS CONSTITUTE ELECTRONIC COMMUNICATIONS. YOU CONSENT TO RECEIVE ELECTRONIC COMMUNICATIONS, AND YOU AGREE THAT ALL AGREEMENTS, NOTICES, DISCLOSURES, AND OTHER COMMUNICATIONS WE PROVIDE TO YOU ELECTRONICALLY, VIA EMAIL AND ON THE SITE, SATISFY ANY LEGAL REQUIREMENT THAT SUCH COMMUNICATION BE IN WRITING. YOU HEREBY AGREE TO THE USE OF ELECTRONIC SIGNATURES, CONTRACTS, ORDERS, AND OTHER RECORDS, AND TO ELECTRONIC DELIVERY OF NOTICES, POLICIES, AND RECORDS OF TRANSACTIONS INITIATED OR COMPLETED BY US OR VIA THE SITE. <BR><BR>

You hereby waive any rights or requirements under any statutes, regulations, rules, ordinances, or other laws in any jurisdiction which require an original signature or delivery or retention of non-electronic records, or to payments or the granting of credits by any means other than electronic means.

<h3>NOTICES AND CONTACT US</h3>

We may notify you via postings on www.mandsltv.com , and via e-mail or any other communications means to contact information you provide to us. All notices given by you are required from you under these Terms of Service or the MandsLTV Privacy Policy shall be in writing and addressed to: MandsLTV, Calle Ebro 21, Planta baja, 41806, Sevilla, Spain. Any notices that you provide without compliance in this Section on Notices shall have no legal effect.<br><br>

In order to resolve a complaint regarding the Site or to receive further information regarding use of the Site, please contact us at:<br>
<a href="mandsltv@mandsltv.com">mandsltv@mandsltv.com</a> or you can fill in <a href="https://www.mandsltv.com/contactus/contact.php">this form</a>.

<h3>MISCELLANEOUS</h3>

These Terms of Use and any policies or operating rules posted by us on the Site or in respect to the Site constitute the entire agreement and understanding between you and us. Our failure to exercise or enforce any right or provision of these Terms of Use shall not operate as a waiver of such right or provision. <br><br>

These Terms of Use operate to the fullest extent permissible by law. We may assign any or all of our rights and obligations to others at any time. We shall not be responsible or liable for any loss, damage, delay, or failure to act caused by any cause beyond our reasonable control. <br><br>

If any provision or part of a provision of these Terms of Use is determined to be unlawful, void, or unenforceable, that provision or part of the provision is deemed severable from these Terms of Use and does not affect the validity and enforceability of any remaining provisions. There is no joint venture, partnership, employment or agency relationship created between you and us as a result of these Terms of Use or use of the Site. <br><br>

You agree that these Terms of Use will not be construed against us by virtue of having drafted them. You hereby waive any and all defenses you may have based on the electronic form of these Terms of Use and the lack of signing by the parties hereto to execute these Terms of Use.
<br><br><br><br><br>
</p>
</div>

<div class="container">
    <div class="bottom-left"> <p style="font-size:75%;"> Copyright © 2021 MANDSLTV.COM | All Rights Reserved</p> </div> 
</div>
</body>
</html>