<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<body>
<div>
     <a href="../index.php" > <img src="1-01.png" alt="logo" width="11%" height="11%"></a>
</div>
<br><br>
<div style="font-size: 115%;margin: 0 10% 0 10%">
<label style="margin-left: 13%; font-size:200%">Privacy Policy, Cookie Policy and Cookies Settings</label>
<br>
<br>
<p>
<h3>1. Introduction</h3>

The privacy of our website visitors is very important to us, and we are committed to safeguarding it. This policy explains what we will do with your personal information.
Consenting to our use of cookies in accordance with the terms of this policy when you first visit our website permits us to use cookies every time you visit our website.

<h3>2. Credit</h3>
This document was created by MANDSLTV (www.mandsltv.com).
Data Protection Department: mandsltv@mandsltv.com.

<h3>3. Collecting personal information</h3>

The following types of personal information may be collected, stored, and used:
<div>
<ul>
<li>Information about your computer including your IP address, geographical location, browser type and version, and operating system;</li>
<li>Information about your visits to and use of this website including the referral source, length of visit, page views, and website navigation paths;</li>
<li>Information, such as your email address, that you enter when you register with our website;</li>
<li>Information that you enter when you create a profile on our website. For example, your name, profile pictures, gender, birthday, relationship status, interests and hobbies, educational details, and employment details;</li>
<li>Information, such as your name and email address, that you enter in order to set up subscriptions to our emails and/or newsletters;</li>
<li>Information that you enter while using the services on our website;</li>
<li>Information that is generated while using our website, including when, how often, and under what circumstances you use it;</li>
<li>Information relating to anything you purchase, services you use, or transactions you make through our website, which includes your name, address, telephone number, email address, and credit card details;</li>
<li>Information that you post to our website with the intention of publishing it on the internet, which includes your username, profile pictures, and the content of your posts;</li>
<li>Information contained in any communications that you send to us by email or through our website, including its communication content and metadata;</li>
<li>Any other personal information that you send to us.</li>
</ul>
</div>
Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing of that personal information in accordance with this policy.

<h3>4. Using your personal information</h3>

Personal information submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website. We may use your personal information for the following:
    
<div>
<ul>
<li>administering our website and business;</li>
<li>personalizing our website for you;</li>
<li>enabling your use of the services available on our website;</li>
<li>sending you goods purchased through our website (It is NOT available in www.mandsltv.com);</li>
<li>supplying services purchased through our website;</li>
<li>sending statements, invoices, and payment reminders to you, and collecting payments from you;</li>
<li>sending you non-marketing commercial communications;</li>
<li>sending you email notifications that you have specifically requested;</li>
<li>sending you our email newsletter, if you have requested it (you can inform us at any time if you no longer require the newsletter);</li>
<li>sending you marketing communications relating to our business or the businesses of carefully-selected third parties which we think may be of interest to you, by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications);</li>
<li>providing third parties with statistical information about our users (but those third parties will not be able to identify any individual user from that information);</li>
<li>dealing with inquiries and complaints made by or about you relating to our website;</li>
<li>keeping our website secure and prevent fraud;</li>
<li>verifying compliance with the terms and conditions governing the use of our website (including monitoring private messages sent through our website private messaging service); and</li>
<li>other uses.</li>
</ul>
</div>
If you submit personal information for publication on our website, we will publish and otherwise use that information in accordance with the license you grant to us.

Your privacy settings can be used to limit the publication of your information on our website and can be adjusted using privacy controls on the website.

We will not, without your express consent, supply your personal information to any third party for their or any other third party's direct marketing.

<h3>5. Disclosing personal information</h3>

We may disclose your personal information to any of our employees, officers, insurers, professional advisers, agents, suppliers, or subcontractors as reasonably necessary for the purposes set out in this policy.

We may disclose your personal information to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) as reasonably necessary for the purposes set out in this policy.

We may disclose your personal information:

to the extent that we are required to do so by law;
in connection with any ongoing or prospective legal proceedings;
in order to establish, exercise, or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);
to the purchaser (or prospective purchaser) of any business or asset that we are (or are contemplating) selling; and
to any person who we reasonably believe may apply to a court or other competent authority for disclosure of that personal information where, in our reasonable opinion, such court or authority would be reasonably likely to order disclosure of that personal information.
Except as provided in this policy, we will not provide your personal information to third parties.

<h3>6. International data transfers</h3>

Information that we collect may be stored, processed in, and transferred between any of the countries in which we operate in order to enable us to use the information in accordance with this policy.
Information that we collect may be transferred to the following countries which do not have data protection laws equivalent to those in force in the European Economic Area: the United States of America, Russia, Japan, China, and India.
Personal information that you publish on our website or submit for publication on our website may be available, via the internet, around the world. We cannot prevent the use or misuse of such information by others.
You expressly agree to the transfers of personal information described in this Section 6.

<h3>7. Retaining personal information</h3>

This Section 7 sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations regarding the retention and deletion of personal information.
Personal information that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.
Notwithstanding the other provisions of this Section 7, we will retain documents (including electronic documents) containing personal data:
to the extent that we are required to do so by law;
if we believe that the documents may be relevant to any ongoing or prospective legal proceedings; and
in order to establish, exercise, or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk).

<h3>8. Security of your personal information</h3>

We will take reasonable technical and organizational precautions to prevent the loss, misuse, or alteration of your personal information.
We will store all the personal information you provide on our secure (password- and firewall-protected) servers.
All electronic financial transactions entered into through our website will be protected by encryption technology.
You acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.
You are responsible for keeping the password you use for accessing our website confidential; we will not ask you for your password (except when you log in to our website).

<h3>9. Amendments</h3>

We may update this policy from time to time by publishing a new version on our website. You should check this page occasionally to ensure you understand any changes to this policy. We may notify you of changes to this policy by email or through the private messaging system on our website.

<h3>10. Your rights</h3>

You may instruct us to provide you with any personal information we hold about you; provision of such information will be subject to the following:

the payment of a fee; and
the supply of appropriate evidence of your identity.
We may withhold personal information that you request to the extent permitted by law.

You may instruct us at any time not to process your personal information for marketing purposes.

In practice, you will usually either expressly agree in advance to our use of your personal information for marketing purposes, or we will provide you with an opportunity to opt out of the use of your personal information for marketing purposes.

<h3>11. Third party websites</h3>

Our website includes hyperlinks to, and details of, third party websites. We have no control over, and are not responsible for, the privacy policies and practices of third parties.

<h3>12. Updating information</h3>

Please let us know if the personal information that we hold about you needs to be corrected or updated.

<h3>13. Cookies</h3>

Our website uses cookies. A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server. Cookies may be either “persistent” cookies or “session” cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed. Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.
<br>
The names of the cookies that we use on our website, and the purposes for which they are used, are set out below:
<div>
<ol>
<li>We use session cookies (PHPSISSD) to personalize the website for each user and identify the session by the application server.	</li>
<li>We use Google Analytics and Adwords on our website to recognize a computer when a user visits the website / track users as they navigate the website / improve the website's usability / analyze the use of the website / administer the website / prevent fraud and improve the security of the website / personalize the website for each user / target advertisements which may be of particular interest to specific users.</li>
</ol>
</div>

Most browsers allow you to refuse to accept cookies. For example:
<div>
<ol>
<li>in Internet Explorer (version 10) you can block cookies using the cookie handling override settings available by clicking “Tools,” “Internet Options,” “Privacy,” and then “Advanced”;</li>
<li>in Firefox (version 24) you can block all cookies by clicking “Tools,” “Options,” “Privacy,” selecting “Use custom settings for history” from the drop-down menu, and unticking “Accept cookies from sites”; and</li>
<li>in Chrome (version 29), you can block all cookies by accessing the “Customize and control” menu, and clicking “Settings,” “Show advanced settings,” and “Content settings,” and then selecting “Block sites from setting any data” under the “Cookies” heading.</li>
</ol>
</div>

<div>
<ul>
    <li>How can I configure cookies?<br>

        You have the option to exercise your right to block, delete and reject the use of cookies via the pop-up which is shown when you access the website. You can also do so by modifying the options of your browser.
            <ul>
            <li>Internet Explorer: Tools > Internet Options > Privacy > Advanced.</li>
            <li>Microsoft Edge: Settings > Advanced settings > Privacy and services > Cookies.</li>
            <li>Mozilla Firefox: Tools > Options > Privacy > History > Custom settings.</li>
            <li>Google Chrome: Settings > Show advanced options > Privacy > Content settings.</li>
            <li>Safari (Apple): Preferences > Security.</li>
            <li>Opera (Opera Software): Settings > Options > Advanced > Cookies.</li>
            <li>If you use a browser distinct from those above, please consult its policy for the installation, use and blocking of cookies.</li>
            <ul>
    </li>
</ul>
</div>
<div>
<ul>
    <li>Options to disable cookies and opt-out<br>

        Use the tool from www.youronlinechoices.com to find useful information and to configure your cookies preferences provider by provider.<br>

        There are more third party tools which enable users to detect the cookies of each website they visit and to manage their deactivation. For example:
        <ul>
        <li><a href="https://www.ghostery.com/">Ghostery</a>.</li>
        <li><a href="https://tools.google.com/dlpage/gaoptout?hl=en">Google Analytics Opt-out Browser Add-on</a>.</li>
        <li><a href="https://optout.networkadvertising.org/?c=1">Disable advertising cookies, from NAI member companies</a> (Network Advertising Initiative).</li>
        <li>For more information about cookies, go to: <a href="https://www.aboutcookies.org/">AboutCookies.org</a> and <a href="https://www.allaboutcookies.org/verify">AllAboutCookies.org</a>.</li>
        </ul>
    </li>
</ul>
        </div>
Blocking all cookies will have a negative impact upon the usability of many websites. If you block cookies, you will not be able to use all the features on our website.
<br>
<br>
You can delete cookies already stored on your computer—for example:
<div>    
<ul>       
<li>in Internet Explorer (version 10), you must manually delete cookie files (you can find instructions for doing so at <a href="http://support.microsoft.com/kb/278835">http://support.microsoft.com/kb/278835</a> );</li>
<li>in Firefox (version 24), you can delete cookies by clicking “Tools,” “Options,” and “Privacy”, then selecting “Use custom settings for history”, clicking “Show Cookies,” and then clicking “Remove All Cookies”; and</li>
<li>in Chrome (version 29), you can delete all cookies by accessing the “Customize and control” menu, and clicking “Settings,” “Show advanced settings,” and “Clear browsing data,” and then selecting “Delete cookies and other site and plug-in data” before clicking “Clear browsing data.”</li>
</ul>
</div>
Deleting cookies will have a negative impact on the usability of many websites.
<br><br><br><br><br>
</p>
</div>

<div class="container">
    <!-- <img src="gray2.jpg" alt="last" width="100%" style="box-shadow: 0 0 15px 0 #888;">  -->
    <div class="bottom-left"> <p style="font-size:75%;"> Copyright © 2021 MANDSLTV.COM | All Rights Reserved.</p> </div> 
</div>
</body>
</html>