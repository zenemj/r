<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../images/apple-touch-icon.png" rel="apple-touch-icon" />
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />


<script src="bootstrap3-typeahead.min.js"></script> 
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css"/>
  <link type="text/css" rel="stylesheet" href="http://fakedomain.com/smilemachine/html.css" />
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <link rel="stylesheet" href="styles.css?v=<?php echo time();?>">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=button] {
  background-color: #1381f0;
  color: white;
  padding: 12px 20px;
  border: none;
  font-size: 100%;
  border-radius: 4px;
  cursor: pointer;
}

input[type=button]:hover {
  background-color: #0d63ba;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
</head>
<body>
<div>
     <a href="../index.php" > <img src="1-01.png" alt="logo" width="11%" height="11%"></a>
</div>

<div style="margin: 0 30% 0 30%">
<div id = "message"></div>
<h3>Contact Us:</h3>

<div class="container">
  <!-- <form action="/action_page.php"> -->
  <div class="form-group">
    <label>First Name</label>
    <input type="text" id="fname" name="fname" placeholder="Your name..">
</div>
  <div class="form-group">
    <label>Last Name</label>
    <input type="text" id="lname" name="lname" placeholder="Your last name..">
</div>
<div class="form-group">
    <label>Email</label>
    <input type="text" id="email" name="email" placeholder="Your Email..">
</div>
<div class="form-group">
    <label>Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
</div>
<div class="form-group">
    <input type="button" name="contactus" id="contactus" class="btn btn-primary" value="Submit">
</div>
  <!-- </form> -->
</div>
<p>Or you can email us via this email: <a href="mandsltv@mandsltv.com"><b>mandsltv@mandsltv.com</b></a></p>
<div class="bottom-left" style="position: fixed; bottom: 0; left: 0;"> <p style="font-size:75%;">&nbsp Copyright © 2022 MANDSLTV.COM | All Rights Reserved</p> </div> 
</div>
</body>
</html>

<script>
$(document).ready(function(){
    $('#contactus').click(function(){
        var fname            = $("#fname").val();
        var lname            = $("#lname").val();
        var email            = $("#email").val();
        var subject          = $("#subject").val();

        if(email=='' || fname=='' || lname=='' ||subject==''){alert("All fields are mandatory and can't be empty.");}
        else{
        $.ajax({
          url:"../contactus/contactsubject.php",
          method:"POST",
          data:{email:email, fname:fname, lname:lname, subject:subject},
          success: function (data) {
            $("#message").html(data);
            setTimeout(function() {location.reload();}, 3500);  

                }   
               
                          
             });}
       
        });
});
</script>