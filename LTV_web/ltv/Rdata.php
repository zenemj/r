<?php
session_start();
include('../conn.php');
if(isset($_POST["platform"],$_POST["country"],$_POST["campaign"],$_POST["from_date"],$_POST["revenue"],$_POST["adsrevenue"]
,$_POST["to_date"],$_POST["games"],$_POST["weeks"],$_SESSION["id"]))
{   
  
    $end       = $_POST["to_date"];
    $start     = $_POST["from_date"];
    $campaign  = join("','", $_POST["campaign"]);
    $platform  = join("','", $_POST["platform"]);
    $country   = join("','", $_POST["country"]);
    $weeks     = $_POST["weeks"];
    $email     = $_SESSION["id"];
    $games     = $_POST["games"];
    $iaprevenue= $_POST["revenue"];
    $adsrevenue= $_POST["adsrevenue"];

    
    $query = "SELECT * FROM games where game_name_short='$games' and email='$email'";
    $statement = $connect->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    foreach($result as $row)
    { $game_name = $row['game_name']; 
      $table_name = $row['table_name'];
      $event_date = $row['event_date'];
      $user_id = $row['user_id'];
      $user_start_date = $row['user_start_date'];
      $platforms = $row['platform'];
      $countrys = $row['country'];
      $campaigns = $row['campaign'];
      $acquisition_channel = $row['acquisition_channel'];      
      $revenue = $row['revenue'];
      $ads_revenue = $row['ads_revenue'];
      $revenue_rate = $row['revenue_rate'];
    }
    
    if($iaprevenue == "checked" && $adsrevenue != "checked"){ $ads_revenue = '';} elseif($iaprevenue != "checked" && $adsrevenue == "checked"){ $revenue = '';}

    $query1 = "SELECT delta_username,delta_password, snowflake_server FROM login where emaillogin='$email'";
    $statement1 = $connect->prepare($query1);
    $statement1->execute();
    $result1 = $statement1->fetchAll();
    foreach($result1 as $row)
    { $delta_username = $row['delta_username']; 
      $delta_password = $row['delta_password'];
      $snowflake_server = $row['snowflake_server'];      
    }
    
    $dir       = '../users/'.$email.'/';
    $files     = glob($dir.'*.{json,Json,JSON}', GLOB_BRACE); //scandir($dir);  
    $key    = '../users/'.$email.'/';
    foreach ($files as $filename) {        
            $key .= basename($filename);
    }

    $editemail = str_replace(".", "_" , str_replace("@","_", $email));
    $query3 = 'delete FROM '.$editemail.'_ltvdata where DaySinceInstall >= 0';
    $statement3 = $connect->prepare($query3);
    $statement3->execute();
    

    if($snowflake_server ==""){
    $ltv = shell_exec('python3 ../users/'.$email.'/ltv.py "'.$start.'" "'.$end.'" "'.$weeks.'" "'.$game_name.'" "'.$table_name.'" "'.$event_date.'" "'.$user_start_date.'" "'.$platform.'" "'.$platforms.'" "'.$country.'" "'.$countrys.'" "'.$campaign.'" "'.$campaigns.'" "'.$acquisition_channel.'" "'.$revenue.'" "'.$revenue_rate.'" "'.$delta_username.'" "'.$delta_password.'" "'.$email.'" "'.$user_id.'" "'.$key.'" "'.$snowflake_server.'" "'.$ads_revenue.'" 2>&1');

    }
    else{
    $ltv0 = shell_exec('python ../users/'.$email.'/ltv.py "'.$start.'" "'.$end.'" "'.$weeks.'" "'.$game_name.'" "'.$table_name.'" "'.$event_date.'" "'.$user_start_date.'" "'.$platform.'" "'.$platforms.'" "'.$country.'" "'.$countrys.'" "'.$campaign.'" "'.$campaigns.'" "'.$acquisition_channel.'" "'.$revenue.'" "'.$revenue_rate.'" "'.$delta_username.'" "'.$delta_password.'" "'.$email.'" "'.$user_id.'" "'.$key.'" "'.$snowflake_server.'" "'.$ads_revenue.'" 2>&1');
    
    $ltv = shell_exec('python3 ../users/'.$email.'/ltv2.py "'.$ltv0.'" 2>&1');
    }

	  $ltvdata = 'INSERT INTO '.$editemail.'_ltvdata VALUES '.$ltv;
    $statement2 = $connect->prepare($ltvdata);
    $statement2->execute();
   
  
   
    $query4 = 'select round(avg(total_users),0) as total_users, 
                      round(avg(revenue),2) as total_revenue,
                      round(avg(revenue/total_users),3) as ARPU,
                      round(avg(total_payers),0) as total_payers
              FROM '.$editemail.'_ltvdata';
    $statement4 = $connect->prepare($query4);
    $statement4->execute();
    $result4 = $statement4->fetchAll();
    foreach($result4 as $row){$users         = $row['total_users'];
                              $total_revenue = $row['total_revenue'];
                              $ARPU          = $row['ARPU'];
                              $payers        = $row['total_payers'];                              
                            }

	
    $date=date_create($end);
    date_add($date,date_interval_create_from_date_string($weeks));
    echo substr($games,0,35)." estimated LTV & ROAS from ".$start." to ".date_format($date,"Y-m-d").",".$users.",".$total_revenue.",".$ARPU.",".$payers;
    // echo $campaign;
}

?>