
<?php
session_start();
include('../conn.php');

if(isset($_POST["data1"],$_SESSION["id"]))
{           
            $email = $_SESSION["id"];
            $editemail = str_replace(".", "_" , str_replace("@","_", $email));
            $query = 'SELECT DaySinceInstall, LTV, UpperLimit, LowerLimit,100 as BreakevenPoint, ROAS FROM '.$editemail.'_ltvdata order by DaySinceInstall';
            $statement = $connect->prepare($query);
            $statement->execute();
            $result = $statement->fetchAll();
            foreach($result as $row)
            {
                $output[] = array(
                'Day Since Install'        => $row["DaySinceInstall"],
                'Estimated LTV'            => $row["LTV"],
                'Upper Margin of Error'    => $row["UpperLimit"],
                'Lower Margin of Error'    => $row["LowerLimit"],
                'Estimated ROAS'           => $row["ROAS"],
                'Breakeven Point'          => $row["BreakevenPoint"]
            );
            }
            echo json_encode($output);       

}

?>
