<?php
session_start();
include("TOPLTV.php");
?>

<div  style ="width: 100%; height: 100%; background-color:#F0F0F0;  ">
  <div class="container" style="float:left;width:17%; height:1350px; margin-left:-1px; margin-right:5px; color:white;background-color:#383838;" >
     <br />  
     <div class = "form-group" > 
        <label >Data Period to Estimate:</label><br />            
             <div class="radio" style="float:left;width:100%; margin-left:22px;"> 
                    <input type="radio" name="weeks" value = "07 days"  />1 Week (Weak)<br />
                    <input type="radio" name="weeks" value = "14 days" />2 Weeks (Accepted)<br />
                    <input type="radio" name="weeks" value = "21 days" />3 Weeks (Good)<br />
                    <input type="radio" name="weeks" value = "30 days" />1 Month (Very Good)<br />        
        
            </div>
            
    </div>
    
    
    <label >Players Start Dates:</label><br />
    <div  id="daterang">
        <div class="form-group">
        <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" autocomplete="off" disabled/>
        </div>
        <div class="form-group">
        <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" autocomplete="off" disabled/>
        </div>

    </div> 

    <div class="form-group" >
        
          <label >Game:</label><br />
          <select class="form-control" name="games" id="games" style="text-align:center;">
             <?php
               session_start();
               include('../conn.php');
               if(isset($_SESSION["id"]))
               {    
                   $email = $_SESSION["id"];
                   $query = "SELECT game_name_short from games where email = '$email' group by game_name_short order by game_name_short";
                   $statement = $connect->prepare($query);
                   $statement->execute();
                   $result = $statement->fetchAll();
                   $output = '';
                   foreach($result as $row)
                   {
                       $output .='<option value="'.$row['game_name_short'].'">'.$row['game_name_short'].'</option>';
                   }
                   echo $output;
               }
             ?>
          
          </select>
       
    </div>        
    
    
    <div class="form-group">
        
        <input type="button" onclick="remove()" name="btn_search" id="btn_search" value="Collect Dates Data" class="btn btn-primary" style="background-color:forestgreen; border-color:green;" />
    </div>  


    <div class="form-group" >
        
          <label >Platform:</label><br />
          <select name="platform[]" id="platform" multiple class="form-control" style="text-align:center;" >
          
          </select>
       
    </div>

    <div class="form-group" >
          <label >Campaign:</label><br />
          <select name="campaign[]" id="campaign" multiple class="form-control" >
          
          </select>
    </div>

    <div class="form-group">
          <label >Country:</label><br />
          <select name="country[]" id="country" multiple class="form-control">
           
          </select>
    </div>

    <div class="form-group">
                         <input type="checkbox" name = "revenueselectiap" id="iap_revenue_ltv" value ="checked"  checked/> &nbsp IAP Revenue<br />
                         <input type="checkbox" name = "revenueselectads" id="ads_revenue_ltv" value ="checked" /> &nbsp ADS Revenue<br />
    </div>

    <div class="form-group">        
        <input type ="submit" name="btn_LTV" id="btn_LTV" value="Calculate The LTV" class="btn btn-primary" style="background-color:green;border-color:green;"/>
    </div>

    <div style="position: relative; bottom: 0; left:-1%; color:white; margin-top:625px; width:100%"> <p style="font-size:70%;white-space: nowrap;">Copyright © 2022 MANDSLTV | All Rights Reserved</p> </div> 
      
  </div>


  <div class="container" style ="width: 100% ;background-color:#F0F0F0; " > 
        
     <!-- <div style="display:inline-block;vertical-align:top; position: relative; height: 30px;"></div> -->
      <div>

              <div> 
                    <h2 style="float:left; margin-left:1.7%; font-size:150%; color:forestgreen;"> Selected cohort real values:</h2> 
                    <h5 id ="expire" style="float:right; margin-left:25px; color:red;"> </h5> 
              </div>
           
              <br><br><br>
              
              <div class="box" > &nbsp Total Users <br> <center><h2 id = "total_users"   style="margin-top:-1%;"><h2></center></div>
              <div class="box" >&nbsp Total Revenue    <br> <center><h2 id = "total_revenue" style="margin-top:-1%;"><h2></center></div>
              <div class="box" >&nbsp ARPU             <br> <center><h2 id = "total_arpu"    style="margin-top:-1%;"><h2></center></div>
              <div class="box" >&nbsp Total Payers    <br> <center><h2 id = "total_payers"  style="margin-top:-1%;"><h2></center></div>
             

              <div id = "title" style="float:left; margin-left:25px;margin-top:1%; font-size:1.5vw;color:forestgreen;word-wrap: break-word;white-space:pre-wrap;"> </div> 
            
              <div style="margin-bottom:1%;">
                <input type ="submit" name="downloadltv" id="downloadltv" value="Download Data" class="btn btn-primary" style="float:right;margin-top:1%;margin-right:1%; background-color:green;border-color:green;box-shadow: 0 0 15px 0 #888;"/>
              </div>

              <div id="clear"> <input type="hidden" id="clear1" name="clear1" value= "0"> </div>
            
              <br><br><br><br><br> 

              <div >
                        <div id="chart_area" style="margin-left:18%; width: 81%; height: 500px;box-shadow: 0 0 15px 0 #888;"></div>
              </div>
      <br /><br />

                <div style="margin-left:18%;">   
                    <label >Enter CPI:</label>    
                    <input type ="number"  name="CPIN" id="CPIN" step="0.01" min="0" value="1" style="border-radius:4px; border-color:forestgreen;width:5%;box-shadow: 0 0 10px 0 #888;margin:0 0 1% 0;"/>       
                    <p style="display: inline;font-size:10px;">(CPI = Cost Per Install)</p>
                </div> 
     
                <div >
                                  <div id="chart_area2" style="margin-left:18%; width: 81%; height: 500px;box-shadow: 0 0 15px 0 #888;"></div>
                </div>
      </div>
  </div>         

</div>


<div class="overlay center"  style="float:center; width:17%; height:100%; margin-top:0px; margin-right:5px; color:black; text-align:center;">
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <h2>    Please wait...</h2>
                
<?php
include("../BOTTOM.php");
?>