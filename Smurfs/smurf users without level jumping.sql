with
data00 as (select userID, count(*) as N from events where eventName = 'smurfberryEvent' and berryEffector = 'NEWPLAYER' group by 1),
data000 as (select userID from data00 where N > 1),
data0 as (select userID, min(levelNumber) from events group by 1 having min(levelNumber) > 1 order by 1),
data as (select userID, levelNumber from events where levelNumber is not null group by 1,2),
data2 as (select userID, levelNumber - lead(levelNumber) over (partition by userID order by levelNumber) as D from data order by 1,2),
data3 as (select userID from data2 where D is not null and D != -1), -- be carful with levels 0 and 1 because some versions do not have level 1 they have 0 then 2 and the difference is -2
data4 as ((select userID from data0) union all (select userID from data3) union all (select userID from data000 ) ),
data5 as (select userID from data4 group by 1),



with
data00 as (select userID, count(*) as N from events where eventName = 'smurfberryEvent' and berryEffector = 'NEWPLAYER' group by 1),
data000 as (select userID from data00 where N > 1),
data0 as (select userID, min(levelNumber) from events group by 1 having min(levelNumber) > 1 order by 1),
data as (select userID, levelNumber from events where levelNumber is not null group by 1,2),
data2 as (select userID, levelNumber - lead(levelNumber) over (partition by userID order by levelNumber) as D from data order by 1,2),
data3 as (select userID from data2 where D is not null and D != -1),
data4 as ((select userID from data0) union all (select userID from data3) union all (select userID from data000 ) ),
data5 as (select userID from data4 group by 1),

data6 as (select levelNumber,events.userID, datediff('DAY', gaUserStartDate, eventDate) as days 
, dense_rank() over (partition by events.userID order by eventDate)-1 as rank
from events left join data5 on
events.userID = data5.userID
where 
data5.userID is null
and eventName = 'missionCompleted'
and events.userID != 'be217ff0f85bb39bfa2ced8b93ead691e959c50f9b96705d07e18e6f67a0d70e'
--and useCheats != 1
)

select levelNumber,count(distinct userID), avg (days) as Day_of_install, avg(rank) as Day_of_reconnect from data6
where days >= 0
group by 1
order by 1 


with
data00 as (select userID, count(*) as N from events where eventName = 'smurfberryEvent' and berryEffector = 'NEWPLAYER' group by 1),
data000 as (select userID from data00 where N > 1),
data0 as (select userID, min(levelNumber) from events group by 1 having min(levelNumber) > 1 order by 1),
data as (select userID, levelNumber from events where levelNumber is not null group by 1,2),
data2 as (select userID, levelNumber - lead(levelNumber) over (partition by userID order by levelNumber) as D from data order by 1,2),
data3 as (select userID from data2 where D is not null and D != -1),
data4 as ((select userID from data0) union all (select userID from data3) union all (select userID from data000 ) ),
data5 as (select userID from data4 group by 1),

data6 as (select levelNumber,events.userID, sum(amount) as amount from events left join data5 on events.userID = data5.userID
where
eventName = 'smurfberryEvent'
and berryEffector not in ('MINIMAP', 'HEFTYCHALLENGE','COINCONVERSION','BUYBERRIES')
and amount between 0 and 12000
and data5.userID is null
-- and events.userID not in ('be217ff0f85bb39bfa2ced8b93ead691e959c50f9b96705d07e18e6f67a0d70e'
-- ,'53442f7d11f9f0dc984a095399a47337b5da6f88f93072be269f549abb4468d9'
-- ,'4862fff22745532f276e1f32f3605a6e7ac74af83058d656ac1f6cee4eb8492c'
-- )
--and useCheats != 1
group by 1,2 
order by 2,1)
select levelNumber,count(distinct userID), min(amount), avg(amount), max(amount) from data6 group by 1 order by 1
