1. "#009900" "forestgreen"
2. "#009999"
3. "#0066CC"
4. "#0033CC"
5. "#330099"
6. "#990099"

1. "#33CC33"
2. "#00CCCC"
3. "#3399FF"
4. "#3366FF"
5. "#6666FF"
6. "#9933CC"


1. "#66FF99"
2. "#66FFFF"
3. "#99CCFF"
4. "#6699FF"
5. "#9999FF"
6. "#CC99FF"



-- --session length 
--                 with data1 as (select user_id , revenue, total_time_ms/60000 as time_in_minutes from fact_user_sessions_day order by session_id )
--                 select count (time_in_minutes) from data1 where time_in_minutes < 1

-- coins given and spent 

                -- -- given
                -- with 
                -- data0 as (select count(distinct userId) as tu from events),
                -- data1 as (select datediff("DAY",gaUserStartDate ,eventDate) as DSI, userID, amount from events 
                -- where eventName = 'coinsEvent' and amount between 0 and 175000 and coinsEffector not in ('BELOW_1000','BUYCOINS'))
                -- select DSI, sum(amount)/tu as avg_coins_give from data1, data0 where DSI >= 0 group by DSI,tu order by DSI 


                -- --spent
                -- with 
                -- data0 as (select count(distinct userID) as tu from events),
                -- data1 as (select datediff("DAY",gaUserStartDate ,eventDate) as DSI, userID, amount from events 
                -- where eventName = 'coinsEvent' and amount between -10001 and 0 and coinsEffector not in ('BELOW_1000','BUYCOINS'))
                -- select DSI, sum(amount)/tu as avg_coins_spend from data1, data0 where DSI >= 0 group by DSI,tu order by DSI 



                --bought
                -- with 
                -- data0 as (select count(distinct userID) as tu from events),
                -- data1 as (select datediff("DAY",gaUserStartDate ,eventDate) as DSI, userID, amount from events 
                -- where eventName = 'coinsEvent' and amount between 0 and 175000 and coinsEffector in ('BUYCOINS'))
                -- select DSI, sum(amount)/tu as avg_coins_spend from data1, data0 where DSI >= 0 group by DSI,tu order by DSI 


--inventory


                -- -- end of the day inventory
                -- with 
                -- data0 as (select eventDate, count(distinct userId) as tu from events group by eventDate),
                -- data1 as (select eventDate, userID as users, max(eventTimestamp) as timee from events where userCoins is not Null group by userID, eventDate),
                -- data3 as (select events.eventDate,events.eventTimestamp, userID, userCoins from events, data1 
                -- where events.eventTimestamp = data1.timee and events.userID = data1.users and events.eventDate = data1.eventDate
                -- and userCoins < 200000
                -- order by userID ),
                -- data2 as (select eventDate, userID, userCoins from data3 group by 1,2,3 )

                -- select data2.eventDate, sum(userCoins)/tu as userCoins from data2, data0
                -- where data2.eventDate = data0.eventDate
                -- group by data2.eventDate, tu  order by data2.eventDate 


                -- start of the day inventory
                with 
                data0 as (select eventDate, count(distinct userId) as tu from events group by eventDate),
                data1 as (select eventDate, userID as users, min(eventTimestamp) as timee from events where userCoins is not Null group by userID, eventDate),
                data3 as (select events.eventDate,events.eventTimestamp, userID, userCoins from events, data1 
                where events.eventTimestamp = data1.timee and events.userID = data1.users and events.eventDate = data1.eventDate
                and userCoins < 200000
                order by userID ),
                data2 as (select eventDate, userID, userCoins from data3 group by 1,2,3 )

                select data2.eventDate, sum(userCoins)/tu as userCoins from data2, data0
                where data2.eventDate = data0.eventDate
                group by data2.eventDate, tu  order by data2.eventDate

                -- coins given daily
                with 
                data0 as (select eventDate, count(distinct userId) as tu from events group by eventDate),
                data1 as (select eventDate, userID, amount from events 
                where eventName = 'coinsEvent' and amount between 0 and 175000 and coinsEffector not in ('BELOW_1000','BUYCOINS'))
                select data1.eventDate, sum(amount)/tu as avg_coins_give
                from data1, data0 
                where data1.eventDate =data0.eventDate
                group by data1.eventDate,tu order by data1.eventDate 

                -- coins spent daily 
                -- with 
                -- data0 as (select eventDate, count(distinct userId) as tu from events group by eventDate),
                -- data1 as (select eventDate, userID, amount from events 
                -- where eventName = 'coinsEvent' and amount between -10001 and 0 and coinsEffector not in ('BELOW_1000','BUYCOINS'))
                -- select data1.eventDate, sum(amount)/tu as avg_coins_spent
                -- from data1, data0 
                -- where data1.eventDate =data0.eventDate
                -- group by data1.eventDate,tu order by data1.eventDate 

                -- with data1 as (select eventDate, sum(amount)/count(distinct userID) as amount from events_live 
                -- where eventName = 'coinsEvent' 
                -- and amount between -10001 and 0 
                -- and coinsEffector not in ('BELOW_1000','BUYCOINS')
                -- and eventDate between '",startDate,"' and '",endDate,"'
                -- group by 1)
                
                -- select round(avg(amount),2) from data1



                -- coins purchased daily 
                -- with 
                --                 data0 as (select eventDate, count(distinct userID) as tu from events group by eventDate),
                --                 data1 as (select eventDate, userID, amount from events 
                --                 where eventName = 'coinsEvent' and amount between 0 and 175000 and coinsEffector in ('BUYCOINS'))
                --                 select data1.eventDate, sum(amount)/tu as avg_coins_purchased from data1, data0 
                --                 where data1.eventDate = data0.eventDate 
                --                 group by data1.eventDate,tu order by data1.eventDate 


-- average time and level to convert  (Only for New users)
                 
                -- by date of install
                -- with data1 as (select user_id, player_start_date,min(event_date), datediff('day',player_start_date, min(event_date)) as conversion_day , count(revenue)
                -- from fact_user_sessions_day 
                -- where revenue > 0
                -- group by user_id,player_start_date 
                -- order by user_id )
                -- select avg(conversion_day) from data1

                -- by levels  
                         -- ##  is not working if the date more than 3 months
                with data1 as (select userID,  levelNumber as conversion_level 
                from events
                where revenueValidated = 1 and convertedProductAmount > 0
                )
                select avg(conversion_level) from data1

-- -- average time users watched ads daily
--                 with data1 as (select eventDate, userID, count(eventName) from events
--                 where
--                 eventName = 'advertising' and
--                 adEventTrigger = 'REWARDED'
--                 group by userID, eventDate)

--                 select  round(avg(count),2) as 'Avg. Ads watched' from data1 --group by gaUserCountry order by gaUserCountry


-- -- average decors purchased per day 
--                 with data1 as (select eventDate, userID, count(itemName) as DecorBought from events 
--                 where eventName = 'smurfberryEvent' 
--                 and itemName is not null
--                 group by eventDate, userID order by eventDate)

--                 select avg(DecorBought) from data1

-- -- average store visits per day 
--                 -- per placement per day
--                 with data1 as (select eventDate, userID, placement, count(*) as visits from events
--                 where eventName = 'storeEvent' and placement is not null and storeEffector = 'OPEN'
--                 group by 1,2,3
--                 order by userID)
--                 select  placement, avg(visits) as store_visit from data1 group by 1 order by 2 DESC

--                 -- per day only
--                 with data1 as (select eventDate, userID, count(*) as visits from events
--                 where eventName = 'storeEvent' and placement is not null and storeEffector = 'OPEN'
--                 group by 1,2
--                 order by userID)
--                 select avg(visits) as store_visit from data1 --group by 1 order by 2 DESC    

-- average store visits before the first purchase
                -- with data1 as (select user_id, min(event_date) as min_event_date from fact_user_sessions_day where revenue > 0 group by user_id ),
                -- data2 as (select userID, eventDate, storeEffector from events
                -- where 
                -- (storeEffector = 'OPEN' or (revenueValidated = 1 and convertedProductAmount > 0))),

                -- data3 as (select userID, count(*) as store from data2, data1
                -- where storeEffector = 'OPEN' and user_id = userID and  eventDate <= min_event_date
                -- group by userID )

                -- select round(avg(store),2) as 'Avg store visits before the first purchase' from data3

-- -- average store visits per day per non-payers users
--                 with data1 as (select user_id from fact_user_sessions_day group by user_id having sum(revenue) = 0 ),
--                 data2 as (select userID, eventDate, storeEffector from events
--                 where 
--                 storeEffector = 'OPEN' )
--                 ,data3 as (
--                 select userID,eventDate,  count(*) as store from data2, data1
--                 where user_id = userID 
--                 group by userID, eventDate 
--                 )
--                 select round(avg(store),2) as 'Avg stove visits per day per user' from data3

-- -- average store visits per day per payers users 
--                 with data1 as (select user_id from fact_user_sessions_day group by user_id having sum(revenue) > 0 ),
--                 data2 as (select userID, eventDate, storeEffector from events
--                 where 
--                 storeEffector = 'OPEN' )
--                 ,data3 as (
--                 select userID,eventDate,  count(*) as store from data2, data1
--                 where user_id = userID 
--                 group by userID, eventDate 
--                 )
--                 select round(avg(store),2) as 'Avg stove visits per day per user' from data3



-- Avg Berries Inventory
h0 = paste("
              with data1 as (select userID as users, eventDate as datee, max(eventTimestamp) as maxEvent from events_live 
              where eventName = 'missionCompleted'
              and userSmurfberries < 500000
              group by 1,2)

             select userID, rank() over (partition by userID order by eventTimestamp) rank, userSmurfberries 
              from events_live, data1  
              where 
              eventDate between '",startDate,"' and '",endDate,"'
              and events_live.userID = data1.users
              and events_live.eventTimestamp = data1.maxEvent
              and eventName = 'missionCompleted'
              order by userID
              
                ")
          
ABI0 = dbGetQuery(conn, h0)

-- Avg berries Spent
k0 = paste("
            select eventDate,userID, sum(amount) as amount from events_live 
                where eventName = 'smurfberryEvent' 
                and amount between -10001 and 0 
                and berryEffector not in ('BUYBERRIES')
                and eventDate between '",startDate,"' and '",endDate,"'
                group by 1,2
            ")

ABS0 = dbGetQuery(conn, k0)

-- Avg Boosters Used
z0 = paste("
            select userID, eventDate,
case when sum(boostersUsed_verticalZap) is null then 0 else sum(boostersUsed_verticalZap) end  as verticalZap,
case when sum(boostersUsed_horizontalZap) is null then 0 else sum(boostersUsed_horizontalZap) end as horizontalZap,
case when sum(boostersUsed_colorClear) is null then 0 else sum(boostersUsed_colorClear) end as colorClear,
case when sum(boostersUsed_rainbowBubble) is null then 0 else sum(boostersUsed_rainbowBubble) end as rainbowBubble,
case when sum(boostersUsed_starShower) is null then 0 else sum(boostersUsed_starShower) end as starShower,
--case when sum(superUsedAmount) is null then 0 else sum(superUsedAmount) end as superFill,
case when sum(boostersUsed_extraBubbles) is null then 0 else sum(boostersUsed_extraBubbles) end as extraBubbles
from events_live
where eventDate between '",startDate,"' and '",endDate,"' group by 1,2
                        
            ")

ABU0 = dbGetQuery(conn, z0)


-- Avg Coins Inventory

h0 = paste("
              with data1 as (select userID as users, eventDate as datee, max(eventTimestamp) as maxEvent from events_live 
              where eventName = 'missionCompleted'
              and userCoins < 200000
              group by 1,2)

              select userID, rank() over (partition by userID order by eventTimestamp) rank, userCoins 
              from events_live, data1  
              where 
              eventDate between '",startDate,"' and '",endDate,"'
              and events_live.userID = data1.users
              and events_live.eventTimestamp = data1.maxEvent
              and eventName = 'missionCompleted'
              order by userID

                ")
          
ACI0 = dbGetQuery(conn, h0)

-- Avg Coins Purchased
h0 = paste("
            with
            data0 as (select distinct user_id as users from fact_user_sessions_day_live 
            where
            revenue > 0 and event_date between '",startDate,"' and '",endDate,"')
           
            select eventDate,userID, sum(productAmount) as coins from events_live, data0
              where events_live.userID = data0.users
                    and eventName = 'transaction'
                    and productName = 'coins'
                    and revenueValidated = 1
                    and eventDate between '",startDate,"' and '",endDate,"'
                    group by 1,2 having sum(productAmount) > 0
            ")

ACP0 = dbGetQuery(conn, h0)

--Avg Coins Spent

k0 = paste("
            select eventDate, userID, sum(amount) as amount from events_live 
                where eventName = 'coinsEvent' 
                and amount between -10001 and -1 
                and coinsEffector not in ('BELOW_1000','BUYCOINS')
                and eventDate between '",startDate,"' and '",endDate,"'
                group by 1,2
                
            ")

ACS0 = dbGetQuery(conn, k0)


-- Avg Decor Purchased

n0 = paste("
           select eventDate, userID, count(itemName) as DecorBought from events_live 
                where eventName = 'smurfberryEvent' 
                and itemName is not null
                and eventDate between '",startDate,"' and '",endDate,"'
            group by eventDate, userID order by eventDate
            ")

ADP0 = dbGetQuery(conn, n0)

--Avg Levels beat number

q0 = paste("
            select eventDate, userID , count(distinct levelNumber) as levels from events_live
            
            where 
                   (minimapId is null or minimapId = 'n/a')
                   --and isHeftyLevel != 1
                  and eventDate between '",startDate,"' and '",endDate,"'
            group by 1,2
            order by 1,2
                        
            ")

ALN0 = dbGetQuery(conn, q0)

--Avg Session Length

q0 = paste("
           select user_id ,event_date, round(total_time_ms/60000,2) as time_in_minutes 
           from fact_user_sessions_day_live  
           where event_date between '",startDate,"' and '",endDate,"'
           and total_time_ms/60000 > 1
           
           ")

ASL0 = dbGetQuery(conn, q0)


-- Avg Sessions Number
q0 = paste("
            select eventDate, userID , count(distinct sessionID) as sessions from events_live
            
            where 
                   eventDate between '",startDate,"' and '",endDate,"'
            group by 1,2
            order by 1,2
              ")

ASN0 = dbGetQuery(conn, q0)

--Avg Store visits

h0 = paste("
              select userID, eventDate, count(storeEffector) as store from events_live
                where 
                storeEffector = 'OPEN' 
                and eventDate between '",startDate,"' and '",endDate,"'
                group by 1,2
                ")
          
ASV0 = dbGetQuery(conn, h0)

--Avg Vedio rewarded

m0 = paste("
            select eventDate, userID, count(eventName) from events_live
                where
                 eventDate between '",startDate,"' and '",endDate,"'
                 and eventName = 'advertising' 
                 and adEventTrigger = 'REWARDED'
                group by userID, eventDate
            ")

AVW0 = dbGetQuery(conn, m0)

--
