Q1
select APP_NAME, sum(REVENUE)/count(distinct ID) as RPI from
((select PSEUDO_USER_ID as ID, APP_NAME from "DEMO2"."PUBLIC"."USER_DETAIL" where INSTALLATION_DATE ='2010-11-01') A
left join
(select ID, APP_NAME, sum(REVENUE_CENTS) as REVENUE from "DEMO2"."PUBLIC"."USER_PERFORMANCE" where DATE ='2010-11-01' group by 1,2) B
using(ID, APP_NAME))
group by 1
order by 1

Q2
select APP_NAME, VERSION, TU as TOTAL_INSTALLS from
(select *,rank() over (partition by APP_NAME order by TU DESC) as rank from 
(select APP_NAME, VERSION, count(distinct PSEUDO_USER_ID) as TU from "DEMO2"."PUBLIC"."USER_DETAIL" 
where LOWER(COUNTRY) = 'us' 
group by 1,2))
where rank <= 2
order by 1,3 desc

Q3
with data1 as(
  select * from
(select CAMPAIGN_NAME as CAMPAIGN, sum(REVENUE_CENTS) as REVENUE from 
(select PSEUDO_USER_ID ,CAMPAIGN_NAME,REVENUE_CENTS from 
"DEMO2"."PUBLIC"."USER_DETAIL" A left join "DEMO2"."PUBLIC"."USER_PERFORMANCE" B on A.PSEUDO_USER_ID=B.ID) group by 1) C
join
(select LOWER(substr(CAMPAIGN,1,14)) as CAMPAIGN, sum(COST_IN_CENTS) as COST from "DEMO2"."PUBLIC"."CAMPAIGN_SPEND" group by 1) D
using(CAMPAIGN)),
data2 as (select CAMPAIGN, REVENUE-COST as MARGIN from data1),
data3 as (select sum(REVENUE)-sum(COST) as TOTAL_MARGIN from data1)
select CAMPAIGN, round(MARGIN*100/TOTAL_MARGIN,2)||'%' as MARGIN_PERCENTAGE,
round(sum(MARGIN*100/TOTAL_MARGIN) over (order by MARGIN*100/TOTAL_MARGIN DESC),2)||'%' as CUMULATED_TOP_MARGIN_PERCENTAGE
from data2, data3 
order by 2 desc
